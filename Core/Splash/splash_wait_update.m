function update = splash_wait_update(pal, message, value)

% splash_wait_update - update splash waitbar
% ------------------------------------------
%
% update = splash_wait_update(pal, message, value);
%
% Input:
% ------
%  pal - splash handle
%  message - message
%  value - value

%------------------
% HANDLE INPUT
%------------------

% NOTE: return if there is nothing to set

if nargin < 2
	return;
end

% NOTE: this is convenient, so we can ask update in a single line

if isempty(pal) || ~ishandle(pal)
	return;
end 

%--
% get value from tick
%--

if nargin < 3
	value = get_tick_value(pal);
end

%------------------
% SET WAITBAR
%------------------

update = 1;

if ischar(message)
	update = waitbar_update(pal, 'PROGRESS', 'message', upper(message)); pause(0.05);
end 

if update
	update = waitbar_update(pal, 'PROGRESS', 'value', value);
end


%--------------------------------
% GET_TICK_VALUE
%--------------------------------

function value = get_tick_value(pal)

% get_tick_value - get value from waitbar tick
% --------------------------------------------
%
% value = get_tick_value(pal) 
%
% Input:
% ------
%  pal - splash handle
%
% Output:
% -------
%  value - waitbar value

data = get(pal, 'userdata');

tick = data.tick;

% NOTE: this incremente the tick position value to the end of the tick sequence

value = tick.value(tick.pos); tick.pos = min(tick.pos + 1, length(tick.value));

data.tick = tick; set(pal, 'userdata', data);

