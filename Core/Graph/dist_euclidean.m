function D = dist_euclidean(x,y)

% dist_euclidean - euclidean distance matrix for vectors
% ------------------------------------------------------
%
% D = dist_euclidean(x,y)
%
% Input:
% ------
%  x - column vectors  (row index)
%  y - column vectors (column index)
%
% Output:
% ------
%  D - distance matrix

%--
% pack vectors into multidimensional arrays
%--

m = size(x,2);
n = size(y,2);

% non hermitian transpose

x = x.';

x = repmat(permute(x,[1 3 2]),[1 m]);
y = repmat(permute(y,[3 2 1]),[n 1]);

%--
% compute distance matrix
%--

D = sqrt(sum((x - y).^2,3));

% D = (sum((x - y).^p,3)).^(1/p);

