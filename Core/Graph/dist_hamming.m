function D = dist_hamming(x,y)

% dist_hamming - hamming distance matrix for integers
% ---------------------------------------------------
%
% D = dist_hamming(x,y)
%
% Input:
% ------
%  x - integers (row index)
%  y - integers (column index)
%
% Output:
% -------
%  D - distance matrix

%--
% compute integer binary representations
%--

% add some error cheking on the fact that inputs are integers

k = max(max(floor(log2(x))) + 1, max(floor(log2(y))) + 1);

x = int_to_bin(x,k);
y = int_to_bin(y,k);

%--
% compute distance matrix
%--

D = dist_euclidean(x,y).^2;
