function G = get_profile_graph

%--
% get profile info
%--

info = profile('info');

if (isempty(info))
	G = []; return;
end

%--
% extract graph from function table
%--

table = info.FunctionTable;

n = length(table);

G = sparse(n,n);

for k = 1:n
	
	% TODO: include number of calls information
	
	pix = cell2mat({table(k).Parents.Index}');
	
	if (~isempty(pix))
		G(k,pix) = 1;
	end
	
	cix = cell2mat({table(k).Children.Index}');
	
	if (~isempty(cix))
		G(cix,k) = 1;
	end
	
end


fig; spy(G);

p = colamd(G);
fig; spy(G(p,p));