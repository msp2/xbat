function [L,l] = edge_lengths(X,E)

% edge_lengths - compute edge lengths
% -----------------------------------
%
%  L = edge_lengths(X,E)
%
% Input:
% ------
%  X - vertex positions
%  E - edge cell array
%
% Output:
% -------

%--
% create complex representation of position
%--

% this only works for planar graphs

x = X*[1; i];

%--
% compute edge lengths
%--

for k = 1:length(E)
	L{k} = abs(x(k) - x(E{k})');
end

%--
% put lengths in a vector
%--

if (nargout > 1)
	
	l = [];
	for k = 1:length(L)
		l = [l; L{k}'];
	end

end








% This function can be generalized to the case of measuring
% how distant other variables, linking features, are with
% respect to the graph of another, or even to their own graph,
% for example in this way we may determine if we can tighten
% the linking interval

% Sensitivity analysis of the linking interval is definitely
% an interesting thing to do, it is not clear yet what is the
% mathematical statement behind this
