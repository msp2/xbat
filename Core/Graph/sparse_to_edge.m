function [E,W] = sparse_to_edge(A)

% sparse_to_edge - sparse matrix to edge list representation of graph
% -------------------------------------------------------------------
%
% [E,W] = sparse_to_edge(A)
%
% Input:
% ------
%  A - sparse matrix
%
% Output:
% -------
%  E - vertex edge list
%  W - vertex edge weights

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 586 $
% $Date: 2005-02-22 14:22:50 -0500 (Tue, 22 Feb 2005) $
%--------------------------------

%--
% get size of sparse matrix
%--

[m,n] = size(A);

if (m ~= n)
	error('Matrix is not square.');
end

%--
% unweighted graph
%--

if (nargout < 2)
	
	E = cell(n,1);

	for k = 1:n
		E{k} = find(A(k,:));
	end
	
%--
% weighted graph
%--

else
	
	E = cell(n,1); W = cell(n,1);

	for k = 1:n
		[ignore,E{k},W{k}] = find(A(k,:));
	end
	
end













