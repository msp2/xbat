function obj = timer_stop(name)

% timer_stop - stop a named timer
% -------------------------------
%
% obj = timer_stop(name)
%
% Input:
% ------
%  name - timer name
%
% Output:
% -------
%  obj - timer considered

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1935 $
% $Date: 2005-10-14 16:58:12 -0400 (Fri, 14 Oct 2005) $
%--------------------------------

%------------------------------------
% SETUP
%------------------------------------

% NOTE: configure timer action

prop = 'running'; state = 'on'; action = @stop;

%------------------------------------
% HANDLE INPUT
%------------------------------------

% NOTE: act on existing timers when no input

if nargin < 1
	timer_action(timerfind, prop, state, action); return;
end

%------------------------------------
% ACT ON TIMERS
%------------------------------------

%--
% look for named timer
%--

% NOTE: we consider non-singleton timer problem

obj = timerfind('name', name);

if isempty(obj)
	return;
end

if length(obj) > 1
	warning(['There are multiple timers named ''', name, '''.']);
end

%--
% act on timer
%--

timer_action(obj(1), prop, state, action);
