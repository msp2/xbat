function obj = timer_action(obj, prop, state, action)

% timer_action - timer action on property state
% ---------------------------------------------
%
% obj = timer_action(obj, prop, state, action)
%
% Input:
% ------
%  obj - action timers
%  prop - property to watch
%  state - value to watch for
%  action - action callback
%
% Output:
% -------
%  obj - action timers

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1935 $
% $Date: 2005-10-14 16:58:12 -0400 (Fri, 14 Oct 2005) $
%--------------------------------

% TODO: add checking on property and state

for k = 1:length(obj)
	
	% NOTE: check that property is in prescribed action state
	
	if ~isequal(get(obj(k), prop), state)
		continue;
	end
	
	% TODO: modify this to handle general callbacks via 'eval_callback'
	
	action(obj(k));
	
end