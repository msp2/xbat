function k = kernel_window(type,n)

% kernel_window - create smoothing kernel window
% ----------------------------------------------
% 
% k = kernel_window(type,n)
%
% Input:
% ------
%  type - kernel type
%  n - kernel length
%
% Output:
% -------
%  k - smoothing kernel window

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--------------------------------------------
% HANDLE INPUT
%--------------------------------------------

%--
% ensure odd length kernel
%--

if (~mod(n,2))
	n = n - 1;
end

%--------------------------------------------
% COMPUTE KERNEL ACCORDING TO TYPE
%--------------------------------------------

% NOTE: we capitalize the kernel type

switch (title_caps(type))
	
	%--
	% binomial kernel
	%--
	
	case ('Binomial')
		
		k = filt_binomial(1,n);
	
	%--
	% gaussian kernel
	%--
	
	case ('Gauss')
		
		k = linspace(-4,4,n);
		k = exp(-k.^2);
		k = k / sum(k);
	
	%--
	% epanechnikov kernel
	%--
	
	case ('Epanechnikov')
		
		k = linspace(-1,1,n);
		k = (1 - k.^2);
		k = k / sum(k);
	
	%--
	% tukey biweight kernel
	%--
	
	case ('Tukey')
		
		k = linspace(-1,1,n);
		k = (1 - k.^2).^2;
		k = k / sum(k);
	
	%--
	% uniform kernel
	%--
	
	case ('Uniform')
		
		k = ones(1,n) / n;
		
	%--
	% unrecognized kernel type
	%--
	
	otherwise
		
		disp(' ');
		error(['Unrecognized kernel type ''' type '''.']);
	
end