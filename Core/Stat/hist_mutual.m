function I = hist_mutual(H)

% hist_mutual - compute variable mutual information
% -------------------------------------------------
%
% I = hist_mutual(H)
%
% Input:
% ------
%  H - joint histogram
%
% Output:
% -------
%  I - mutual information

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2005-12-15 13:52:40 -0500 (Thu, 15 Dec 2005) $
% $Revision: 2304 $
%--------------------------------

%--
% compute marginals and product histogram
%--

N = sum(H(:));
H = H / N;
			
h1 = sum(H,2);
h2 = sum(H,1);
			
H12 = h1*h2;

%--
% compute divergence of joint and product histograms
%--

I = hist_divergence(H,H12);
