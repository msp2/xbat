function y = kernel_smooth(h,c,k)

% kernel_smooth - smooth histogram using a kernel
% -----------------------------------------------
%
% y = kernel_smooth(h,c,k)
%
% Input:
% ------
%  h - bin counts
%  c - bin centers
%  k - kernel
%
% Output:
% -------
%  y - smoothed histogram

%--
% normalize histogram
%--

nh = h / ((c(2) - c(1)) * sum(h));

%--
% smooth histogram using kernel 
%--

y = conv2(nh,k,'same');

%--
% consider edge effects
%--

k = cumsum(k);
n = length(k); 
n = floor(n/2) + 1;
k = k(end - n + 1:end);

y(1:n) = y(1:n) ./ k;
y(end - n + 1:end) = y(end - n + 1:end) ./ fliplr(k);
