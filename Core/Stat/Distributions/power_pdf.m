function y = power_pdf(x,p)

% power_pdf - one sided power law pdf
% -----------------------------------
%
% y = power_pdf(x,p)
%
% Input:
% ------
%  x - points to evaluate
%  p - generalized gaussian parameters [mean, deviation, shape]
%
% Output:
% -------
%  y - pdf values

y = gauss_pdf_(x,p(3),p(2),p(1));

% set lower than mean values to zero

ix = find(x < p(1));
y(ix) = 0;
