function p = loggauss_fit(X)

% loggauss_fit - log generalized gaussian fit
% -------------------------------------------
% 
% p = loggauss_fit(X)
%
% Input:
% ------
%  X - input data
%
% Output:
% -------
%  p - log generalized gaussian parameters [mean, deviation, shape]

%--
% put data in column and zero center
%--

% remove zeros from values

X = X(:);
ix = find(X == 0);
X(ix) = [];

% compute log transformation

X = log(X);

% center values

m = sum(X)/length(X);
X = X - m;

%--
% fit using mex and update mean parameter
%--

p = zeros(1,3);
[p(3),p(2),p(1)] = gauss_fit_(X);

p(1) = p(1) + m;
