function p = frechet_fit(X)

% frechet_fit - fit frechet extreme value distribution
% --------------------------------------------------
%
% p = frechet_fit(X)
%
% Input:
% ------
%  X - input data
%
% Output:
% -------
%  p - frechet parameters

%--
% put data into vector
%--

x = X(:);

%--
% compute initial parameter estimates
%--

p(1) = mean(x);
p(2) = std(x);

%--
% minimize negative likelihood
%--

opt = optimset;
opt.Display = 'iter';

p = fminsearch('frechet_likelihood',p,opt,x);
