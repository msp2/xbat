function L = frechet_likelihood(p,X)

% frechet_likelihood - frechet log likelihood
% -----------------------------------------
%
% L = frechet_likelihood(p,X)
%
% Input:
% ------
%  p - frechet parameters
%  X - input data
%
% Output:
% -------
%  L - negative log likelihood of data

%--
% put data in vector and separate parameters
%--

x = X(:);

a = p(1);
b = p(2);

%--
% compute negative log likelihood
%--

d = -x + a;
z = d / b;

L = sum((1 / b) * exp((d - (b * exp(z))) / b));
% L = -L;