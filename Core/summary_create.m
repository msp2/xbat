function summary = summary_create(varargin)

% summary_create - create generation summary structure
% ----------------------------------------------------
%
%  summary = summary_create
%
% Output:
% -------
%  summary - generation summary structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%---------------------------------------------------------------------
% CREATE SUMMARY STRUCTURE
%---------------------------------------------------------------------

persistent SUMMARY_PERSISTENT;

if (isempty(SUMMARY_PERSISTENT))
	
	%--------------------------------
	% PRIMITIVE FIELDS
	%--------------------------------
	
	summary.type = 'summary';
	
% 	rand('state',sum(100*clock));
% 	
% 	summary.id = round(rand * 10^16); 		% id for summary
			
	%--------------------------------
	% DATA FIELDS
	%--------------------------------
	
	summary.preset = []; 					% state of computating extension
			
	summary.events = []; 					% events created or modified as result of computation
	
	summary.etime = []; 					% time elapsed during computation

	%--------------------------------
	% ADMINISTRATIVE FIELDS
	%--------------------------------
	
	summary.author = ''; 					% user requesting computation
	
	summary.created = now; 					% creation date
	
	summary.modified = []; 					% modification date

	%--------------------------------
	% USERDATA FIELD
	%--------------------------------
	
	summary.userdata = []; % userdata field is not used by system
	
	%--
	% set persistent scan
	%--
	
	SUMMARY_PERSISTENT = summary;
	
else
	
	%--
	% copy persistent scan and update creation date
	%--
	
	summary = SUMMARY_PERSISTENT;
	
	summary.created = now;
	
end

%---------------------------------------------------------------------
% SET FIELDS IF PROVIDED
%---------------------------------------------------------------------

if (length(varargin))
	
	%--
	% try to get field value pairs from input
	%--
	
	summary = parse_inputs(summary,varargin{:});

end


