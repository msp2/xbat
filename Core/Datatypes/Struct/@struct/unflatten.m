function out = unflatten(in, sep)

% unflatten - unflatten struct
% ----------------------------
%
% out = unflatten(in, sep)
%
% Input:
% ------
%  in - flattened scalar struct
%  sep - field separator (def: '__', double underscore)
%
% Output:
% -------
%  out - unflattened struct

%---------------------------------------
% HANDLE INPUT
%---------------------------------------

%--
% set default separator
%--

if (nargin < 2) 
	sep = '__';
end

%--
% check for scalar struct
%--

if (length(in) ~= 1)
	error('Scalar struct input is required.');
end

%---------------------------------------
% UNFLATTEN STRUCTURE
%---------------------------------------

% NOTE: the order of flattening and unflattening matters

field = fieldnames(in);

for k = 1:length(field)
	eval(['out.', strrep(field{k}, sep, '.'), ' = in.', field{k}, ';']);
end