function skel = get_skel(X)

% get_skel - get form skeleton
% ----------------------------
%
% skel = get_skel(X)
%
% Input:
% ------
%  X - input object
%
% Output:
% -------
%  skel - object skeleton

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.7 $
% $Date: 2004-06-08 13:54:58-04 $
%--------------------------------

skel = get_form(X,[],1);