function value = has_space(str)

% has_space - test for presence of space in string
% ------------------------------------------------
%
% value = has_space(str)
%
% Input:
% ------
%  str - string
%
% Output:
% -------
%  value - result of test

if iscellstr(str)
	
	value = zeros(size(str));
	
	for k = 1:numel(str)
		value(k) = has_space(str{k});
	end
	
	return;
	
end

if ischar(str)
	value = any(isspace(str)); return;
end

error('Input must be string or string cell array.');
