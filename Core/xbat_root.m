function root = xbat_root

% xbat_root - get XBAT root directory
% -----------------------------------
%
% root = xbat_root
%
% Output:
% -------
%  root - XBAT root directory

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 6472 $
% $Date: 2006-09-11 18:11:43 -0400 (Mon, 11 Sep 2006) $
%--------------------------------

%--
% try to get environment variable
%--

root = get_env('xbat_root');

%--
% set environment variable if needed
%--

if isempty(root)

	%--
	% get root by parsing path for this file
	%--
	
	% NOTE: the logical root may change as directory organization changes
	
	root = mfilename('fullpath'); 
	
	ix = findstr(root, filesep); 
	
	root = root(1:ix(end - 1) - 1);

	%--
	% set environment variable
	%--
	
	set_env('xbat_root', root);
	
end