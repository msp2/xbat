function str = string_title(str,p)

% string_capitalize - create title string
% ---------------------------------------
%
% str = string_title(str,p)
%
% Input:
% ------
%  str - input string
%  p - separator pattern (def: ' ')
%
% Output:
% -------
%  str - output title string

%--
% set default pattern
%--

if (nargin < 2)
	p = ' ';
end

%--
% split string capitalize and join
%--

tok = string_split(str,p);
str = '';

if (iscell(tok))
	for k = 1:length(tok)
		tmp = tok{k};
		tmp(1) = upper(tmp(1));
		str = [str, ' ', tmp];
	end
else
	str = tok;
	str(1) = upper(str(1));
end