function log_fileupdate(str)

% log_fileupdate - update filenames in log after renaming files
% -------------------------------------------------------------
% 
% log_fileupdate(str)
%
% Input:
% ------
%  str - filename update string
%
% Notes:
% ------
%  1. 
%
%     '*_yyyylldd_hhmmss' -> ID5_20010314_203412.aif -> March 14,2001 20:34:12
%     'yylldd_hhmmss' -> 010105_034512.wav -> January 5,2001 03:45:12

%--
% open log file and load variables
%--

if (nargin < 2)
	
	%--
	% open log file
	%--
	
	try
		[fn_in,p_in] = uigetfile('*.mat','Select log file to update filenames:');
		tmp = load([p_in,fn_in]);
	catch
		disp(' ');
		warning('Get log file was cancelled.');
	end
	
	%--
	% get variables
	%--
	
	try 
		geom = tmp.arrGeom;
		sound = tmp.fileList;
		log = tmp.output;
		new_log = tmp.new_log;
	catch
		disp(' ');
		error('File does not contain needed log variables.');
	end
	
end

%--
% select output file location
%--

tmp = findstr(fn_in,'.'); 
fn = fn_in(1:tmp(end) - 1);
name = [p_in, fn, '_new.mat'];

[fn,p] = uiputfile( ...
	name, ...
	'Choose location of output text file:' ...
);
