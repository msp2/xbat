function  [c, S] = color_to_rgb(str)

% color_to_rgb - get rgb values for named color
% ---------------------------------------------
%
% [c, S] = color_to_rgb(str)
%
% Input:
% ------
%  str - color name
%
% Output:
% -------
%  c - rgb values for named color or list of available colors
%  S - menu separators

%--
% create name color cell array table
%--

T = { ...
	'Red', [0.7, 0.05, 0.05]; ...
	'Green', [0.05, 0.7, 0.05]; ...
	'Blue', [0, 0.05, 0.7]; ...
	'Cyan', [0, 0.7, 0.7] ;...
	'Magenta', [0.7, 0, 0.7]; ...
	'Yellow', [0.8, 0.8, 0]; ...
	'Black', [0, 0, 0]; ...
	'Dark Gray', [0.25, 0.25, 0.25]; ...
	'Gray', [0.5, 0.5, 0.5]; ...
	'Light Gray', [0.75, 0.75, 0.75]; ...
	'White', [1, 1, 1]; ...
	'Bright Red', [1, 0, 0.05]; ...
	'Bright Green', [0, 1, 0.05]; ...
	'Bright Blue', [0, 0.05, 1]; ...
	'Dark Red', [0.5, 0.05, 0.05]; ...
	'Dark Green', [0.05, 0.5, 0.05]; ...
	'Dark Blue', [0, 0.05, 0.5]; ...
	'Light Red', [1, 0.45, 0.45]; ...
	'Light Green', [0.45, 1, 0.45]; ...
	'Light Blue', [0.45, 0.45, 1] ...
};


if nargin
	
	%--
	% look up color
	%--
	
	ix = find(strcmp(str, T(:, 1)));
	
	if ~isempty(ix)
		c = T{ix, 2};
	else
		c = [];
	end
	
else
	
	%--
	% output all available colors
	%--
	
	c = T(:, 1);
	
	%--
	% output menu separators
	%--
	
	if (nargout > 1)
		
		n = length(c);
		
		S = bin2str(zeros(n, 1)); S{4} = 'On'; S{8} = 'On'; S{12} = 'On';
	
	end
	
end

