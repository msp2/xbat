function cache = cache_create

% cache_create - create cache structure
% -------------------------------------
% 
% cache = cache_create
%
% Output:
% -------
%  cache - cache structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% get current cache labels
%--

data = get(0,'userdata');

if (isfield(data,'cache'))
	
	if (length(data.cache))
		labels = struct_field(data.cache,'label');
		flag = 1;
	else
		flag = 0;
	end
	
else
	
	data.cache = [];
	flag = 0;
	
end

%--
% create unique double cache label
%--

label = rand(1) * 10^6;

if (flag)
	while (length(find(label == labels)))
		label = rand(1) * 10^6;
	end
end

%--
% create cache structure
%--

cache.type = 'cache';

cache.label = label;

cache.data = [];







