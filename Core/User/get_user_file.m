function file = get_user_file(user, root)

% get_user_file - get user file name
% ----------------------------------
%
% file = get_user_file(user, root)
%
%      = get_user_file(name, root)
%
% Input:
% ------
%  user - user
%  name - name
%  root - parent application root (def: xbat_root)
%
% Output:
% -------
%  file - user file name

%---------------------
% HANDLE INPUT
%---------------------

%--
% set application root
%--

if nargin < 2
	root = xbat_root;
end

%--
% handle user input
%--

switch class(user)
	
	case 'char'
		
		name = user;
		
		if ~proper_filename(name)
			error('User name must be a proper filename.');
		end
		
	case 'struct'
		
		if ~isfield(user, 'name')
			error('Input does not seem to be user.');
		end
		
		name = user.name;
		
	otherwise, error('User input must be user or user name.');
		
end

%---------------------
% USER FILE
%---------------------

file = [users_root(root), filesep, name, filesep, name, '.mat'];

