function S = library_info_str(lib, fields)

%-----------------------------------------------------
% HANDLE INPUT
%-----------------------------------------------------

%--
% set fields if needed
%--

if (nargin < 2) || isempty(fields)
	
	fields = { ...
		'name', ...
		'sounds', ...
		'logs', ...
		'author', ...
		'created', ...
		'modified' ...
	};

end

%--
% set default library
%--

if ~nargin
	lib = get_active_library;
end

%-----------------------------------------------------
% CREATE STRING
%-----------------------------------------------------

S = cell(0); 

for k = 1:length(fields)
	
	prefix = [title_caps(fields{k}), ':  '];
	
	switch fields{k}
	
		case 'name'
			
			S{end + 1} = [prefix, lib.name];
			
		case 'sounds'
			
			S{end + 1} = [prefix sound_info_str(get_library_sounds(lib), 'multiple')];
			
		case 'logs'
			
			S{end + 1} = [prefix, log_info_str(get_library_logs('logs', lib), 'multiple')];
			
		case 'author'
			
			S{end + 1} = [prefix, lib.author];
				
		case 'created'
			
			if isempty(lib.created)
				continue;
			end
			
			S{end + 1} = [prefix, datestr(lib.created)];
			
		case 'modified'
			
			if isempty(lib.modified) || (abs(lib.modified - lib.created) < 1/86400)
				continue;
			end
			
			S{end + 1} = [prefix, datestr(lib.modified)];
			
	end
	
end
