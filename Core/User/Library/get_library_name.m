function name = get_library_name(lib, user)

% get_library_name - get full library name, includes author
% ---------------------------------------------------------
%
% name = get_library_name(lib)
%
% Input:
% ------
%  lib - library
%  
% Output:
% -------
%  name - library name

name = lib.name; author = lib.author;

% NOTE: this is to suppress the author from the name if it matches the
% input user, consider using a 'short' mode or something like that

if nargin > 1 && ~isempty(user) && strcmp(author, user.name)
	return;	
end

name = [author, filesep, name];