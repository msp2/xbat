function [lib, user] = get_library_from_name(name, user)

% get_library_from_name - get library from its full user/library name
% -------------------------------------------------------------------
%
% [lib, user] = get_library_from_name(name, user)
%
% Input:
% ------
%  name - full library name string
%  user - user in context
%
% Output:
% -------
%  lib - library
%  user - user

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision: 1632 $
% $Date: 2005-08-23 13:09:06 -0400 (Tue, 23 Aug 2005) $
%--------------------------------

if (nargin < 2)
	user = get_active_user;
end

%--
% handle multiple inputs recursively
%--

if iscell(name)
	
	lib = empty(library_create);
	
	for k = 1:length(name)	
		libk = get_library_from_name(name{k}); lib = [lib, libk]; 
	end
	
	user = []; return;

end
	
%--
% parse string
%--

info = parse_tag(name, filesep, {'author', 'libname'});

if isempty(info.libname)
	lib = get_libraries(user, 'name', info.author); return;	
end

%--
% get user
%--

user = get_users('name', info.author); 

%--
% get library
%--

lib = get_libraries(user, 'name', info.libname);