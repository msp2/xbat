function author = author_validate(author,title)

% author_validate - ensure non-empty author
% -----------------------------------------
%
% author = author_validate(author,title)
%
% Input:
% ------
%  author - author string
%  title - title of input dialog
%
% Output:
% -------
%  author - author string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% set title
%--

if (nargin < 2)
	title = 'Author';
end

%--
% ensure non-empty author
%--

if (isempty(author))
	
	ans_dialog = '';
	
	while (isempty(ans_dialog))
		
		ans_dialog = input_dialog( ...
            'Author', ...
            title, ...
            [1,40], ... 
            {''} ...
        );
	
		if (isempty(ans_dialog))
		
			author = [];
			return;
			
		else
		
			ans_dialog = deblank(fliplr(deblank(fliplr(ans_dialog{1}))));
			
			if (isempty(ans_dialog))
				
				tmp = warn_dialog( ...
					'Please enter a non-empty author.', ...
					'XBAT Warning  -  Empty Author', ...
					'modal' ...
				);
			
				waitfor(tmp);
				
			end
			
		end
		
	end	        
	
	author = ans_dialog;
	
end