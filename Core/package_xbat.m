function package_xbat(location, revision, destination, platform)

% package_xbat - export and clean up the xbat working copy for distribution
% -------------------------------------------------------------------------
%
% package_xbat(location, revision, destination)
%
% Input:
% ------
%  location - working copy or URL (def: xbat_root)
%  revision - revision number (def: HEAD)
%  destination - where to put the package (def: xbat_root/Package)
%  platform - the platform to package for

%--
% handle input
%--

if nargin < 4
    platform = computer;
end

if nargin < 3 || isempty(destination)
    destination = fullfile(xbat_root, 'Package');
end

if nargin < 2
    revision = [];
end

if ~nargin || isempty(location)
    location = xbat_root;
end

if ~ischar(revision)
    revision = int2str(revision);
end

%--
% clean up current destination directory
%--

rmdir(destination, 's');

%--
% export
%--

str = ['"', location, '"', ' "', destination, '"'];

if ~isempty(revision)
    str = ['-r ', revision, str];
end

svn('export', str);

%-------------------------
% CLEAN UP
%-------------------------

%--
% remove not-needed root-level directories
%--

not_needed = { ...
    'Samples', ...
    'Tools', ...
    'Toolboxes', ...
    'Sites' ...
};

for k = 1:length(not_needed) 
    rmdir(fullfile(destination, not_needed{k}), 's');
end
    
%--
% get annoying 'svn_info' file
%--

copyfile( ...
    fullfile(xbat_root, 'Core', 'Subversion', 'svn_info.m'), ...
    fullfile(destination, 'Core', 'Subversion', 'svn_info.m') ...
);

%--
% remove mex-files for non-win platforms
%--

if ~strcmp(platform, 'PCWIN')
    scan_dir(destination, @(p)delete([p, filesep, '*.mexw32'])); 
end




