function str = factor_str(n)

% factor_str - create string representation of prime factorization
% ----------------------------------------------------------------
%
% str = factor_str(n)
%
% Input:
% ------
%  n - integer to factor
%
% Output:
% -------
%  str - string representation of prime factors

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2271 $
% $Date: 2005-12-13 12:58:18 -0500 (Tue, 13 Dec 2005) $
%--------------------------------

%----------------------
% HANDLE INPUT
%----------------------

n = max(1, round(n));

%--
% handle array input recursively
%--

if (numel(n) > 1)
	
	str = cell(size(n)); 
	
	for k = 1:numel(n)
		str{k} = factor_str(n(k));
	end
	
	return;
	
end 

%----------------------
% CREATE FACTOR STRING
%----------------------

%--
% factor integer and collect like factors
%--

f = factor(n);

p = unique(f);

for k = 1:length(p)
	e(k) = sum((p(k) == f));
end

%--
% create factorization text string
%--

str = [int2str(n), ' = ', int2str(p(1))];

if (e(1) > 1)
	str = [str '^' int2str(e(1))];
end

for k = 2:length(p)
	str = [str ' x ' int2str(p(k))];
	if (e(k) > 1)
		str = [str '^' int2str(e(k))];
	end
end
	