function n = better_fft_size(n)

% better_fft_size - get a compatible better fft size
% --------------------------------------------------
%
% n = better_fft_size(n)
%
% Input:
% ------
%  n - desired fft size
%
% Output:
% -------
%  n - better fft size, may be larger than input

N = better_fft_sizes(n, 2 * n); 

ix = find(n <= N, 1, 'first'); 

n = N(ix);