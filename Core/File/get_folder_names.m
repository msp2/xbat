function names = get_folder_names(folder)

% get_folder_names - get child folders, excluding '.' and '__' prefixes
% ---------------------------------------------------------------------
%
% names = get_folder_names(folder)
%
% Input:
% ------
%  folder - folder
%
% Output:
% -------
%  names - names

%--
% iterate
%--

if iscell(folder)
	
	names = {};
	
	for k = 1:length(folder)
		part = get_folder_names(folder{k}); names = {names{:}, part{:}};
	end
	
	return;
	
end

%--
% get folder names
%--

content = dir(folder); names = {};

for k = 1:length(content)
	
	if content(k).isdir && (content(k).name(1) ~= '.') && ~any(strmatch('__', content(k).name))
		names{end + 1} = content(k).name;	
	end
	
end
