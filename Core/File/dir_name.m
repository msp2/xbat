function name = dir_name(path_string)

% dir_name - get name of leaf of path string
% ------------------------------------------

if iscell(path_string)
	
	name = {};
	
	for k = 1:length(path_string)
		name{end + 1} = dir_name(path_string{k});
	end
	
	return;
	
end

if isempty(path_string)
	name = ''; return;
end

if path_string(end) == filesep
	path_string(end) = '';
end

[ignore, name] = fileparts(path_string);