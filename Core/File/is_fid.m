function value = is_fid(in)

% is_fid - check whether input is file identifier
% -----------------------------------------------
%
% value = is_fid(in)
%
% Input:
% ------
%  in - input
%
% Output:
% -------
%  value - result of test

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2014 $
% $Date: 2005-10-25 17:43:52 -0400 (Tue, 25 Oct 2005) $
%--------------------------------

% NOTE: check if input is in list of open file handles

fids = fopen('all');

if isempty(fids)
	value = 0; return;
end

value = ismember(in, fids);