function [fun, args] = parse_fun(in)

% parse_fun - parse file line processing input
% --------------------------------------------
%
% [fun, args] = parse_fun(in)
%
% Input:
% ------
%  in - process line input
%
% Output:
% -------
%  fun - process line function handle
%  args - arguments for process line function

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%--
% consider form of fun input
%--

% NOTE: convert input function handle and argument based on class

switch class(in)
	
	case 'cell'
		args = in(2:end); fun = in{1};
		
	% NOTE: a string is a callback name, not a string to evaluate
	
	case 'char'
		args = []; fun = str2func(in);
		
	case 'function_handle'
		args = []; fun = in;
		
	otherwise
		error('Improper line processing input.');
		
end

%--
% check that fun is a function handle
%--

if ~isa(fun, 'function_handle')
	error('Improper function handle in line processing input.');
end