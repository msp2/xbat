function info = file_writelines(out, lines, fun, opt)

% file_writelines - write lines to file with optional processing
% --------------------------------------------------------------
%
% info = file_writelines(out, lines, fun, opt)
%
%  opt = file_writelines
%
% Input:
% ------
%  out - output file or file identifier
%  lines - file lines
%  fun - fun to process each line
%  opt - options
%
% Output:
% -------
%  info - output file info
%  opt - options

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%--------------------------------------
% HANDLE INPUT
%--------------------------------------

%--
% set default options and output options if needed
%--

if (nargin < 4) || isempty(opt)
	
	opt = file_process;
	
	if ~nargin
		info = opt; return;
	end
	
end

%--
% set no fun default
%--

if nargin < 3
	fun = [];
end

%--
% check lines are cell array of strings
%--

if ~iscellstr(lines)
	error('File lines must be a string cell array.');
end

%--------------------------------------
% WRITE LINES TO FILE
%--------------------------------------

% TODO: enforce write to file at this level, process will do buffer to buffer

info = file_process(out, lines, fun, opt);
