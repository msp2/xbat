function str = printf_escape(str)

% printf_escape - escape 'printf' special characters in string
% ------------------------------------------------------------
%
% str = printf_escape(str)
%
% Input:
% ------
%  str - input string
%
% Output:
% -------
%  str - escaped string

str = strrep(strrep(str, '\', '\\'), '%', '%%');

