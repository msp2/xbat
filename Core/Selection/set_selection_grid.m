function set_selection_grid(ax, x, y)

%--
% set default grid states
%--

if (nargin < 2)
	x = 0;
end

if (nargin < 3) 
	y = x;
end

%--
% update axes selection grid options
%--

opt = get_axes_selection_options(ax); opt.grid.x.on = x; opt.grid.y.on = y;

set_axes_selection_options(ax, opt);
