function [value, opt] = is_selection_axes(ax)

% is_selection_axes - test whether axes are selection axes
% --------------------------------------------------------
%
% [value, opt] = is_selection_axes(ax)
%
% Input:
% ------
%  ax - axes handles
%
% Output:
% -------
%  value - result of test
%  opt - axes selection options

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2273 $
% $Date: 2005-12-13 18:12:04 -0500 (Tue, 13 Dec 2005) $
%--------------------------------

%----------------------
% HANDLE INPUT
%----------------------

%--
% handle multiple axes recursively
%--

if numel(ax) > 1
	
	% NOTE: we store options in cell to handle empty options
	
	value = zeros(size(ax)); opt = cell(size(ax));
	
	for k = 1:numel(ax)
		[value(k), opt{k}] = is_selection_axes(ax(k));
	end
	
	return;
	
end

%----------------------
% CHECK AXES
%----------------------

%--
% check selection axes condition
%--

callback = get(ax, 'buttondownfcn');

value = iscell(callback) && isequal(callback{1}, @selection_axes_callback);

%--
% output selection options
%--

if nargout > 1
	if value
		opt = callback{2};
	else
		opt = [];
	end
end
