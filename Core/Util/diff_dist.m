function d = diff_dist(A, type, varargin)

% diff_dist - distance of adjacent columns
% ----------------------------------------
%
% d = diff_dist(A, type, param, ... )
%
% Input:
% ------
%  A - array to compute distances for
%  type - distance type
%  param - parameters for distance

%--
% handle input
%--

if (nargin < 3) || isempty(type)
	type = 2;
end

%--
% compute distance based on type
%--

if ~ischar(type)
	
	%-------------------------
	% P-NORM
	%-------------------------
	
	% TODO: consider the infinity norm separately
	
	d = sum(abs(diff(A, 1, 2)).^type, 1).^(1 / type);
	
else
	
	switch type
	
		%-------------------------
		% MAHALANOBIS
		%-------------------------
	
		% NOTE: this is euclidean with non-trivial covariance
		
		case 'mahalanobis'
			
			if ~length(varargin)
				d = diff_dist(A, 2); return;
			end
		
			C = varargin{1};
			
		%-------------------------
		% PEARSON AND FOOTE
		%-------------------------
		
		% NOTE: these are based on normalized correlation with and without centering of the vectors
	
		case {'pearson', 'foote'}
			
			%--
			% normalize dimension
			%--
			
			rows = size(A, 1); 
			
			if strcmp(type, 'pearson')
				A = A - repmat(sum(A, 1) / rows, rows, 1);
			end
			
			fast_min_max(sum(A, 1))
			
			A = A ./ repmat(sqrt(sum(A.^2, 1)), rows, 1);
			
			%--
			% compute distance
			%--
			
			d = abs(sum(A(:, 1:end - 1) .* A(:, 2:end), 1));
		
		%-------------------------
		% KL AND MODIFIED-KL
		%-------------------------
	
		% NOTE: the following two are based on more or less on information divergence
		
		% NOTE: these are not symmetric in time!
		
		case {'kl','kullback', 'mkl', 'modified-kullback'}
			
			%--
			% check for positive input
			%--
			
			if any(A < 0)
				error('To use information distance, input array must be positive.');
			end
			
			%--
			% normalize dimension
			%--
			
			rows = size(A, 1);
			
			A = A ./ repmat(sum(A, 1), rows, 1);
			
			%--
			% compute log odds, also known as surprise
			%--
			
			surprise = log2(A(:, 2:end) ./ A(:, 1:end - 1));
			
			%--
			% combine log odds based on type
			%--
			
			switch type
				
				case {'kl','kullback'}
					d = sum(A(:, 2:end) .* surprise, 1);
						
				case {'mkl', 'modified-kullback'}
					d = sum(surprise .* (surprise > 0), 1);
			end
			
	end
	
end
