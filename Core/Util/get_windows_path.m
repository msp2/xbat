function path = get_windows_path

% get_windows_path - get path elements in windows
% -----------------------------------------------
%
% path = get_windows_path
%
% Output: 
% -------
%  path - string cell array with elements of windows path

if ~ispc
	error('This function is only available for Windows.');
end

% NOTE: we get path string and parse elements into cells

[status, result] = system('path'); 

[ignore, result] = strtok(result, '='); 

path = strread(result(2:end), '%s', -1, 'delimiter', ';');
