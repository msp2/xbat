function str = stack_line(stack) 

% stack_line - create a linked description for a stack line
% ---------------------------------------------------------
%
% str = stack_line(stack)
%
% Input:
% ------
%  stack - element of stack
%
% Output:
% -------
%  str - linked description string

file = strrep(stack.file, xbat_root, '');

link = ~isequal(file, stack.file);

file = strrep(file, matlabroot, '(matlabroot)');

str = [file, ' at line ', int2str(stack.line)];

if link
	str = ['In <a href="matlab:opentoline(''', stack.file, ''',', int2str(stack.line), ')">', '(xbat_root)', str, '</a>'];
else
	str = ['In ', str];
end