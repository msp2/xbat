function opt = nice_catch(info, str, opt)

% nice_catch - typically used to produce warnings from catch blocks
% -----------------------------------------------------------------
%
% nice_catch(info, str, opt)
%
% opt = nice_catch
%
% Input:
% ------
%  info - error info struct as provided by 'lasterror'
%  str - message string
%  opt - options
%
% Output:
% -------
%  opt - options

% TODO: implement logging, save to file, then optionally report to site

%----------------
% HANDLE INPUT
%----------------

%--
% set and possibly output default options
%--

% NOTE: the log field indicates the log file to use, this should contain
% name and file extension, the file extension will determine storage type

if nargin < 3
	
	opt.log = ''; opt.timestamp = 0;
	
	if ~nargin
		return;
	end
	
end

%--
% set default message
%--

% NOTE: the message really wants to be called from a catch!

if nargin < 2
	str = 'WARNING: Exception handled.';
end

%--
% pad message and create line
%--

str = [' ', str];

n = length(str) + 1; line = str_line(n, '_');

%----------------
% DISPLAY
%----------------

%--
% display warning
%--

if ~isstruct(info)
	return;
end

disp(' '); 

disp(line);
disp(' '); 
disp(str);
disp(line);

disp(' '); 

disp(' MESSAGE:');
disp(' ');

if ~isfield(info, 'identifier') || isempty(info.identifier)
	disp(['   ', info.message]);
else
	disp(['   ', info.message, ' (', info.identifier, ')']);
end

disp(' ');

disp(' STACK:');

% NOTE: try to get stack information if not available

full = 1;

if ~isfield(info, 'stack')
	try
		info.stack = dbstack('-completenames'); info.stack(1:2) = []; full = 0;
	catch
		info.stack = [];
	end
end

if ~isempty(info.stack)
	
	if ~full
		disp(' ');
		disp('   WARNING: Only partial stack information is available.');
	end

	disp(' ');

	stack_disp(info.stack);
	
% 	for k = 1:length(info.stack)
% 		disp(['   ', int2str(k), '. ', stack_line(info.stack(k))]);
% 	end
	
else
	
	disp(' ');
	disp('   WARNING: No stack information is available.');
	
end
	
disp(' ');
disp(line);
disp(' ');

	
