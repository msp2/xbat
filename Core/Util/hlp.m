function hlp(fun)

% hlp - better help
% -----------------
%
% hlp fun
%
% Input:
% ------
%  fun - function name

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2173 $
% $Date: 2005-11-21 18:26:27 -0500 (Mon, 21 Nov 2005) $
%--------------------------------

%--
% clear screen and set more on
%--

clc; more on;

%--
% display help
%--

disp(' ');

if ~nargin || isempty(fun)
	help;
else
	help(fun);
end

disp(' ');

%--
% reset more
%--

more off;
