function content = no_dot_dir(root)

% no_dot_dir - get children directories that do not start with dot
% ----------------------------------------------------------------
%
% content = no_dot_dir(root)
%
% Input:
% ------
%  root - root
%
% Output:
% -------
%  child - children

%--
% set current directory default
%--

if ~nargin
	root = pwd;
end

%--
% get children directories without dots
%--

content = dir(root);

for k = length(content):-1:1
    
	if content(k).isdir && (content(k).name(1) == '.' || content(k).name(1) == '_')
		content(k) = [];
	end
    
end