function value = is_handles(in, type)

% is_handles - check input is handles possibly of a type
% ------------------------------------------------------
%
% value = is_handles(in, type)
%
% Input:
% ------
%  in - input array
%  type - required handle type
%
% Output:
% -------
%  value - result of test

%--
% check input is handles
%--

value = 1;

if ~all(ishandle(in))
	value = 0; return;
end

if nargin < 2
	return;
end

%--
% check handle types
%--

if any(~strcmp(get(in, 'type'), type))
	value = 0;
end
