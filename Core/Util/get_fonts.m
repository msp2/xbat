function fonts = get_fonts(select)

% get_fonts - get available system fonts
% --------------------------------------
%
% fonts = get_fonts(select)
%
% Input:
% ------
%  select - select fonts to look for
% 
% Output:
% -------
%  fonts - available select fonts in system 

%--
% get all fonts
%--

fonts = listfonts;

% NOTE: return if there is no selection

if (nargin < 1) || isempty(select)
	return;
end

%--
% select fonts
%--

fonts = intersect(fonts, select);