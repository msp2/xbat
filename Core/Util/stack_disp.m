function stack_disp(stack)

for k = 1:length(stack)
	disp(['   ', int2str(k), '. ', stack_line(stack(k))]);
end