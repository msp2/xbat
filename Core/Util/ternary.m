function value = ternary(test, first, second)

% ternary - ternary operator
% --------------------------
%
% value = ternary(test, first, second)
%
% Input:
% ------
%  test - test
%  first - value when test is true
%  second - value when test is false

if ~isempty(test) && test
	value = first;
else
	value = second;
end