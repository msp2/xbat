function ppi = get_ppi

% get_ppi - get screen pixels per inch
% ------------------------------------
%
% ppi = get_ppi
%
% Output:
% -------
%  ppi = pixels per inch on screen

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 976 $
% $Date: 2005-04-25 19:27:22 -0400 (Mon, 25 Apr 2005) $
%--------------------------------

% NOTE: this is simply a shortcut to get a root property

ppi = get(0,'ScreenPixelsPerInch');