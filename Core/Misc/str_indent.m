function S = str_indent(T,n)

% str_indent - indent printf string
% ---------------------------------
%
% S = str_indent(T,n)
% 
% Input:
% ------
%  T - fprintf strings
%  n - number of levels to indent (def: 1)
% 
% Output:
% -------
%  S - indented fprintf strings

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-07-06 13:36:09-04 $
%--------------------------------

%--
% set levels
%--

if (nargin < 2)
	n = 1;
end

%--
% put string in cell
%--

if (isstr(T))
	T = cellstr(T);
	flag = 1;
else
	flag = 0;
end

%--
% indent string using replace
%--

if (n > 1)

	s = [];
	for k = 1:n
		s = [s '\t'];
	end
	
	S = cell(size(T));
	for k = 1:length(T)
		S{k} = [s strrep(T{k},'\n',['\n' s])];
	end
	
else

	S = cell(size(T));
	for k = 1:length(T)
		S{k} = ['\t' strrep(T{k},'\n','\n\t')];
	end
	
end

%--
% output string
%--

if (flag)
	S = S{1};
end
