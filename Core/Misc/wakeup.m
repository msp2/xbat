function flag = wakeup(t)

% wakeup - restart timers
% -----------------------
% 
% flag = wakeup(t)
%
% Input:
% ------
%  t - timer array
%
% Output:
% -------
%  flag - timer state change flag

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 923 $
% $Date: 2005-04-07 10:15:20 -0400 (Thu, 07 Apr 2005) $
%--------------------------------

%-------------------------------------------
% HANDLE INPUT
%-------------------------------------------

%--
% set default timer array
%--

if (nargin < 1)
	t = timerfind;
end

% NOTE: return when there are no timers

if (isempty(t))
	flag = 0; return;
end

%-------------------------------------------
% WAKE TIMERS
%-------------------------------------------

%--
% get current running state
%--

s1 = get(t,'running');

%--
% restart timers
%--

stop(t); start(t);

%--
% get new running state
%--

s2 = get(t,'running');

%--
% compare states to determine if restart changed anything
%--

flag = 0;

for k = 1:length(s1)
	
	if (~strcmp(s1{k},s2{k}))
		flag = 1; break;
	end
	
end