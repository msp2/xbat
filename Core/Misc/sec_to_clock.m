function str = sec_to_clock(t,n)

% sec_to_clock - convert seconds to time string
% ---------------------------------------------
%
% str = sec_to_clock(t,n)
%
% Input:
% ------
%  t - time in seconds
%  n - fractional second digits (def: 2)
%
% Output:
% -------
%  str - time string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 4794 $
% $Date: 2006-04-25 14:54:27 -0400 (Tue, 25 Apr 2006) $
%--------------------------------

%---------------------------------------------
% MEX COMPUTATION
%---------------------------------------------

%--
% compute clock string using mex
%--

% NOTE: we are not using precision right now

str = sec_to_clock_(t);

%--
% remove trailing zeros if needed
%--

for k = 1:length(str)
	
	if (abs(t(k) - floor(t(k))) < 0.005)
		str{k}(end - 2:end) = [];
	end
	
end

%--
% output string
%--

% NOTE: we output a string for a single time

if (length(str) == 1)
	str = str{1};
end