function out = find_path(pat,type)

% find_path - find a path element matching a pattern
% --------------------------------------------------
%
% out = find_path(pat,type)
%
% Input:
% ------
%  pat - pattern to find
%  type - type of matching (def: 'strcmpi')
%
% Output:
% -------
%  out - path elements that match

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 3934 $
% $Date: 2006-02-26 18:24:50 -0500 (Sun, 26 Feb 2006) $
%--------------------------------

%----------------------
% HANDLE INPUT
%----------------------

%--
% set default match type
%--

if (nargin < 2)
	type = 'strcmpi';
end

%--
% check match type
%--

types = {'strcmp','strcmpi','regex'};

if (~ismember(type,types))
	error(['Unrecognized match type ''', type, '''.']);
end

%----------------------
% FIND PATH
%----------------------

%--
% get and parse path
%--

out = strread(path,'%s',-1,'delimiter',';');

%--
% consider type in computing match
%--

% NOTE: it seems like this would be a useful function, like 'filter_strings'

switch (type)
	
	case ({'strcmp','strcmpi'})

		% NOTE: be case insensitive if needed
		
		if (type(end) == 'i')
			in = lower(out); pat = lower(pat);
		else
			in = out;
		end
		
		ix = find(~cellfun(@isempty,strfind(in,pat)));

	case ('regex')
		
		error('Regular expression matching not implemented yet.');
		
end
		
%--
% output path elements
%--

if (isempty(ix))
	out = cell(0); return;
end

out = out(ix);

