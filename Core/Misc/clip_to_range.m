function x = clip_to_range(x, r)

% clip_to_range - clip values to range
% ------------------------------------
%
% y = clip_to_range(x, r)
%
% Input:
% ------
%  x - input array
%  r - clip range
%
% Output:
% -------
%  y - clipped array

%--
% return if no clipping needed
%--

if (nargin < 2) || isempty(r)
	return;
end

%--
% clip array using range
%--

x(x < r(1)) = r(1);

x(x > r(2)) = r(2);