function h = match_tag_prefix(g,str,fast)

% match_tag_prefix - select handles by matching tag prefix
% --------------------------------------------------------
%
% h = match_tag_prefix(g,str)
%
% Input:
% ------
%  g - input handles
%  str - prefix string
%
% Output:
% -------
%  h - selected handles

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 513 $
% $Date: 2005-02-09 21:06:29 -0500 (Wed, 09 Feb 2005) $
%--------------------------------

%---------------------------------------------
% HANDLE INPUT
%---------------------------------------------

%--
% set default fast flag
%--

if (nargin < 3)
	fast = 0;
end

%--
% check input is handles
%--

if (~fast)
	if (any(~ishandle(g)))
		disp(' ')
		error('First input to this function must consist only of handles.');
	end
end

%---------------------------------------------
% GET AND MATCH TAGS
%---------------------------------------------

%--
% get object tags
%--

N = numel(g);

% NOTE: pack single figure tag into cell array

if (N == 1)
	tag = {get(g,'tag')};
else
	tag = get(g,'tag');
end

%--
% get length of prefix
%--

n = length(str);

%--
% match tag prefix
%--

% NOTE: initialize counter and output array for speed

j = 0;
h = zeros(size(g));

for k = 1:N
	
	% NOTE: check length of tag before comparing for speed
	
	if ((length(tag{k}) >= n) && strncmp(tag{k},str,n))
		j = j + 1;
		h(j) = g(k);
	end

end	

%--
% output only selected handles
%--

h = h(1:j);