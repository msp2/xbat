function value = not_empty(in)

% not_empty - true if not empty
% -----------------------------
% 
% test = not_empty(in)
%
% Input:
% ------
%  in - input value
%
% Output:
% -------
%  test - result of not empty test

value = ~isempty(in);
