function flash_restore(state)

% flash_restore - restore values changed by 'flash_update'
% --------------------------------------------------------
%
% flash_restore(state)
%
% Input:
% ------
%  state - output from 'flash_update'

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1789 $
% $Date: 2005-09-15 17:23:27 -0400 (Thu, 15 Sep 2005) $
%--------------------------------

% NOTE: state contains handles updated in flash, properties and initial values

for j = 1:length(state.handles)
	
	% NOTE: handle object may have been destroyed since flash
	
	if (~ishandle(state.handles(j)))
		continue;
	end
	
	for k = 1:length(state.prop)
		set(state.handles(j),state.prop{k},state.init{j}{k});
	end
	
end
