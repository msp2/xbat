function [rle,part,Y] = interview(X)

%-----------------------------------------------------------
% PROBLEM 1: run-length encode 
%-----------------------------------------------------------

x = [0, X(:)', 0];

rle.size = size(X);

rle.init = X(1);

rle.code = diff(find(diff(x)));

%-----------------------------------------------------------
% PROBLEM 2: decompose into increasing and decreasing
%-----------------------------------------------------------

d = diff(x);

z = zeros(size(x)); 

up = z; up(d > 0) = 1;

down = z; down(d < 0) = -1;

part.up = cumsum(up(1:end - 2));

part.down = cumsum(down(1:end - 2));

%-----------------------------------------------------------
% PROBLEM 3: run-length decode 
%-----------------------------------------------------------

x = cumsum(rle.code);

d = (-1).^(1:length(x));

Y(x) = d;

Y = [rle.init, Y];

Y = cumsum(Y(1:end - 1));

Y = reshape(X,rle.size);