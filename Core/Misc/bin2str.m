function y = bin2str(x)

% bin2str - binary to string conversion
% -------------------------------------
%
% y = bin2str(x)
%
% Input:
% ------
%  x - binary array
%
% Output:
% -------
%  y - cell array of 'off' and 'on' strings

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1286 $
% $Date: 2005-07-26 13:55:35 -0400 (Tue, 26 Jul 2005) $
%--------------------------------

% NOTE: consider extending to other types, particularly 'char' in a different way

%--
% create cell array
%--

y = cell(size(x));

%--
% fill cell array
%--

for k = 1:length(x)

	if (x(k))
		y{k} = 'On';
	else
		y{k} = 'Off';
	end
	
end

%--
% output string
%--

if (length(x) == 1)
	y = y{1};
end
