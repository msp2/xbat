function out = mfile_info(in,field)

% mfile_info - get file info for m-file
% -------------------------------------
%
%  info = mfile_info(file)
%
% value = mfile_info(file,field)
%
% Input:
% ------
%  file - file name
%  field - field to extract
%
% Output:
% -------
%  info - file info structure
%  value - info field value, empty if field does not exist

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1000 $
% $Date: 2005-05-03 19:36:26 -0400 (Tue, 03 May 2005) $
%--------------------------------

%--
% get file info
%--

out = dir(which(in));

%--
% extract field if needed
%--

if (nargin > 1)
	
	% NOTE: we return empty when the field is not available
	
	if (isfield(out,field))
		out = out.(field);
	else
		out = [];
	end
	
end