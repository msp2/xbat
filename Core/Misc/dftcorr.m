function Y = dftcorr(X,H)

% NOTE: code from DIPUM

[M,N] = size(X);
X = fft2(X);
H = conj(fft2(H,M,N));
Y = real(ifft2(H.*X);