function out = rel_path(in, root, name)

% rel_path - display relative path using token
% --------------------------------------------
%
% out = rel_path(in, root, name)
%
% Input:
% ------
%  in - absolute path
%  root - root path
%  name - root token name
%
% Output:
% -------
%  out - relative path

%--
% handle input
%--

if nargin < 3
	name = '$XBAT_ROOT';
end

if nargin < 2
	root = xbat_root;
end

if iscellstr(in)
	
	% NOTE: the number of output arguments is used by the function
	
	if ~nargout
		for k = 1:numel(in)
			rel_path(in{k}, root, name);
		end
	else
		for k = 1:numel(in)
			out{k} = rel_path(in{k}, root, name);
		end
	end
	
	return;

end

%--
% perform replacement, possibly display
%--

out = strrep(in, root, name);

if ~nargout
	disp(out);
end