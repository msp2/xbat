function x = rand_ab(n, a, b)

% rand_ab - uniform random numbers in an interval
% -----------------------------------------------
%
% x = rand_ab(n, a, b)
%
% Input:
% ------
%  n - size of output
%  a - min value
%  b - max value
%
% Output:
% -------
%  x - random numbers in interval

%----------------------------------
% HANDLE INPUT
%----------------------------------

%--
% set default interval limit
%--

if (nargin < 2)
	a = 1;
end

%--
% set interval for single interval input
%--

if (nargin < 3)
	b = abs(a);
	a = -b;
end

%--
% set default number of outputs
%--

if (nargin < 1)
	n = 1;
end

%--
% check limits
%--

% NOTE: we simply swap the values

if (b < a)
	c = b; b = a; a = c;
end

%----------------------------------
% COMPUTE
%----------------------------------

x = ((b - a) * rand(n)) + a;

