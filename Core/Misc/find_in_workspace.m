function var = find_in_workspace(x, workspace)

% find_in_workspace - find variable in specified workspace
% --------------------------------------------------------
%
% var = find_in_workspace(x, workspace)
%
% Input:
% ------
%  x - variable
%  workspace - workspace to search (def: 'base')
%
% Output:
% -------
%  var - variable structure output from 'whos'


if nargin < 2 || isempty(workspace)
	workspace = 'base';
end

var = [];

vars = evalin(workspace, 'whos');

for k = 1:length(vars)

	if ~isequal(size(x), vars(k).size) || ~isequal(x, evalin(workspace, vars(k).name))
		continue
	end

	var = vars(k); 

end