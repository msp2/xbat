function str = show_reg_exp(res, sep)

% show_reg_exp - display regular expression match
% -----------------------------------------------
%
% str = show_reg_exp(res, sep)
%
% Input:
% ------
%  res - match results structure
%  sep - match separator structure ('open' and 'close' fields)
%
% Output:
% -------
%  str - string display for match

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1380 $
% $Date: 2005-07-27 18:37:56 -0400 (Wed, 27 Jul 2005) $
%--------------------------------

% TODO: extent may be empty even when there is a match, handle this

%--
% set display separator patterns
%--

if (nargin < 2)
	sep.open = '<<'; sep.close = '>>';
end

%--
% build match string
%--

% NOTE: output trivial no match string

if (~length(res.start))
	str = res.str; return;
end

str = [res.str(1:res.start(1) - 1), sep.open, res.str(range(res.extent{1})), sep.close];

for k = 2:length(res.start)
	str = [ ...
		str, res.str(res.end(k - 1) + 1:res.start(k) - 1), sep.open, res.str(range(res.extent{k})), sep.close ...
	];
end

str = [str, res.str(res.end(end) + 1:end)];

%--
% utility function to expand extent
%--

function ix = range(extent)

ix = extent(1):extent(2);