function tok = string_split(str,p)

% string_split - split a string based on a pattern
% ------------------------------------------------
% 
% tok = string_split(str,p)
%
% Input:
% ------
%  str - input string
%  p - separator pattern
%
% Output:
% -------
%  tok - string tokens

%--
% get pattern length and indexes
%--

m = length(p);
ix = findstr(str,p);

%--
% get tokens from string
%--

if (length(ix))
	tok{1} = str(1:(ix(1) - 1));
	for k = 2:length(ix)
		tok{k} = str((ix(k - 1) + m):ix(k) - 1);
	end
	tok{length(ix) + 1} = str((ix(end) + m):end);
else
	tok = str;
end



