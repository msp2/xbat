function metadata = metadata_create(varargin)

% metadata_create - create metadata structure
% -------------------------------------------
%
% metadata = metadata_create
%
% Output:
% -------
%  metadata - XBAT metadata structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%---------------------------------------------------------------------
% CREATE METADATA STRUCTURE
%---------------------------------------------------------------------

persistent METADATA_PERSISTENT;

if (isempty(METADATA_PERSISTENT))

	%--------------------------------
	% PRIMITIVE FIELDS
	%--------------------------------
	
	metadata.name = ''; % name of metadata
	
	metadata.fun = []; % function implementing metadata, function handle
	
	%--------------------------------
	% ADMINISTRATIVE FIELDS
	%--------------------------------
	
	metadata.author = ''; % author of metadata, string or function handle (classifier)
	
	metadata.created = now; % creation date 
	
	metadata.modified = []; % modification date
	
	%--------------------------------
	% METADATA FIELDS
	%--------------------------------
	
	metadata.value = []; % metadata values computed
	
	metadata.parameter = []; % paremeters used in metadata computation
	
	%--------------------------------
	% USERDATA FIELDS
	%--------------------------------
	
	metadata.userdata = []; % system independent user data field
	
	%--
	% set persistent metadata
	%--
	
	METADATA_PERSISTENT = metadata;
	
else 
	
	%--
	% copy persistent metadata and update creation date
	%--
	
	metadata = METADATA_PERSISTENT;
	
	metadata.created = now;
	
end

%---------------------------------------------------------------------
% SET FIELDS IF PROVIDED
%---------------------------------------------------------------------

if (length(varargin))
	metadata = parse_inputs(metadata,varargin{:});
end
