function [status, result] = sqlite_cli(file, command)

% sqlite - file-based database engine access
% ------------------------------------------
%
% [status, result] = sqlite(file, command)
%
% Input:
% ------
%  file - database file
%  command - command
%
% Output:
% -------
%  status - status
%  result - result

% TODO: move to the MEX interface 

%--
% get tool
%--

tool = get_tool('sqlite3.exe');

if nargin && isempty(tool)
	error('Unable to find tool.');
end

% NOTE: return tool if we are called with no input

if ~nargin
	status = tool; result = []; return;
end

%--
% use tool
%--

str = ['"', tool.file, '" "', file, '"'];

if (nargin > 1) && ~isempty(command)
	str = [str, ' "', command, '"'];
end

[status, result] = system(str);




