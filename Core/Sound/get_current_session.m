function [ix, str] = get_current_session(sound, time)

%--
% handle input
%--

if nargin < 2 || isempty(time)
	slider = get_time_slider(get_active_browser); time = slider.value;
end

if nargin < 1 || isempty(sound)
	sound = get_active_sound;
end

ix = []; str = '';


if ~has_sessions_enabled(sound)
	return;
end

%--
% get real time
%--

time = map_time(sound, 'real', 'slider', time);

%--
% get sessions in real time
%--

sessions = get_sound_sessions(sound, 0);

if isempty(sessions)
	return;
end

starts = [sessions(:).start];

stops = [sessions(:).end];

%--
% find current session and output info string
%--

ix = find([time < stops], 1, 'first');

str = sec_to_clock(starts(ix));

