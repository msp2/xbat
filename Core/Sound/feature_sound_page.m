function page = feature_sound_page(page, context)

% feature_sound_page - "feature" a sound page using context
% ---------------------------------------------------------
%
% page = feature_sound_page(page, context)
%
% Input:
% ------
%  page - page to "feature"
%  context - context
%
% Output:
% -------
%  page - featured page

%--
% check if we need to compute features
%--

if isempty(context) || isempty(context.active.sound_feature)
	page.feature = []; return;
end

%--
% feature page
%--

feature = context.active.sound_feature;

for k = 1:length(feature)

	ext = feature(k);

	%--
	% compute
	%--

	page.feature(k).name = ext.name; page.feature(k).value = [];

	try
		[page.feature(k).value, context(k)] = ext.fun.compute(page, ext.parameter, context(k));
	catch
		extension_warning(feature(k), 'Compute failed.', lasterror);
	end

end