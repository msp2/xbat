function datetime = get_sound_datetime(sound, time)

% get_sound_datetime - get datetime from head time
% ------------------------------------------------
%
% datetime = get_sound_datetime(sound, time)
%
% Input:
% ------
%  sound - sound
%  time - head time
%
% Output:
% -------
%  datetime - datetime

% NOTE: this maps the head time (native event time store) to datetime of recording

time = map_time(sound, 'real', 'recording', time);

% NOTE: what should we do for empty realtime?

datetime = offset_date(sound.realtime, time);