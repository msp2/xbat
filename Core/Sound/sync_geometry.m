function geometry = sync_geometry(geometry, type, ref)

% sync_geometry - synchronize local and global geometry fields
% ------------------------------------------------------------
%
% geometry = sync_geometry(geometry, type)
%
% Input:
% ------
%  geometry - geometry struct
%  type - which reference frame to synchronize to
%
% Output:
% -------
%  geometry - geometry struct
%

if isempty(which('m_ll2xy'))
	return;
end

if nargin < 3 || isempty(ref)
	[ignore, ref] = min(sum(geometry.local.^2, 2));
end

if isempty(geometry.global) 
	
	if strcmp(type, 'global')
		return;
	end
		
	%--
	% create global geometry if we need to sync to local
	%--
		
	min_max_lon = [-1, 1]; min_max_lat = [-1, 1];
	
	geometry.global = zeros(size(geometry.local));
	
	geometry.offset = [0, 0, 0];
	
	geometry.ellipsoid = 'wgs84';
		
		
else

	min_max_lat = fast_min_max(geometry.global(:, 1));

	min_max_lon = fast_min_max(geometry.global(:, 2));
	
end

%--
% set up projection using current coordinates
%--

m_proj('utm', 'longitude', min_max_lon, 'latitude', min_max_lat, 'ellipsoid', geometry.ellipsoid);

%--
% synchronize
%--

switch type
	
	case 'global'
		
		%--
		% get utm coordinates
		%--
		
		[x y] = m_ll2xy(geometry.global(:, 2), geometry.global(:, 1));
		
		%--
		% compute new offset and store in local
		%--
		
		geometry.offset = [x(ref), y(ref)];
		
		ix = 1:length(x);
		
		geometry.local(ix, 1) = x - geometry.offset(1); 
		
		geometry.local(ix, 2) = y - geometry.offset(2);
		
	case 'local'
		
		%--
		% get coordinates with offset
		%--
		
		x = geometry.local(:, 1) + geometry.offset(1);
		
		y = geometry.local(:, 2) + geometry.offset(2);
		
		[lon, lat] = m_xy2ll(x, y);
		
		ix = 1:length(lat);
		
		% NOTE: there are some ambiguities regarding what happens when the
		% reference channel is edited.
		
		geometry.global(ix, 2) = lon; geometry.global(ix, 1) = lat;

end

