function [file, ix] = get_current_file(sound, time)

% get_current_file - get file at specific time
% --------------------------------------------
%
% [file, ix] = get_current_file(sound, time)
%
% Input:
% ------
%  sound - sound
%  time - slider time
%
% Output:
% -------
%  file - file at sound time
%  ix - index of file in sound files

%---------------------------
% HANDLE INPUT
%---------------------------

%--
% check input time to be non-negative
%--

if (time < 0)
	error('Sound times are always positive.');
end

%--
% single file sounds are special
%--

if ischar(sound.file)
	ix = 1; file = sound.file; return;
end

%---------------------------
% GET CURRENT FILE
%---------------------------

%--
% get file start sound times
%--

% NOTE: get_file_times.m was written to accept 'real' time

time = map_time(sound, 'real', 'slider', time);

start = get_file_times(sound);

%--
% locate time within file start times
%--

ix = find(time >= start,1,'last'); 

file = sound.file{ix};