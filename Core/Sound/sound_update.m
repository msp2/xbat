function sound = sound_update(sound, data)

% sound_update - update view fields of sound using browser structure
% ------------------------------------------------------------------
%
% sound = sound_update(sound, data)
%
% Input:
% ------
%  sound - sound structure
%  data - browser figure structure
%
% Output:
% -------
%  sound - updated sound structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 6221 $
% $Date: 2006-08-18 12:25:48 -0400 (Fri, 18 Aug 2006) $
%--------------------------------

% TODO: rename to sound_view_update

% NOTE: this function is temporary. keeping two copies of these fields is redundant

%--
% update view fields
%--

sound.view.channels = data.browser.channels;

sound.view.time = data.browser.time;

sound.view.page = data.browser.page;

sound.view.grid = data.browser.grid;

sound.view.colormap = data.browser.colormap;

%--
% update spectrogram computation fields
%--

sound.specgram = data.browser.specgram;

%--
% update modification date
%--

sound.modified = now;