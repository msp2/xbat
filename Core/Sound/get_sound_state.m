function state = get_sound_state(sound, lib)

% get_sound_state - get the browser state for a sound and library
% ----------------------------------------------------------------
%
% state = get_sound_state(sound, lib)
%
% Input:
% ------
%  sound - sound
%  lib - library
%  
% Output:
% -------
%  state - browser state

if nargin < 2
	lib = get_active_library;
end

out = sound_load(lib, sound_name(sound));

state = out.state;

