function delete_labelled_lines(ax,tag)

% delete_labelled_lines - what it says
% ------------------------------------
%
% delete_labelled_lines(ax,tag)
%
% Input:
% ------
%  ax - parent axes
%  tag - labelled lines tag

%--------------------
% HANDLE INPUT
%--------------------

%--
% normalize tag
%--

tag = upper(strtrim(tag));

%--
% handle multiple axes recursively
%--

if (numel(ax) > 1)
	
	for k = 1:numel(ax)
		delete_labelled_lines(ax(k),tag);
	end

	return;
	
end

%--------------------
% DELETE LINES
%--------------------

% TODO: consider making safer by using type of object in 'findobj'

tags = {[tag, '_LINE'], [tag, '_LABEL']};

for k = 1:length(tags)
	delete(findobj(ax,'tag',tags{k}))
end