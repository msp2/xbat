function format = get_readable_formats(format)

% get_readable_formats - get readable formats
% -------------------------------------------
%
% format = get_readable_formats(format)
%
% Input:
% ------
%  format - list of formats to evaluate
%
% Output:
% -------
%  out - readable formats

%--
% get all formats if needed
%--

if nargin < 1
	format = get_formats;
end

%--
% remove non-readable formats
%--

for k = length(format):-1:1
	if isempty(format(k).info) || isempty(format(k).read)
		format(k) = [];
	end
end
