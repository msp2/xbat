function out = decode_mp3(f1, f2)

% decode_mp3 - decode mp3 to wav
% ------------------------------
%
% com = decode_mp3(f1, f2)
%
% Input:
% ------
%  f1 - input file
%  f2 - output file
%
% Output:
% -------
%  com - command to execute to perform decoding

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 498 $
% $Date: 2005-02-03 19:53:25 -0500 (Thu, 03 Feb 2005) $
%--------------------------------

%----------------------------------------------------
% BUILD COMMAND STRING
%----------------------------------------------------

%--
% persistently  store location of command-line helper
%--

persistent LAME_PATH;

if isempty(LAME)
	LAME_PATH = [fileparts(mfilename('fullpath')), filesep, 'lame.exe'];
end

%--
% build full command string
%--

out = ['"', LAME_PATH, '" --decode "', f1, '" "', f2, '"'];
