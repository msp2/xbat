function format = get_writeable_formats(format)

% get_writeable_formats - get writeable formats
% -------------------------------------------
%
% format = get_writeable_formats(format)
%
% Input:
% ------
%  format - list of formats to evaluate
%
% Output:
% -------
%  out - writeable formats

%--
% get all formats if needed
%--

if nargin < 1
	format = get_formats;
end

%--
% remove non-writeable formats
%--

for k = length(format):-1:1
	if isempty(format(k).write)
		format(k) = [];
	end
end
