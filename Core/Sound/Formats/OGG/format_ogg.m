function format = format_ogg

% format_ogg - create format structure
% -----------------------------------
%
% format = format_ogg
%
% Output:
% -------
%  format - format structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 689 $
% $Date: 2005-03-09 22:14:37 -0500 (Wed, 09 Mar 2005) $
%--------------------------------

%--
% create format structure
%--

format = format_create;

%--
% fill format structure
%--

format.name = 'Ogg Vorbis (OGG)';

format.ext = {'ogg'};

format.home = 'http://www.vorbis.com/';

format.info = @info_ogg;

format.read = @read_ogg;

format.write = []; % @write_ogg;

format.encode = @encode_ogg;

format.decode = @decode_ogg;

format.seek = 0;

format.compression = 1;