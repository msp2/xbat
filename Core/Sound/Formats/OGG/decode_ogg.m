function out = decode_ogg(f1,f2)

% decode_ogg - decode an ogg to wav
% ---------------------------------
%
% com = decode_ogg(f1,f2)
%
% Input:
% ------
%  f1 - input file
%  f2 - output file
%
% Output:
% -------
%  com - command to execute to perform decoding

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 498 $
% $Date: 2005-02-03 19:53:25 -0500 (Thu, 03 Feb 2005) $
%--------------------------------

%----------------------------------------------------
% BUILD COMMAND STRING
%----------------------------------------------------

%--
% persistently  store location of command-line helper
%--

persistent OGGDEC;

if (isempty(OGGDEC))
	OGGDEC = [fileparts(mfilename('fullpath')), filesep, 'oggdec.exe'];
end

%--
% build full command string
%--

% NOTE: the 'Q' flag tries to make the decoding quiet

out = [ ...
	'"', OGGDEC, '" -Q --output="', f2, '" "', f1, '"' ...
];