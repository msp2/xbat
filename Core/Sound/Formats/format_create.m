function format = format_create(varargin)

% forrmat_create - create format structure
% -------------------------------------
%
%  format = format_create
%
% Output:
% -------
%  format - format structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 563 $
% $Date: 2005-02-21 05:59:20 -0500 (Mon, 21 Feb 2005) $
%--------------------------------

%---------------------------------------------------------------------
% CREATE FORMAT STRUCTURE
%---------------------------------------------------------------------

persistent FORMAT_PERSISTENT;

if (isempty(FORMAT_PERSISTENT))
	
	%--------------------------------
	% PRIMITIVE FIELDS
	%--------------------------------
	
	format.name = [];		% human readable name of format (equivalent to description for 'imformats')
	
	format.ext = cell(0);	% extensions associated with format

	% NOTE: it is not clear that this belongs here
	
	format.home = [];		% home page for format handler
	
% 	format.version = [];	% version of format handler
	
	%--------------------------------
	% FILE ACCESS AND CREATION
	%--------------------------------
		
	%--
	% matlab level functions
	%--
	
	format.config = [];		% format configuration controls
	
	format.info = [];		% get file info
	
	format.read = [];		% read file into samples
	
	format.write = [];		% write samples to file
	
	%--
	% file level operations
	%--
	
	format.encode = [];		% file to file encoding function (always to format)
	
	format.decode = [];		% file to file decoding function (always to wav)
	
	%--------------------------------
	% FORMAT INFORMATION
	%--------------------------------
	
	% NOTE: formats that do not support seeking are storage only
	
	format.seek = [];			% format HANDLER supports efficient seeking
		
	format.compression = [];	% format supports compression
	
else
	
	format = FORMAT_PERSISTENT;
		
end

%---------------------------------------------------------------------
% SET FIELDS IF PROVIDED
%---------------------------------------------------------------------

if (length(varargin))
	
	%--
	% try to get field value pairs from input
	%--
	
	format = parse_inputs(format,varargin{:});

end


