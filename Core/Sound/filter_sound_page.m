function page = filter_sound_page(page, context)

% filter_sound_page - filter a sound page using context
% -----------------------------------------------------
%
% page = filter_sound_page(page, context)
%
% Input:
% ------
%  page - page to filter
%  context - context
%
% Output:
% -------
%  page - filtered page

%--
% check if we need to filter
%--

if isempty(context) || isempty(context.active.signal_filter)
	page.filtered = []; return;
end

%--
% filter page
%--

% NOTE: the context page provides channels information

context.page = page;

page.filtered = apply_signal_filter(page.samples, context.active.signal_filter, context);