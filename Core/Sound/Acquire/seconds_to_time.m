function str = seconds_to_time(secs)

%----------------------------
% HANDLE INPUT
%----------------------------

%--
% handle array input recursively
%--

if (numel(secs) > 1)
	
	str = cell(size(secs));
	
	for k = 1:numel(secs)
		
		if (iscell(secs))
			sec = secs{k};
		else
			sec = secs(k);
		end
		
		str{k} = seconds_to_time(sec);
		
	end	
	
	return;	
	
end

%----------------------------
% COMPUTE TIME STRING
%----------------------------

%--
% compute minutes and seconds
%--

mins = floor(secs / 60); secs = secs - mins * 60;

%--
% get minutes and seconds label
%--

if (secs ~= 1)
	sec_str = 'seconds';	
else
	sec_str = 'second';
end

if (mins ~= 1)
	min_str = 'minutes';
else
	min_str = 'minute';
end

%--
% put together time string
%--

if (mins == 0)
	str = [num2str(secs) ' ' sec_str];
elseif (secs == 0)
	str = [num2str(mins) ' ' min_str];	
else
	str = [num2str(mins) ' ' min_str ', ' num2str(secs) ' ' sec_str];	
end

