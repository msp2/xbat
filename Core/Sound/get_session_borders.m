function borders = get_session_borders(sessions, time)

% get_session_borders - find nearest borders surrounding a set of times
% ---------------------------------------------------------------------
%
% borders = get_session_boundaries(sessions, time)
%
% Input:
% ------
%  sessions - session objects
%  time - time vector (usually 1 or two points)
%
% Output:
% -------
%  borders - nearest session boundaries
%
%

if isempty(sessions)
	borders = []; return;
end
	
session_ix = find([[sessions.end] > max(time)] & [[sessions.start] < min(time)]);

if isempty(session_ix)
	borders = []; return;
end

borders = [sessions(session_ix).start,sessions(session_ix).end];

	