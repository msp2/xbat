function attribute = sensor_calibration(file)

% sensor_calibration - read sensor_calibration file
% ----------------------------------
% attribute = sensor_calibration(file)
%
% Inputs:
% -------
% file - file with path
%
% Outputs:
% --------
% attribute - sensor calibration attribute

lines = file_readlines(file);

if isempty(lines)
	return
end

%--
% get reference from second line
%--

[ignore, reference] = strread(lines{2}, '%s%f', 'delimiter', ',');

%--
% create table by reading lines
%--

rows = length(lines) - 3;

calibration = zeros(rows, 1);

for k = 1:rows
	
	line = lines{k + 3};
	
	calibration(k) = strread(line, '%f', 1, 'delimiter', ',');
	
end

attribute.calibration = calibration;

attribute.reference = reference;

