function bytes = get_sound_bytes(sound)

% get_sound_bytes - get bytes from sound
% --------------------------------------
%
% bytes = get_sound_bytes(sound)
%
% Input:
% ------
%  sound - sound array
%
% Output:
% -------
%  bytes - bytes in each sound

%--
% get number of bytes for each sound
%--

% NOTE: if we cannot get the bytes from the sound we output zero, consider nan

bytes = zeros(size(sound));

for k = 1:numel(sound)
	
	if isfield(sound(k).info, 'bytes')
		bytes(k) = sound(k).info.bytes;
	end

end
