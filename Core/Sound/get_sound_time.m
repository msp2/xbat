function time = get_sound_time(sound, time, backwards)

% get_sound_time - get sound times from slider times
% --------------------------------------------------
%
% time = get_sound_time(sound, time)
%
% Input:
% ------
%  sound - sound
%  time - slider times
%
% Output:
% -------
%  time - sound time

if nargin < 3 || isempty(backwards)
	backwards = 0;
end

% NOTE: when scheduled sound display is not hiding silence slider time is sound time

if has_sessions_enabled(sound) && xor(sound.time_stamp.collapse, backwards)
	time = get_session_time(time, sound);
end
