function player = get_current_player

%--
% access global players
%--

global BUFFERED_PLAYER_1 BUFFERED_PLAYER_2;

%--
% set default empty player and test for playing or paused player
%--

player = [];

if is_playing_or_paused(BUFFERED_PLAYER_1)
	
	player = BUFFERED_PLAYER_1; return;
	
end

if is_playing_or_paused(BUFFERED_PLAYER_2)
	
	player = BUFFERED_PLAYER_2; return;
	
end


%--------------------------------
% IS_PLAYING_OR_PAUSED
%--------------------------------

function value = is_playing_or_paused(player)

% is_playing_or_paused - check for player
% ---------------------------------------
%
% value = is_playing_or_paused(player)
%
% Input:
% ------
%  player - player
%
% Output:
% -------
%  value - result of test (0 - no player, 1 - playing, 2 - paused)

%--
% check for availability
%--

if isempty(player)
	value = 0; return;
end

%--
% check for playing
%--

value = isplaying(player);

if value
	return;
end

%--
% check for paused
%--

current = get(player, 'currentsample'); 

total = get(player, 'totalsamples');

% NOTE: players reset to sample number 1 when done

value = 2 * ((current < total) && (current > 1));

