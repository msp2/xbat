function stop_cb(obj, eventdata)

%--
% access global players
%--

global BUFFERED_PLAYER_1 BUFFERED_PLAYER_2;

%--
% return if we were paused
%--

if get(obj, 'currentsample') ~= 1	
	return;
end 

%--
% select next player when we are done
%--

switch get(obj, 'tag')

	case 'BUFFERED_PLAYER_1'
		next_player = BUFFERED_PLAYER_2;

	case 'BUFFERED_PLAYER_2'
		next_player = BUFFERED_PLAYER_1;

end

% NOTE: clean up and return if there is no next player and the current player is done

if isempty(next_player)
	BUFFERED_PLAYER_1 = []; BUFFERED_PLAYER_2 = []; return;
end

%--
% start next player
%--

play(next_player);
