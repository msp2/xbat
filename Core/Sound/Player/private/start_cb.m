function start_cb(obj, eventdata)

%--
% access global players
%--

global BUFFERED_PLAYER_1 BUFFERED_PLAYER_2;

%--
% get and update player
%--

player = get(obj, 'userdata');

% NOTE: we only need to set the starting index for next player

player.bix = player.bix + player.buflen;

%--
% create next player if we need to
%--

if player.bix >= player.ix + player.n

	next_player = [];
	
else
	
	%--
	% get samples from sound
	%--
	
	X = get_data(player);
	
	player.buffer = X;
	
	%--
	% create player
	%--
	
	next_player = audioplayer(X, player.samplerate);
	
	set(next_player, ...
		'startfcn', @start_cb, ...
		'stopfcn', @stop_cb, ...
		'userdata', player ...
	);
	
end

%--
% set next global player
%--

switch get(obj, 'tag')

	case 'BUFFERED_PLAYER_1'
		set(next_player, 'tag', 'BUFFERED_PLAYER_2'); BUFFERED_PLAYER_2 = next_player;

	case 'BUFFERED_PLAYER_2'
		set(next_player, 'tag', 'BUFFERED_PLAYER_1'); BUFFERED_PLAYER_1 = next_player;

end
