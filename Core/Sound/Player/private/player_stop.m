function player_stop(player)

%--
% access global players
%--

global BUFFERED_PLAYER_1 BUFFERED_PLAYER_2;

%--
% stop and clear global players
%--

if ~isempty(BUFFERED_PLAYER_1)
	stop(BUFFERED_PLAYER_1); BUFFERED_PLAYER_1 = [];
end

if ~isempty(BUFFERED_PLAYER_2)
	stop(BUFFERED_PLAYER_2); BUFFERED_PLAYER_2 = [];
end
