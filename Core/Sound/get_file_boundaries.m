function [times, files, ix] = get_file_boundaries(sound, time, duration)

% get_file_boundaries - get sound file boundaries
% -----------------------------------------------
%
% [times, files, ix] = get_file_boundaries(sound, time, duration)
%
% Input:
% ------
%  sound - input sound
%  time - start of slider time interval
%  duration - duration of slider time interval
%
% Output:
% -------
%  times - boundary times
%  files - boundary files
%  ix - file indices

%--
% get all file start times and files
%--

% NOTE: this outputs a time for the end of the stream, remove it

times = get_file_times(sound); times(end) = [];

times = map_time(sound, 'slider', 'real', times); files = sound.file;

if ischar(files)
	files = {files};
end

%--
% select boundaries within interval
%--

% NOTE: we do not need the file boundary at the end of the interval

ix = find((times >= time) & (times < time + duration));

times = times(ix); files = files(ix);
		
