function settings_inference(sound,lib)

% settings_inference - infer settings for sound
% ---------------------------------------------
%
% sound = settings_inference(sound,lib)
%
% Input:
% ------
%  sound - sound structure
%  lib - library to look for settings (def: active library)
%
% Output:
% -------
%  sound - sound with settings

