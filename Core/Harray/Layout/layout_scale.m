function layout = layout_scale(layout, scale)

% layout_scale - scale layout
% ---------------------------
%
% layout = layout_scale(layout, scale)
%
% Input:
% ------
%  layout - layout
%  scale - scale
%
% Output:
% -------
%  layout - scaled layout

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 976 $
% $Date: 2005-04-25 19:27:22 -0400 (Mon, 25 Apr 2005) $
%--------------------------------

%---------------------------------------
% HANDLE INPUT
%---------------------------------------

%--
% set default scale
%--

if (nargin < 2) || isempty(scale)
	scale = 1;
end

%--
% check scale
%--

if ~isreal(scale) || (length(scale) > 1) || (scale <= 0)
	error('Layout scale must be a positive real number.');
end

%--
% handle multiple layouts recursively
%--

if length(layout) > 1
	
	for k = 1:length(layout)
		layout(k) = layout_scale(layout(k), scale);
	end 
	
	return;
	
end

%--
% return if the input and output scales are the same
%--

% NOTE: this would lead to a 1 scaling factor

if scale == layout.scale
	return;
end

%---------------------------------------
% SCALE LAYOUT
%---------------------------------------

% NOTE: layout scaling scales tool, status, and color bars and various margins

%--
% compute scaling factor and update scale
%--

fac = scale / layout.scale;

layout.scale = scale;

%--
% scale tool, status, and color bar sizes
%--

layout.tool.size = fac * layout.tool.size;

layout.status.size = fac * layout.status.size;

layout.color.size = fac * layout.color.size;

layout.color.pad = fac * layout.color.pad;

%--
% scale margins
%--

layout.margin = fac * layout.margin;

%--
% scale row and column padding
%--

layout.row.pad = fac * layout.row.pad;

layout.col.pad = fac * layout.col.pad;
