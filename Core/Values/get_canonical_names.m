function [names,values] = get_canonical_names(value)

% get_canonical_names - canonical names for struct fields
% -------------------------------------------------------
%
% [names,values] = get_canonical_names(value)
%
% Input:
% ------
%  value - struct
%
% Output:
% -------
%  names - canonical names
%  values - values corresponding to names

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 976 $
% $Date: 2005-04-25 19:27:22 -0400 (Mon, 25 Apr 2005) $
%--------------------------------

% NOTE: canonical names are fieldnames of flat structure

%--
% get canonical names
%--

flat = flatten_struct(value);

names = fieldnames(flat);

%--
% get corresponding values if needed
%--

if (nargout > 1)
	
	values = cell(length(names),1);
	
	for k = 1:length(names)
		values{k} = flat.(names{k});
	end
	
end