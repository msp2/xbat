function main = get_extension_main(ext)

% get_extension_main - get main extension file
% --------------------------------------------
%
% main = get_extension_main(ext)
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  main - main extension file

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

main = which(func2str(ext.fun.main));