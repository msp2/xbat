function out = extension_description_dialog(ext)

% extension_description_dialog - dialog for extension description edit
% --------------------------------------------------------------------
%
% out = extension_description_dialog(ext)
%
% Input:
% ------
%  ext - extension to edit
%
% Output:
% -------
%  out - dialog results

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1380 $
% $Date: 2005-07-27 18:37:56 -0400 (Wed, 27 Jul 2005) $
%--------------------------------

% NOTE: the extensions required should be a text that gets fed from popup

%----------------------------------
% CREATE DIALOG CONTROLS
%----------------------------------

% TODO: solve the problem of getting extension information into the dialog

% NOTE: we can inject extension information in the description file, description, or controls

% file = [tempname, '.xdf'];

control = generate_description_controls('extension_description.xdf');

%----------------------------------
% CREATE DIALOG
%----------------------------------

% NOTE: make the description header non-collapsible'

control(1).min = 1;

%--
% configure dialog options
%--

opt = dialog_group;

opt.width = 14;

opt.header_color = get_extension_color(ext);

%--
% create dialog
%--

% name = [ext.name, ' Description'];

name = 'Edit ...';

out = dialog_group(name, control, opt, @dialog_callback);

% TODO: consider handling values to update extension through merge


%-------------------------------------
% DIALOG_CALLBACK
%-------------------------------------

function dialog_callback(obj, eventdata);

% NOTE: this is useful when developing, it wraps 'get_callback_context' and 'control_update'

call = control_callback_tracer(obj);

	
