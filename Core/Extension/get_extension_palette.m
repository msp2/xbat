function pal = get_extension_palette(ext, par)

% get_extension_palette - get extension palette of parent
% -------------------------------------------------------
% 
% pal = get_extension_palette(ext, par)
%
% Input:
% ------
%  ext - extension
%  par - parent (def: active browser)
%
% Output:
% -------
%  pal - palette handle

%--
% initialize for convenience
%--

pal = [];

%--
% get parent
%--

% NOTE: we try to get the parent from the context input

if (nargin < 2) || isempty(par)
	par = get_active_browser;
end

% NOTE: return if we have no parent

if isempty(par)
	return;
end

%--
% try to get extension palette
%--

pals = get_xbat_figs('type', 'palette', 'parent', par);

if isempty(pals)
	return;
end

pal = findobj(pals, 'tag', get_extension_tag(ext));
