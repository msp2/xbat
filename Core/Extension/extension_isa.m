function value = extension_isa(ext, type)

% extension_isa - test whether extension is of a type
% ---------------------------------------------------
%
% value = extension_isa(ext, type)
%
% Input:
% ------
%  ext - extension
%  type - type
%
% Output:
% -------
%  value - type indicator

%--
% handle input
%--

if nargin < 2
	error('Extension type input is required.');
end

if ~is_extension_type(type)
	error('Unrecognized extension type.');
end

%--
% handle multiple extensions recursively
%--

if numel(ext) > 1
	
	value = zeros(size(ext));
	
	for k = 1:numel(ext)
		value(k) = extension_isa(ext(k));
	end
	
	return;
	
end

%--
% check extension is of type 
%--

% NOTE: there is a thin check for being an extension here

value = isstruct(ext) && isfield(ext, 'subtype') && strcmp(ext.subtype, type);