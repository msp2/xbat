function set_active_toggle_menu(pal, state)

%--
% user the state to find and set the top menu
%--

if state
	label = '(Off)'; update = '(On)';
else 
	label = '(On)'; update = '(Off)';
end

% NOTE: we only find the menu when we need to change it

top = findobj(pal, 'type', 'uimenu', 'tag', 'TOP_TOGGLE_MENU');

if isempty(top)
	return;
end

%--
% update menus
%--

set(top, 'label', update);

states = allchild(top);

set(states, 'check', 'off')

if state
	label = 'On';
else
	label = 'Off';
end

set(findobj(states, 'label', label), 'check', 'on');