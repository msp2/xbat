function update_extensions(verb)

% update_extensions - shortcut to updating system extensions
% ----------------------------------------------------------
%
% update_extensions(verb)
%
% Input:
% ------
%  verb - verbosity flag (def: 0)

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2098 $
% $Date: 2005-11-09 21:51:51 -0500 (Wed, 09 Nov 2005) $
%--------------------------------

if ~nargin
	verb = 0;
end

%--
% update extensions cache
%--

get_extensions('!');

if verb
	disp('Extensions cache updated.');
end

%--
% update extension menus
%--

update_filter_menu; 

update_detect_menu;

if verb
	disp('Extension menus updated.');
	disp(' ');
end
