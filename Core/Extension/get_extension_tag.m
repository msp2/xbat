function tag = get_extension_tag(ext)

% get_extension_tag - get tag for extension figure
% ------------------------------------------------
%
% tag = get_extension_tag(ext)
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  tag - tag for extension figure

% TODO: extend to handle widgets as well?

tag = ['XBAT_PALETTE::', upper(ext.subtype), '::', ext.name];