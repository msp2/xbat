function preset = preset_load(ext, name, type, context)

% preset_load - load preset from file
% -----------------------------------
%
% preset = preset_load(ext, name, type, context)
%
% Input:
% ------
%  ext - extension
%  name - preset name
%  type - preset type
%  context - context
%
% Output:
% -------
%  preset - preset or name

%-------------------------
% HANDLE INPUT
%-------------------------

if nargin < 4
	context = struct;
end

if nargin < 3
	type = '';
end

%-------------------------
% LOAD PRESET
%-------------------------

% TODO: implement custom preset load, consider file and directory presets

%--
% get preset file
%--

file = preset_file(ext, name, type);

if ~exist(file, 'file')
	error('Preset file does not seem to exist.');
end

%--
% load preset content based on type
%--

% NOTE: this loads the preset variable

load(file);

preset.name = name;


% NOTE: the context in this case is used to validate the content of the
% preset
