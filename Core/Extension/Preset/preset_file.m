function file = preset_file(ext, name, type)

% preset_file - get extension preset file name
% --------------------------------------------
%
% file = preset_file(ext, name, type)
%
% Input:
% ------
%  ext - extension
%  name - preset name
%  type - preset type
%
% Output:
% -------
%  file - preset file

%--
% handle input
%--

if nargin < 3
	type = '';
end

if ~proper_filename(name)
	error('Preset name must be a proper filename.');
end

%--
% get preset directory and full filename
%--

root = preset_dir(ext, type);

file = [root, filesep, name, '.mat'];