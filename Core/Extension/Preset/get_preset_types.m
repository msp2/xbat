function types = get_preset_types(ext)

% get_preset_types - get preset types for extension
% -------------------------------------------------
%
% types = get_preset_types(ext)
%
% Input:
% ------
%  ext - extension
%
% Output:
% -------
%  types - preset types

% NOTE: we assume all parameters have presets

%--
% get function names
%--

fun = fieldnames(flatten_struct(ext.fun)); 

%--
% get preset types from function names
%--

% NOTE: don't we need controls in order to have presets?

types = cell(0);

for k = 1:numel(fun)
	
	%--
	% check for parameter create type method
	%--
	
	ix1 = strfind(fun{k}, 'parameter__create');
	
	if isempty(ix1)
		continue;
	end
	
	% NOTE: we expect the match to be unique
	
	if (ix1 == 1)
		types{end + 1} = ''; continue;
	end
	
	ix2 = strfind(fun{k}, '_');
	
	% NOTE: this code has not been tested
	
	for j = numel(ix2):-1:1
		if (ix2(j) < ix1 - 2)
			types{end + 1} = fun{k}(ix2(j) + 1:ix1 - 3); continue;
		end
	end
	
	types{end + 1} = fun{k}(1:ix1 - 3);

end
