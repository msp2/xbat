function measurement = measurement_create(varargin)

% measurement_create - create measurement structure
% -------------------------------------------------
%
% measurement = measurement_create
%
% Output:
% -------
%  measurement - XBAT measurement (result) structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%---------------------------------------------------------------------
% CREATE MEASUREMENT STRUCTURE
%---------------------------------------------------------------------

persistent MEASUREMENT_PERSISTENT;

if (isempty(MEASUREMENT_PERSISTENT))

	%--------------------------------
	% PRIMITIVE FIELDS
	%--------------------------------
	
	measurement.name = ''; % name of measurement
	
	measurement.fun = ''; % function implementing measurement
	
	
	%--------------------------------
	% ADMINISTRATIVE FIELDS
	%--------------------------------
	
	measurement.author = ''; % author of measurement
	
	measurement.created = now; % creation date 
	
	measurement.modified = []; % modification date
	
	
	%--------------------------------
	% METADATA FIELDS
	%--------------------------------
	
	measurement.value = []; % measurement values computed
	
	measurement.parameter = []; % paremeters used in measurement computation
	
	
	%--------------------------------
	% USERDATA FIELDS
	%--------------------------------
	
	measurement.userdata = [];
	
	%--
	% set persistent measurement
	%--
	
	MEASUREMENT_PERSISTENT = measurement;
	
else 
	
	%--
	% copy persistent measurement and update creation date
	%--
	
	measurement = MEASUREMENT_PERSISTENT;
	
	measurement.created = now;
	
end


%---------------------------------------------------------------------
% SET FIELDS IF PROVIDED
%---------------------------------------------------------------------

if (length(varargin))
	measurement = parse_inputs(measurement,varargin{:});
end
