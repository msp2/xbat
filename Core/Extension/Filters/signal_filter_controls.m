function control = signal_filter_controls(ext,presets)

% signal_filter_controls - signal filter controls 
% -----------------------------------------------
%
% control = signal_filter_controls(ext,presets)
%
% Input:
% ------
%  ext - extension
%  presets - preset names
%
% Output:
% -------
%  control - control array

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1482 $
% $Date: 2005-08-08 16:39:37 -0400 (Mon, 08 Aug 2005) $
%--------------------------------

control = filter_controls(ext,presets);
