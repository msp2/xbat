function fun = sound_action

% sound_action - function handle structure
% ----------------------------------------
%
% fun = sound_action
%
% Output:
% -------
%  fun - structure for extension type API

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

fun.prepare = {{'result', 'context'}, {'parameter', 'context'}};

fun.compute = {{'result', 'context'}, {'sound', 'parameter', 'context'}};

fun.conclude = {{'result', 'context'}, {'parameter', 'context'}};

fun.parameter = param_fun;	