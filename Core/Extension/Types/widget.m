function fun = widget

% widget - function handle structure
% ----------------------------------
%
% fun = widget
%
% Output:
% -------
%  fun - structure for extension type API

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

% NOTE: a widget extension provides a parametrized display that listens to events

fun.layout = {{'handles'}, {'widget', 'parameter', 'context'}};

events = widget_events;

% NOTE: the context here will probably be used for paging

for k = 1:length(events)
	fun.on.(events{k}) = {{'handles', 'context'}, {'widget', 'data', 'parameter', 'context'}};
end

fun.on = collapse(fun.on);

fun.parameter = param_fun;
