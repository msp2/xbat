function fun = control_fun(in)

% control_fun - control functions structure
% -----------------------------------------
%
% fun = control_fun(in)
%
% Input:
% ------
%  in - name of controlled values
% 
% Output:
% -------
%  fun - function structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

if (nargin < 1)
	in = 'value';
end

% NOTE: the first cell contains output names, the second cell input names

fun.create = {{'control'}, {in, 'context'}};

fun.options = {{'opt'}, {'context'}};

fun.callback = {{'result'}, {'callback', 'context'}};
