function fun = sound_detector

% detector - function handle structure
% -------------------------------------
%
% fun = detector
%
% Output:
% -------
%  fun - structure for extension type API

% NOTE: the event is a simple event at the moment

fun.compute = {{'event', 'context'}, {'page', 'parameter', 'context'}};

fun.parameter = param_fun;

fun.explain = display_fun;
