function info = get_info(caller)

% get_info - get main file info
% -----------------------------
%
% info = get_info(caller)
%
% Output:
% -------
%  info - main file info if computable

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%--
% compute name from caller location
%--

% NOTE: this is the name of the parent directory of the caller

info = dir(which(caller.file));
