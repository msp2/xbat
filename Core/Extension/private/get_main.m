function main = get_main(caller)

% get_main - get main handle for extension by examining caller location
% ---------------------------------------------------------------------
%
% main = get_main(caller)
%
% Output:
% -------
%  main - handle to main extension function if computable

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

% NOTE: we build a function handle from the caller name

main = str2func(caller.name);