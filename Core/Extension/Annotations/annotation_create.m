function annotation = annotation_create(varargin)

% annotation_create - create annotation structure
% -----------------------------------------------
%
% annotation = annotation_create
%
% Output:
% -------
%  annotation - empty annotation structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%---------------------------------------------------------------------
% CREATE ANNOTATION STRUCTURE
%---------------------------------------------------------------------

persistent ANNOTATION_PERSISTENT;

if (isempty(ANNOTATION_PERSISTENT))
	
	%--------------------------------
	% PRIMITIVE FIELDS
	%--------------------------------
	
	annotation.name = ''; % name of annotation 
	
	annotation.fun = ''; % function implementing annotation
	
	
	%--------------------------------
	% ADMINISTRATIVE FIELDS
	%--------------------------------
	
	annotation.author = ''; % author of annotation
	
	annotation.created = now; % creation date
	
	annotation.modified = []; % modification date
	
	
	%--------------------------------
	% METADATA FIELDS
	%--------------------------------
	
	annotation.value = []; % annotation field values
	
	
	%--------------------------------
	% METADATA FIELDS
	%--------------------------------
	
	annotation.userdata = []; % userdata is not used by system
	
	
	%--
	% set persistent annotation
	%--
	
	ANNOTATION_PERSISTENT = annotation;
	
else 
	
	%--
	% copy persistent annotation and update creation date
	%--
	
	annotation = ANNOTATION_PERSISTENT;
	
	annotation.created = now;
	
end


%---------------------------------------------------------------------
% SET FIELDS IF PROVIDED
%---------------------------------------------------------------------

if (length(varargin))
	event = parse_inputs(event,varargin{:});
end