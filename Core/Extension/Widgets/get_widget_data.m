function data = get_widget_data(par, event, data)

% get_widget_data - get data for widget that handles event
% --------------------------------------------------------
%
% data = get_widget_data(par, event, data)
%
% Input:
% ------
%  par - widget parent
%  event - event data
%  data - parent state
%
% Output:
% -------
%  data - parent event data

%--------------------
% HANDLE INPUT
%--------------------

%--
% check event
%--

if ~ismember(event, get_widget_events)
	error('Unrecognized widget event.');
end

%--
% get parent data if needed
%--

if (nargin < 3) || isempty(data)
	data = get_browser(par);
end

%--------------------
% GET DATA
%--------------------

%--
% provide easy to use state data for widget
%--

% NOTE: some of these ideas will be integrated into state, so update is not required

sound = data.browser.sound;

% NOTE: is this currently done during context construction? in any case, it should!

sound.rate = get_sound_rate(sound);

page.start = data.browser.time;

page.duration = data.browser.page.duration;

page.channels = get_channels(data.browser.channels);

% NOTE: page we use in most parts of system does not include this field

page.freq = data.browser.page.freq;

if isempty(page.freq)
	page.freq = [0, 0.5 * sound.rate];
end

%--
% SELECTION
%--

%--
% pack state data
%--

data.sound = sound;

data.page = page;

data.selection = data.browser.selection;

[data.buffer, data.player] = get_player_buffer;

% TODO: provide structured spectrogram data, handles and options
		
data.spectrogram = data.browser.images;

%--
% page event specific data
%--

return;

% TODO: make this as dry as possible, 'events' should handle their own special requests

switch event
	
	case 'page'
		
	case 'timer'
	
	case 'play__start'
	
	case 'play__update'
		
	case 'play__stop'
		
	case 'selection__create'
		
	case 'selection__edit__start'
		
	case 'selection__edit__update'

	case 'selection__edit__stop'

end
