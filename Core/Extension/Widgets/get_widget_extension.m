function [ext, context] = get_widget_extension(widget)

% get_widget_extension - get widget extension and possibly context
% ----------------------------------------------------------------
%
% [ext, context] = get_widget_extension(widget)
%
% Input:
% ------
%  widget - widget
%
% Output:
% -------
%  ext - extension
%  context - context

%-
% get widget state
%--

data = get(widget, 'userdata');

%--
% get extension, and possibly context
%--

ext = data.opt.ext;

if nargout > 1
	
	context = data.context;
	
	% TODO: light update of context from parent if possible
	
end

