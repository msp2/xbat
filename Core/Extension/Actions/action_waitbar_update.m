function action_waitbar_update(pal, progress, result, type)

% action_waitbar_update - update function for action waitbar
% ----------------------------------------------------------
%
% action_waitbar_update(pal, progress, result, type)
%
% Input:
% ------
%  pal - waitbar handle
%  result - current target result

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

%------------------------
% PROGRESS
%------------------------

waitbar_update(pal, 'PROGRESS', 'value', progress);

%------------------------
% RESULTS
%------------------------

% NOTE: return if there are no results to report

if isempty(result)
	return;
end 

%--
% get listbox handle and string
%--

handles = get_control(pal, 'results', 'handles');

if isempty(handles.obj)
	return;
end

value = get(handles.obj, 'string');

if isempty(value)
	value = {};
end

%--
% update listbox string
%--

value = {value{:}, result_message(result, type)};

% NOTE: the value setting highlights the latest result string

set(handles.obj, ...
	'string', value, 'value', length(value) ...
);


%--------------------------------------------------------------
% RESULT_MESSAGE
%--------------------------------------------------------------

function str = result_message(result, type)

% result_message - produce a message string for an action result
% --------------------------------------------------------------
%
% str = result_message(result, type)
%
% Input:
% ------
%  result - action result
%  type - action type
%
% Output:
% -------
%  str - message string

%--
% display action name
%--

% NOTE: the action name is 'prepare', target name, or 'conclude'

if ischar(result.target)
	str = [upper(result.target), '  '];
else
	str = [get_action_target_name(result.target, type), '  '];
end

%--
% indicate action status in message
%--

switch result.status
		
	case {'undefined', 'failed'}
		
		str = [str, '(', upper(result.status), ')'];
		
	case 'done'
		
		if ~isempty(result.elapsed)
			
			if result.elapsed < 2
				str = [str, '(', num2str(result.elapsed, 4), ' sec)'];
			else
				str = [str, '(', sec_to_clock(result.elapsed), ')'];
			end
			
		end
		
end

%--
% append message to result if needed
%--

if ~isempty(result.message)
	str = [str, '  ', result.message];
end

