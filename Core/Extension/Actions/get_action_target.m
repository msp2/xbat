function target = get_action_target(type, callback)

% get_action_target - get targets for action from type and callback
% -----------------------------------------------------------------
%
% target = get_action_target(type, callback)
%
% Input:
% ------
%  type - type of action
%  callback - packed callback context
%
% Output:
% -------
%  target - action target array

%--
% get palette from callback context
%--

pal = callback.pal;

%--
% get targets from context
%--

switch type

	case 'sound'

		% NOTE: in this case we get sounds
		
		if strcmpi(pal.name, 'xbat')
			target = get_selected_sound(pal.handle);
		end
		
	case 'log'

		% NOTE: in this case we get log names not logs
		
		if strcmpi(pal.name, 'xbat')
			target = get_control(pal.handle, 'Logs', 'value');
		end
	
	case 'event'
	
		% NOTE: in this case we get event palette strings
		
		if strcmpi(pal.name, 'event')
			target = get_control(pal.handle, 'event_display', 'value');
		end
	
	otherwise

		error(['Unrecognized action type ''', type, '''.']);

end
