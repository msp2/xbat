function str = block_to_str(block, fun)

% block_to_str - scan block string
% --------------------------------
%
% str = block_to_str(block, fun)
%
% Input:
% ------
%  block - scan block
%  fun - time string function (def: @sec_to_clock)
%
% Output:
% -------
%  str - block string

%--
% set conversion function
%--

if nargin < 2
	fun = @sec_to_clock;
end

%--
% create string
%--

str = ['[', fun(block(1)), ', ', fun(block(2)), ']'];