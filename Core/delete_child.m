function delete_child(h,str)

% delete_child - delete child from screen and parent
% --------------------------------------------------
%
% delete_child(h,str)
%
% Input:
% ------
%  h - parent figure handle
%  str - child name

%--
% get parent userdata
%--

data = get(h,'userdata');

%--
% get child handle
%--

g = get_child(h,str,data);

if (isempty(g))
	return;
end

%--
% remove child handle from list and update userdata
%--

ix = find(data.browser.children == g);

data.browser.children(ix) = [];

set(h,'userdata',data);

%--
% close child figure
%--

closereq;