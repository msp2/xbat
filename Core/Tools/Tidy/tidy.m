function [status, result] = tidy(file, config, varargin)

%--------------------
% SETUP
%--------------------

tool = get_tool('tidy.exe');

%--------------------
% HANDLE INPUT
%--------------------

if nargin

	% NOTE: it is an error to use an unavailable tool
	
	if isempty(tool)
		error('Tool is not available.');
	end

else

	% NOTE: output info or call tool

	if nargout
		status = tool;
	else
		if ~isempty(tool)
			system(['"', tool.file, '" &']);
		end
	end
	
	return;

end

if nargin < 2
	config = [];
end 

%--------------------
% TIDY
%--------------------

%--
% create options string
%--

options = ' ';

for k = 1:length(varargin)
	options = [options, varargin{k}, ' '];
end

%--
% create config string
%--

if isempty(config)
	config = ' ';
else
	config = [' -config "', which(config), '" '];
end

%--
% create and execute full command string
%--

str = ['"', tool.file, '"', options, config, '"', file, '"'];

str = strrep(str, '  ', ' ');

% NOTE: tidy modifies the file in-place

[status, result] = system(str);

result = file_readlines(result);

% NOTE: this only displays when it finds errors

if status > 1
	disp(result{1});
end