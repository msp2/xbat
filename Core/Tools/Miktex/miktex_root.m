function root = miktex_root(type)

% miktex_root - get miktex root
% -----------------------------
%
% root = miktex_root(type)
%
% Input:
% ------
%  type - root type 'bin' or 'doc'
%
% Output:
% -------
%  root - root, empty if directory does not exist
% 
% NOTE: to see and change directories edit this file

%--
% set default root type
%--

if ~nargin
	type = 'bin';
end

%--
% set default root
%--

switch type
	
	case 'bin'
		root = 'C:\texmf\miktex\bin';
		
	case 'doc'
		root = 'C:\texmf\doc';
		
end

%--
% check for existence of root
%--

if ~exist_dir(root)
	root = '';
end
