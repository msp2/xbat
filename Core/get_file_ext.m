function ext = get_file_ext(str)

% get_file_ext - get file extensions from strings
% -----------------------------------------------
%
% ext = get_file_ext(str)
%
% Input:
% ------
%  str - strings containing filenames
%
% Output:
% -------
%  ext - file extensions

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

% this function should be private

%--
% filenames are stored in a cell array
%--

for k = 1:length(str)
	
	ix = findstr(str{k},'.');
				
	if (isempty(ix))
		ext{k} = '';
	else
		ext{k} = str{k}((ix(end) + 1):end);
	end
	
end