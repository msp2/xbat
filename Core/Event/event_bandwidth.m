function bandwidth = event_bandwidth(event)

% event_bandwidth - get event bandwidth in Hertz
% ----------------------------------------------
%
% bandwidth = event_bandwidth(event)
%
% Input:
% ------
%  event - event array
%
% Output:
% -------
%  bandwidth - event bandwidths

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 563 $
% $Date: 2005-02-21 05:59:20 -0500 (Mon, 21 Feb 2005) $
%--------------------------------

%--
% allocate and compute bandwidths
%--

bandwidth = zeros(size(event));

for k = 1:numel(event)

	switch (event.level)

		%--
		% simple event
		%--
		
		case (1)
			bandwidth(k) = diff(event.freq);
		
		%--
		% hierarchical event
		%--
		
		otherwise

			% NOTE: bandwidth for composite event is still not defined
			
	end
	
end