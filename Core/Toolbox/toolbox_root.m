function [root, exists] = toolbox_root(name, local)

% TOOLBOX_ROOT get root directory of named toolbox
%
% [root, exists] = toolbox_root(name, local)

%--
% handle input
%--

% NOTE: local means within the XBAT root directory structure

if nargin < 2
	local = 1;
end

if ~ischar(name)
	error('Toolbox name must be string.');
end

%--
% build toolbox root
%--

if local
	root = [toolboxes_root, filesep, name];
else
	root = [matlabroot, filesep, 'toolbox', filesep, name];
end

exists = exist_dir(root);
