function missing_toolbox_warning(name, local)

% MISSING_TOOLBOX_WARNING produce warning for missing toolbox
%
% missing_toolbox_warning(name, local)

% NOTE: local means local toolbox

%--
% put warning together using extension
%--

type_str = upper(strrep(ext.subtype,'_',' ')); 

name_str = upper(ext.name);

str = [' WARNING: In ', type_str, ' extension ''', name_str, '''. ', str];

n = length(str) + 1; line = str_line(n, '_');

%--
% display warning
%--

disp(' '); 

disp(line);
disp(' '); 
disp(str);
disp(line);

disp(' '); 

if (nargin > 2)

	disp(' MESSAGE:'); 
	disp(' ');
	if isempty(info.identifier)
		disp(['   ', info.message]);
	else
		disp(['   ', info.message, ' (', info.identifier, ')']);
	end

	disp(' ');
	
	disp(' SOLUTION:');
	disp(' ');
	
	% TODO: get solution info and display, for matlab toolboxes we have a uniform solution
	
	for k = 1:length(info.stack)
		disp(['   ', int2str(k), '. ', stack_line(info.stack(k))]);
	end
	
end

disp(' ');
disp(line);
disp(' ');

% str = ['In <a href="matlab:opentoline(''', stack.file, ''',', int2str(stack.line), ')">', '(xbat_root)', str, '</a>'];

	