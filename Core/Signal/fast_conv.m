function Y = fast_conv(X, h)

% fast_conv - fast convolution
% ----------------------------
% 
% Y = fast_conv(X, h)
%
% Input:
% ------
%  X - input signals as columns
%  h - filter
%
% Output:
% -------
%  Y - filtered signals as columns

%--
% handle input
%--

% TODO: generalize to handle filter bank input

h = h(:);

%--
% compute size
%--

% NOTE: consider using 'better_fft_sizes'

n = size(X, 1) + size(h, 1) - 1; N = pow2(nextpow2(n));

%--
% convolve in the frequency domain
%--

% NOTE: 'fft' pads signals as needed to match size input

X = fft(X, N); H = fft(h, N); 

Y = X .* (H * ones(1, size(X, 2)));

%--
% return to initial domain and select
%--

Y = real(ifft(Y, N)); Y = Y(1:n, :);


