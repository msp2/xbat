function rgb = hex_to_rgb(hex)

% hex_to_rgb - convert hexadecimal color strings to RGB matrix
% ------------------------------------------------------------
%
% rgb = hex_to_rgb(hex) 
%
% Input:
% ------
%  hex - cell array of hexadecimal color strings
%
% Output:
% -------
%  rgb - RGB matrix
%
% Example:
% --------
%  hex = rgb_to_hex(jet(16)); jet2 = hex_to_rgb(hex); jet(16) - jet2

%------------------
% HANDLE INPUT
%------------------

%--
% check input is string or string cell array
%--

% TODO: factor and extend in 'is_hex' function

if ischar(hex) 
	hex = {hex};
end 

if ~iscellstr(hex)
	error('Hexadecimal color input must be a string cell array.');
end

%------------------
% GET RGB VALUES
%------------------

N = numel(hex); rgb = nan(N, 3);

for k = 1:N
	
	rgbk = simple_hex_to_rgb(hex{k});
	
	if ~isempty(rgbk)
		rgb(k, :) = rgbk;
	end
	
end


%-----------------------------------
% SIMPLE_HEX_TO_RGB
%-----------------------------------

function rgb = simple_hex_to_rgb(hex)

rgb = []; hex = upper(hex);

% NOTE: return when length is wrong or string contains improper characters

if (length(hex) ~= 6) || any(~ismember(hex, '012345789ABCDEF'))
	return;
end

rgb = [hex2dec(hex(1:2)), hex2dec(hex(3:4)), hex2dec(hex(5:6))] / 255;

