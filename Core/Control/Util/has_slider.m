function value = has_slider(handles)

% has_slider - test handles array for sliders
% -------------------------------------------
%
% value = has_slider(handles)
%
% Input:
% ------
%  handles - handles array
%
% Output:
% -------
%  value - result of test

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1482 $
% $Date: 2005-08-08 16:39:37 -0400 (Mon, 08 Aug 2005) $
%--------------------------------

% TODO: generalize this function to test other styles and perhaps types

%--
% check for handles array
%--

if (any(~ishandle(handles)))
	error('Input array is not a handles array.');
end

%--
% select controls
%--

handles = handles(strcmp('uicontrol',get(handles,'type')));

% NOTE: there are no controls, hence no sliders 

if (isempty(handles))
	value = 0;
end

%--
% find slider
%--

styles = get(handles,'style');

% NOTE: string must be put in cell for 'ismember' to work properly

if (ischar(styles))
	styles = {styles};
end

if (~ismember('slider',styles))
	value = 0; return;
end 

value = 1;
