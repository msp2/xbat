function controls = get_palette_headers(pal)

% get_palette_headers - get palette header handles
% ------------------------------------------------
%
% header = get_palette_headers(pal)
%
% Input:
% ------
%  pal - palette handle


if ~is_palette(pal)
	error('Input handle is not palette handle.');
end

if nargin < 2
	name = ''; 
end

toggle = findobj(pal, 'tag', 'header_toggle');

if isempty(toggle)
	control = []; return;
end

controls = [];

for k = 1:length(toggle)
	
	control.label = '';
	
	control.handles.axes = get(toggle(k), 'parent');
	
	control.handles.toggle = toggle(k);
	
	control.handles.label = findobj(control.handles.axes, 'tag', 'header_text');
	
	if ~isempty(control.handles.label)
		control.label = get(control.handles.label, 'string');
	end
	
	if isempty(controls)
		controls = control; 
	else
		controls(end + 1) = control;
	end
	
end