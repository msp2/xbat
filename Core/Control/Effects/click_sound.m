function click_sound

% click_sound - generate a "click" (like the iPod wheel noise)
% ------------------------------------------------------------
%
% Usage: click_sound

return;

persistent data

%--
% return if there are no sounds
%--

value = get_env('palette_sounds');

if ~isequal(value,'on')
	return;
end

%--
% play sound
%--

if ~isempty(data)
	wavplay(data,'async');	return;
end

%--
% create sound
%--

data = randn(1, 100);

data = 0.04 * filter([1, -1], [1 0 -0.8], data);