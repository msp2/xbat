function tab_sound

% tab_sound - tab selection sound
% -------------------------------

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1156 $
% $Date: 2005-07-01 07:11:26 -0400 (Fri, 01 Jul 2005) $
%--------------------------------

%--
% return if palette sounds are not on
%--

if (~isequal(get_env('palette_sounds'),'on'))
	return;
end

%--
% create tab selection sound
%--

% TODO: make this configurable and load from file

n = 50;

x = 0.0075 * (sort(randn(1,n)) + 0.1 * randn(1,n));

%--
% play tab selection sound
%--

% NOTE: blocking play avoids multiple simultaneous play requests

sound(2 * x);