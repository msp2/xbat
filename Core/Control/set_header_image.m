function g = set_header_image(ax,color)

% set_header_image - set image in header axes
% -------------------------------------------
%
% g = set_header_image(ax,color)
%
% Input:
% ------
%  ax - axes to parent image
%  color - base color for image
%
% Output:
% -------
%  g - handle to header image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1000 $
% $Date: 2005-05-03 19:36:26 -0400 (Tue, 03 May 2005) $
%--------------------------------

%--
% get gradient image display state
%--

state = get_env('palette_gradient');

if (isempty(state))
	state = 'off'; set_env('palette_gradient',state);
end

%--
% return if gradient display is off
%--

if (strcmp(state,'off'))
	delete(findall(ax,'tag','header_image')); return;
end

%--
% create gradient image data
%--

for k = 1:3
	X(:,1,k) = (5 * color(k) + linspace(0.1,1,64)') / 6;
end

%--
% get image handle
%--

g = findall(ax,'tag','header_image');

% NOTE: create image if needed

if (isempty(g))
	g = image('parent',ax); set(ax,'layer','top');
end

%--
% update image properties
%--

set(g, ...
	'tag','header_image', ...
	'cdata',X, ...
	'xdata',[0,1], ...
	'ydata',[0,1], ...
	'hittest','off', ...
	'handlevisibility','off' ...
);