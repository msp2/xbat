function control = adjust_control_space(control, next)

%--
% handle input
%--

if nargin < 2
	return;
end 

% NOTE: this is the ugliest duckling ever, why even bother

if ~isfield(control, 'space')
	error('Control must have space field to adjust.');
end

if ~isfield(next, 'style')
	error('Next control does not have style field.');
end

%--
% adjust control
%--

switch next.style

	case {'axes', 'slider'}
		control.space = 1.5;

	case {'buttongroup'}
		control.space = 1;

	case {'edit', 'popup', 'checkbox'}
		control.space = 0.75;

	case 'separator'
		control.space = 1.5;

	case 'tabs'
		control.space = 0.12;

end


% FEATURE MENU

% switch (ext_controls(1).style)
% 	
% 	case ('separator')
% 
% 		if (~isempty(ext_controls(1).string))
% 			control(end).space = 1.25;
% 		end
% 
% 	case ('tabs'), control(end).space = 0.10;
% 
% 	case ('popup'), control(end).space = 0.75;
% 
% end

