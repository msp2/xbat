function register_palette(par,pal)

% register_palette - add palettes to browser palette list
% -------------------------------------------------------
%
% register_palette(par,pal)
%
% Input:
% ------
%  par - parent handle
%  pal - palette handles

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1482 $
% $Date: 2005-08-08 16:39:37 -0400 (Mon, 08 Aug 2005) $
%--------------------------------

% NOTE: we only modify the parent

%-------------------------
% HANDLE INPUT
%-------------------------

if ((nargin < 2) || isempty(pal))
	return;
end

if (ishandle(par) && strcmp(get(par,'type'),'figure'))
	data = get(par,'userdata');
else
	error('Parent input must be figure handle.');
end

%-------------------------
% REGISTER PALETTES
%-------------------------

%--
% ensure unique figure handles in palette list
%--

for k = 1:length(pal)
	
	if (ishandle(pal) && strcmp(get(pal,'type'),'figure'))
		data.browser.palette.handle(end + 1) = pal;
	end
	
end

data.browser.palette.handle = unique(data.browser.palette.handle);

%--
% update parent data
%--

set(par,'userdata',data);
