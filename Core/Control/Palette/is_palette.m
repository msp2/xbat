function [value, name] = is_palette(pal)

% is_palette - determine whether figure is palette
% ------------------------------------------------
%
% [value, name] = is_palette(pal)
%
% Input:
% ------
%  pal - figure handle
%
% Output:
% -------
%  value - result of palette test
%  name - name of palette figure

%--
% check for figure handles
%--

if ~is_handles(pal, 'figure')
	error('Input must be figure handles.');
end

%--
% test for palette tag
%--

% NOTE: uses convention of using 'DIALOG, 'PALETTE', 'WAITBAR', or 'WIDGET' in tag

tag = {'DIALOG', 'PALETTE', 'WAITBAR', 'WIDGET'}; value = 0;

for k = 1:length(tag)
	
	if ~isempty(findstr(get(pal, 'tag'), tag{k}))
		value = 1; break;
	end
	
end

%--
% return name for convenience
%--

if nargout > 1
	name = get(pal, 'name');
end
