function selected = selected_create(varargin)

% selected_create - create selected event set structure
% -----------------------------------------------------
% 
% selected = selected_create
%
% Output:
% -------
%  selected - XBAT selected event set structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%---------------------------------------------------------------------
% CREATE SELECTED STRUCTURE
%---------------------------------------------------------------------

persistent SELECTED_PERSISTENT;

if (isempty(SELECTED_PERSISTENT))

	%--------------------------------
	% PRIMITIVE FIELDS
	%--------------------------------
	
	selected.type = 'selected';
	
	selected.name = ''; % name of selected

	%--------------------------------
	% ADMINISTRATIVE FIELDS
	%--------------------------------
	
	selected.created = []; % selected set creation date
	
	selected.modified = ''; % selected set modification date
	
	selected.author = ''; % author of selected set
	
	%--------------------------------
	% RULE FIELDS
	%--------------------------------
	
	selected.rule = []; % required sound attributes
	
	%--------------------------------
	% EVENT FIELDS
	%--------------------------------
	
	selected.id = []; % selected event ids
	
	%--------------------------------
	% USERDATA FIELDS
	%--------------------------------
	
	selected.userdata = [];
	
	%--
	% set persistent selected
	%--
	
	SELECTED_PERSISTENT = selected;
	
else 
	
	%--
	% copy persistent selected
	%--
	
	selected = SELECTED_PERSISTENT;
		
end

%---------------------------------------------------------------------
% SET FIELDS IF PROVIDED
%---------------------------------------------------------------------

if (length(varargin))
	selected = parse_inputs(selected,varargin{:});
end
