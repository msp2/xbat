function obj = replace_tags(varargin)

% replace_tags - replace tags in tagged objects
% ---------------------------------------------
%
% obj = replace_tags(obj, tags, reps)
%
% Input:
% ------
%  obj - taggable objects
%  tags - tags to replace
%  reps - replacement tags
%
% Output:
% -------
%  obj - updated tagged objects

obj = update_tags('replace', varargin{:});
