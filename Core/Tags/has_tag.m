function value = has_tag(varargin)

% has_tag - test for tag in tagged objects
% ----------------------------------------
%
% value = has_tag(obj, tag)
%
% Input:
% ------
%  obj - tagged objects
%  tag - tag to consider
%
% Output:
% -------
%  value - result of tag test

value = update_tags('has', varargin{:});
