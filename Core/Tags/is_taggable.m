function value = is_taggable(in)

% is_taggable - check that input is taggable
% ------------------------------------------
%
% value = is_taggable(in)
%
% Input:
% ------
%  in - object to tag
%
% Output:
% -------
%  value - taggable object test result

%--
% strucures with a tags field are taggable
%--

value = 0;

if ~isstruct(in)
	return;
end

if isfield(in, 'tags')
	value = 1; return;
end

% NOTE: the following code is a future reminder, it is not currently used

return;

%--
% we can read tags if we pass this test
%--

% TODO: consider whether we can write tags

if ~isfield(in, 'type')
	value = 0; return;
end

type = in.type; accesors = {[type, '_tags'], ['get_', type, '_tags']}

for accessor = accesors
	
	info = functions(str2func(accessor));
	
	if ~isempty(info.file)
		value = 1; return;
	end
	
end

value = 0;
