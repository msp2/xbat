function obj = subtract_tags(varargin)

% subtract_tags - subtract tags from tagged objects
% -------------------------------------------------
%
% obj = subtract_tags(obj, tags)
%
% Input:
% ------
%  obj - tagged objects
%  tags - tags to subtract
%
% Output:
% -------
%  obj - updated tagged objects

obj = update_tags('subtract', varargin{:});
