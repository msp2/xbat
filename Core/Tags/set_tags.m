function obj = set_tags(varargin)

% set_tags - set tags of taggable objects
% ---------------------------------------
%
% obj = set_tags(obj, tags)
%
% Input:
% ------
%  obj - taggable objects
%  tags - tags
%
% Output:
% -------
%  obj - tagged objects

obj = update_tags('set', varargin{:});
