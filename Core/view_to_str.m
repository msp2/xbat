function str = view_to_str(opt)

% view_to_str - create command string for view options
% ----------------------------------------------------
% 
% str = view_to_str(opt)
%
% Input:
% ------
%  opt - spectrogram options
%
% Output:
% -------
%  str - command string to achieve spectrogram options

%--
% create string from options
%--

str = '%--\n% VIEW OPTIONS\n%--\n\n';

%--
% colormap options
%--

str = '%--\n% COLORMAP OPTIONS\n%--\n\n';

tmp = opt.colormap;

str = [str 'browser_view_menu(gcf,''Colormap::' tmp.name ''');\n'];
str = [str 'browser_view_menu(gcf,''Invert::' int2str(tmp.invert) ''');\n'];
str = [str 'browser_view_menu(gcf,''Auto Scale::' int2str(tmp.auto_scale) ''');\n'];

%--
% page options
%--

str = '%--\n% PAGE OPTIONS\n%--\n\n';

tmp = opt.page;

str = [str 'browser_view_menu(gcf,''Duration::' int2str(tmp.duration) ''');\n'];
str = [str 'browser_view_menu(gcf,''Overlap::' num2str(tmp.overlap) ''');\n'];
str = [str 'browser_view_menu(gcf,''Frequency Bounds ...::' mat2str(tmp.freq) ''');\n'];

%--
% grid options
%--

str = '%--\n% GRID OPTIONS\n%--\n\n';

tmp = opt.grid;

str = [str 'browser_view_menu(gcf,''Grid::' int2str(tmp.grid.on) ''');\n'];
str = [str 'browser_view_menu(gcf,''Time Grid::' int2str(tmp.grid.time.on) ''');\n'];
str = [str 'browser_view_menu(gcf,''Frequency Grid::' int2str(tmp.grid.freq.on) ''');\n'];
