function str = fig_name(str,h)

% fig_name - get and set fig name
% -------------------------------
%
% str = fig_name(h)
%     = fig_name(str,h)
%
% Input:
% ------
%  str - name string
%  h - figure handle (def: gcf)
%
% Output:
% -------
%  str - name string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-09-16 01:30:43-04 $
%--------------------------------

%--
% set figure handle
%--

if (nargin < 1)
	str = gcf;
end

%--
% set name string
%--

if isstr(str)

	% set figure handle
	
	if (nargin < 2)
		h = gcf;
	end
	
	% set name string
	
	data = get(h,'userdata');
	data.fig.name = str;
	set(h,'userdata',data);

%--
% get name string
%--

elseif ishandle(str)

	h = str;
	data = get(h,'userdata');
	str = data.fig.name;
	
end
