function h = ylabel_edit(s,str)

% ylabel_edit - create editable YLabel and get YLabel string
% ----------------------------------------------------------
% 
% h = ylabel_edit(s)
% s = ylabel_edit(h)
%
% Input:
% ------
%  s - YLabel string
%  h - handle to parent axes (def: gca)
%
% Output:
% -------
%  h - handle of YLabel
%  s - YLabel string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-09-16 01:30:52-04 $
%--------------------------------

%--
% handle variable input
%--

if (nargin < 1)
	
	% get YLabel and make editable
	
	h = get(gca,'YLabel');
	
	set(h,'ButtonDownFcn','ylabel_edit([],''Edit'');');
	% set(h,'erasemode','normal');
	
	% output string and return
	
	h = get(h,'string');
	
	return;
	
end	

if (nargin < 2)
	
	if (isstr(s))
	
		str = 'Initialize';
		
	elseif (ishandle(s))
	
		% get YLabel and make editable
	
		h = get(s,'YLabel');
		
		set(h,'ButtonDownFcn','ylabel_edit([],''Edit'');');
		% set(h,'erasemode','normal');
		
		% output string and return
		
		h = get(h,'string');
		
		return;
		
	end
		
end

%--
% main switch
%--

switch (str)

	case ('Initialize')
	
		h = ylabel(s);
		set(h,'ButtonDownFcn','ylabel_edit([],''Edit'');');
		% set(h,'erasemode','normal');
		
	case ('Edit')
	
		h = get(gca,'YLabel');
		set(h,'Color',[0, 0, 0]);
		set(h,'Editing','on');
		waitfor(h,'Editing');
		
end

%--
% set visible color
%--

if (all(get(gcf,'color') == [0, 0, 0]))
	set(get(gca,'YLabel'),'color',[1, 1, 1]);
end
