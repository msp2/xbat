function title_move(d,h)

% move_title - move all titles in figure
% --------------------------------------
%
% title_move(d,h)
%
% Input:
% ------
%  d - distance to move (negative is up)
%  h - parent figure handle

%--
% set figure
%--

if ((nargin < 2) | isempty(h))
	h = gcf;
end

%--
% return if no distance
%--

if ((nargin < 1) | isempty(d))
	return;
end

%--
% get axes titles in figure
%--

ax = findobj(h,'type','axes');
ti = zeros(size(ax));

for k = 1:length(ax)
	ti(k) = get(ax(k),'title');
end

%--
% modify position of titles in figure
%--

for k = 1:length(ti)
	pos = get(ti(k),'position');
	pos(2) = pos(2) + d;
	set(ti(k),'position',pos);
end


