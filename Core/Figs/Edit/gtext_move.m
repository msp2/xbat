function h = gtext_move(s,str,id)

% gtext_move - create movable gtext
% ---------------------------------
% 
% h = gtext_move(s)
%
% Input:
% ------
%  s - gtext string
%
% Output:
% -------
%  h - handle of text

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-09-16 01:30:51-04 $
%--------------------------------

%--
% set str
%--

if (nargin < 2)
	str = 'Initialize';
end

%--
% main switch
%--

switch (str)

	case ('Initialize')
	
		h = gtext(s);
		
		id = round((10^4)*rand(1));
		set(h,'UserData',id);
		
		set(h,'ButtonDownFcn',['gtext_move([],''Move'',' num2str(id) ')']);
		
	case ('Move')
	
		[x,y] = ginput(1);
		
		h = findobj(get(gca,'Children'),'UserData',id);
		
		set(h,'Position',[x,y]);
		set(h,'Editing','on');

end


