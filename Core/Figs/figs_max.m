function figs_max(h)

% figs_max - maximize figure windows
% ----------------------------------
%
% figs_max(h)
%
% Input:
% ------
%  h - handles to figures to maximize

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-09-16 01:30:50-04 $
%--------------------------------

%--
% get screensize
%--

ss = get(0,'screensize');

%--
% get and sort open figure handles
%--

if (~nargin)
	h = get(0,'children');
	h = sort(h);
end

%--
% maximize figure windows
%--

for k = 1:length(h)
	fig_sub(1,1,1,h(k));
end
