function h = figs_true(h)

% figs_true - display images pixel to pixel
% -----------------------------------------
% 
% h = figs_true(h)
%
% Input:
% ------
%  h - handles of figures to resize (def: open figures)
%
% Output:
% -------
%  h - handles of figures

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-09-16 01:30:51-04 $
%--------------------------------

%--
% get figure handles
%--

if ((nargin < 1) | isempty(h))
	h = get(0,'children');
end

%--
% get figures with image_menu and set normal size
%--

for k = 1:length(h)

	t = findobj(h(k),'type','uimenu','label','Image');
	
	if (~isempty(t))
		image_menu(h(k),'Normal Size');
	end
	
end

%--
% older code
%--

% 	for j = length(t):-1:1
% 		if (strcmp(get(t(j),'tag'),'TMW_COLORBAR'))
% 			t(j) = [];
% 		end
% 	end
% 
% 	if (~isempty(t))
% 		truesize(h(k),s*size(get(t,'CData')));
% 	end

	
		
