function str = fig_short(str,h)

% fig_short - get and set fig short caption
% -----------------------------------------
%
% str = fig_short(h)
%     = fig_short(str,h)
%
% Input:
% ------
%  str - short caption string
%  h - figure handle (def: gcf)
%
% Output:
% -------
%  str - short caption string

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-09-16 01:30:43-04 $
%--------------------------------

%--
% set figure handle
%--

if (nargin < 1)
	str = gcf;
end

%--
% set short caption string
%--

if isstr(str)

	% set figure handle
	
	if (nargin < 2)
		h = gcf;
	end
	
	% set short caption string
	
	data = get(h,'userdata');
	data.fig.short = str;
	set(h,'userdata',data);

%--
% get short caption string
%--

elseif ishandle(str)

	h = str;
	data = get(h,'userdata');
	str = data.fig.short;
	
end
