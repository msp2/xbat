function p = fig_path(d)

% fig_path - get and set figure save path
% ---------------------------------------
%
% p = fig_path(d)
%
% Input:
% ------
%  d - directory to set as local root
% 
% Output:
% -------
%  p - local image directory

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-09-16 01:30:43-04 $
%--------------------------------

%--
% set and get fig save path
%--

if (~nargin)
	try
		p = get_env('fig_path');
	catch
		set_env('fig_path','');
		p = get_env('fig_path');
	end
else
	set_env('fig_path',d);
	p = d;
end
