function [g,tag] = get_image_handles(h)

% get_image_handles - get image handles from figure
% -------------------------------------------------
%
% [g,tag] = get_image_handles(h)
%
% Input:
% ------
%  h - figure handle (def: gcf)
%
% Output:
% -------
%  g - image handles (not colorbars)
%  tag - image tags

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2003-07-06 13:36:52-04 $
% $Revision: 1.0 $
%--------------------------------

%--
% set figure
%--

if (nargin < 1)
	h = gcf;
end

%--
% get image handles
%--

g = findobj(h,'type','image');

%--
% remove colorbars if needed
%--

ix = find(strcmp(get(g,'tag'),'TMW_COLORBAR'));

if (~isempty(ix))
	g(ix) = [];
end

%--
% get tags of remaining images
%--

if (nargout > 1)
	tag = get(g,'tag');
end
