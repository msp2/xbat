function Y = image_rot90(X,n)

% image_rot90 - rotate image multiple of 90 degrees
% -------------------------------------------------
%
% Y = image_rot90(X,n)
%
% Input:
% ------
%  X - input image
%  n - multiple of 90 to rotate
%
% Output:
% -------
%  Y - rotated image

%--
% get effective n
%--

n = mod(n,4);

%--
% scalar or color  image
%--

d = ndims(X);

switch (d)

	case (2)
	
		switch (n)
		
			case (1)
				Y = X';
				Y = flipud(Y);
				
			case (2)
				Y = flipud(X);
				Y = fliplr(Y);
			
			case (3)
				Y = X';
				Y = fliplr(Y);
			
		end
			
	case (3)
		
		switch (n)
		
			case (1)
				Y = permute(X,[2 1 3]);
				Y = flipdim(Y,1);
				
			case (2)
				Y = flipdim(X,1);
				Y = flipdim(Y,2);
			
			case (3)
				Y = permute(X,[2 1 3]);
				Y = flipdim(Y,2);
			
		end
		
end
