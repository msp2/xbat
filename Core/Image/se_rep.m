function r = se_rep(SE)

% se_rep - representation type of structuring element
% ---------------------------------------------------
% 
% r = se_rep(SE)
%
% Input:
% ------
%  SE - structuring element
%
% Output:
% -------
%  r - representation type 'mat', 'vec', or ''

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2006-06-06 17:29:13 -0400 (Tue, 06 Jun 2006) $
% $Revision: 5169 $
%--------------------------------

%--
% get size of input
%--

[m,n] = size(SE);

%--
% displacement vectors
%--

if ((n == 2) && all(round(SE(:)) == SE(:)))
 
	r = 'vec';

%--
% matrix
%--

elseif (rem(m,2) && rem(n,2) && all((SE(:) ~= 0) == SE(:)))

	r = 'mat';

%--
% not a structuring element
%--
	
else

	r = '';
	
	if (~nargout)	
		warning('Input is not a structuring element.');
	end

end
