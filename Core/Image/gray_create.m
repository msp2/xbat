function X = gray_create(f)

% gray_create - create gray image structure
% -----------------------------------------
%
% X = gray_create(f)
%
% Input:
% ------
%  f - image filename (def: file dialog)
%
% Output:
% -------
%  X - image structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.1 $
% $Date: 2003-07-14 21:41:33-04 $
%--------------------------------

%--
% use load gray to get data
%--

if ((nargin < 1) | isempty(f))
	[data,file] = load_gray;
else
	[data,file] = load_gray(f);
end

%--
% pack image and file data into structure
%--

X.type = 'gray';
X.file = file.Filename;
X.data = data;
X.info = file;