function H = filt_comp(F,k)

% filt_comp - compose filter from decomposition
% ---------------------------------------------
%
% H = filt_comp(F,k)
%
% Input:
% ------
%  F - filter decomposition structure
%  k - order of approximation (def: 0, full composition)
%
% Output:
% -------
%  H - filter mask

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 335 $
% $Date: 2004-12-21 19:10:44 -0500 (Tue, 21 Dec 2004) $
%--------------------------------

%--
% set rank of approximation
%--

if (nargin < 2)
	k = 0;
end

%--
% check rank of approximation
%--

if (k > 0)
	if (k > length(F.S))
		k = 0;
	end
end

%--
% compute full or partial synthesis
%--

% NOTE: there is no need to compose full filter since the mask is stored

if (k == 0)
	H = F.H;
% 	H = F.Y * diag(F.S) * F.X';
else
	H = F.Y(:,1:k) * diag(F.S(1:k)) * F.X(:,1:k)';	
end