function SE = se_vec(SE)

% se_vec - displacement vector representation of structuring element
% ------------------------------------------------------------------
% 
% V = se_vec(SE)
%
% Input:
% ------
%  SE - structuring element
%
% Output:
% -------
%  V - displacement vector representation

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2006-06-06 17:29:13 -0400 (Tue, 06 Jun 2006) $
% $Revision: 5169 $
%--------------------------------

%----------------------------
% HANDLE INPUT
%----------------------------

%--
% check input representation type
%--

rep = se_rep(SE);

if isempty(rep)
	error('Input is not a structuring element.');
end

%--
% return if no conversion is needed
%--

if strcmp(rep, 'vec')
	return;
end

%----------------------------
% CONVERT
%----------------------------

% NOTE: structuring element is represented as matrix

%--
% compute center
%--

c = se_supp(SE) + 1;

%--
% compute displacement vectors
%--

[i, j] = find(SE); 

SE = [(i(:) - c(1)), (j(:) - c(2))];






