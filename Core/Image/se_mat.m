function SE = se_mat(SE)

% se_mat - matrix representation of structuring element
% -----------------------------------------------------
% 
% B = se_mat(SE)
%
% Input:
% ------
%  SE - structuring element
%
% Output:
% -------
%  B - matrix representation

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2006-06-06 17:29:13 -0400 (Tue, 06 Jun 2006) $
% $Revision: 5169 $
%--------------------------------

%----------------------------
% HANDLE INPUT
%----------------------------

%--
% check representation
%--

rep = se_rep(SE);

if isempty(rep)
	error('Input is not a structuring element.');
end

%--
% return if no conversion is needed
%--

if strcmp(rep, 'mat')
	return;
end

%----------------------------
% CONVERT
%----------------------------

% NOTE: structuring element is represented as displacement vectors

V = SE; 

%--
% get support and set center
%--

pq = se_supp(SE);

c = pq + 1;

%--
% fill structuring element matrix
%--

SE = zeros(2 * pq + 1);

for k = 1:size(V,1)
	SE(V(k,1) + c(1), V(k,2) + c(2)) = 1;
end


