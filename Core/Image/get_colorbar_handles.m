function h = get_colorbar_handles(g)

% get_colorbar_handles - get colorbar axes handles
% ------------------------------------------------
%
% h = get_colorbar_handles(g)
%
% Input:
% ------
%  g - figure handle (def: gcf)
%
% Output:
% -------
%  h - handles to colorbar axes

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2003-02-18 17:43:20-05 $
% $Revision: 1.24 $
%--------------------------------

%--
% set figure
%--

if (nargin < 1)
	g = gcf;
end

%--
% get colorbar axes handles
%--

h = findobj(g,'type','axes','tag','Colorbar');