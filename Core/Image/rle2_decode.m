function X = rle2_decode(R,L)

% rle2_decode - run-length decoding and labelling of binary image
% ---------------------------------------------------------------
%
% R = rle2_decode(X,L)
%
% Input:
% ------
%  R - run-length code
%  L - run-length based label look-up table
%
% Output:
% -------
%  X - binary or labelled image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
% $Revision: 132 $
%--------------------------------

% NOTE: some error checking would be needed for general use

if (nargin < 2)
	
	X = rle2_decode_(R);
	
else
	
	% NOTE: the direct labelling of runs is not exposed here
	
	X = rle2_decode_(R,L,0);
	
end