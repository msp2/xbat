function s = se_size(SE)

% se_size - number of pixels in structuring element
% -------------------------------------------------
% 
% s = se_size(SE)
%
% Input:
% ------
%  SE - structuring element
%
% Output:
% -------
%  s - number of pixels

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
% $Revision: 132 $
%--------------------------------

%--
% compute according to representation type
%--

switch (se_rep(SE))

	case ('mat')
		s = nnz(SE);
		
	case ('vec')
		s = size(SE,1);
	
end
