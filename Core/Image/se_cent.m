function m = se_cent(SE)

% se_cent - centroid of structuring element
% -----------------------------------------
%
% m = se_cent(SE)
%
% Input:
% ------
%  SE - structuring element
%
% Output:
% -------
%  m - centroid

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
% $Revision: 132 $
%--------------------------------

%--
% put structuring element in vector form
%--

V = se_vec(SE);

%--
% compute centroid
%--

m = sum(V,1)./se_size(V);

