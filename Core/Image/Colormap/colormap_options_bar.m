function h = colormap_options_bar(g)

% colormap_options_bar - colormap options toolbar
% -----------------------------------------------
%
% h = colormap_options_bar(g)
%
% Input:
% ------
%  g - handle to figure to control
%
% Output:
% -------
%  h - handle to toolbar figure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2003-02-18 17:43:20-05 $
% $Revision: 1.24 $
%--------------------------------

%--
% set figure to control and ensure that it has image_menu
%--

if (nargin < 1)
	g = gcf;
end

figure(g); image_menu;

%--
% create toolbar
%--

h = figure;

T = {'Brighten colormap (Ctrl+B)', ...
	'Darken colormap (Ctrl+D)', ...
	'Reverse colormap values (Ctrl+R)', ...
	'Toggle colorbar (Ctrl+C)' ...
};

button_group(h, ...
	'image_menu', ...
	'Colormap Options', ...
	{'Brighten','Darken','Invert','Colorbar'}, ...
	[],{'B','D','R','C'},g,T);

%--
% close toolbar when controlled figure is closed
%--

close_fun = get(g,'CloseRequestFcn');
set(g,'CloseRequestFcn',['delete(' num2str(h) '); ' close_fun]);
