function C = cmap_tint(c,n)

% cmap_tint - tinted monochrome colormap
% --------------------------------------
% 
% C = cmap_tint(c,n)
% 
% Input:
% ------
%  c - tint color
%  n - number of levels
%  
% Output:
% -------
%  C - colormap

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
% $Revision: 132 $
%--------------------------------

%--
% set number of levels
%--

if (nargin < 2)
	n = 256;
end

%--
% set color
%--

if (nargin < 1)
	c = uisetcolor([1 0 0],'Select Tint Color:');
end

%--
% set colormap of current figure
%--

if (~nargout)
	colormap(cmap_tint(c,n));
end

%--
% create colormap by varying saturation
%--

c = rgb_to_hsv(c);

x = linspace(0,1,n)';
o = ones(n,1);

C = [c(1)*o, x, c(3)*x];
C = hsv_to_rgb(C);
