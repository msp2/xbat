function C = cmap_invert(h)

% cmap_invert - invert colormap
% -----------------------------
%
% C = cmap_invert(C)
%   = cmap_invert(h)
%
% Input:
% ------
%  C - colormap
%  h - handle to figure (def: gcf)

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
% $Revision: 132 $
%--------------------------------

%--
% set figure
%--

if (nargin < 1)
	h = gcf;
end

%--
% invert colormap
%--

if (ishandle(h))
	C = flipud(get(h,'colormap'));
	set(h,'colormap',C);
else
	C = flipud(C);
end