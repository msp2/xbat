function Y = stack_shift(X,n)

% stack_shift - shift cells in stack
% ----------------------------------
%
% Y = stack_shift(X,n)
%
% Input:
% ------
%  X - input stack
%  n - shift
%
% Output:
% -------
%  Y - shifted stack

%--
% get effective shift
%--

nf = length(X);
n = mod(n,nf);

%--
% shift stack
%--
n
if (n > 0)
	pi, (nf - n):nf, 1:n
	Y = {X((nf - n):nf), X(1:n)};
else
	n:nf, 1:n
	Y = {X(n:nf), X(1:n)};
end
