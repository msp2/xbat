function [p,q] = se_supp(SE)

% se_supp - support parameters of structuring element
% ---------------------------------------------------
%
% [p,q] = se_supp(SE)
%
% Input:
% ------
%  SE - structuring element
%
% Output:
% -------
%  p - row support or both row and column support
%  q - column support

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 5169 $
% $Date: 2006-06-06 17:29:13 -0400 (Tue, 06 Jun 2006) $
%--------------------------------

%--
% get support depending on representation
%--

switch (se_rep(SE))

	case ('vec'), p = max(abs(SE), [], 1);
		
	case ('mat'), p = (size(SE) - 1) / 2;
			
end

%--
% separate support parameters if needed
%--

if (nargout > 1)
	q = p(2); p = p(1);
end
