function p = save_path(d)

% save_path - get and set default save path
% -----------------------------------------
%
% p = save_path(d)
%
% Input:
% ------
%  d - directory to set as default save
% 
% Output:
% -------
%  p - default save directory

if (~nargin)
	try
		p = get_env('save_path');
	catch
		set_env('save_path','');
		p = get_env('save_path');
	end
else
	set_env('save_path',d);
	p = d;
end