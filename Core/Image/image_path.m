function p = image_path(d)

% image_path - get and set image data path
% ----------------------------------------
%
% p = image_path(d)
%
% Input:
% ------
%  d - directory to set as local root
% 
% Output:
% -------
%  p - local image directory

if (~nargin)
	try
		p = get_env('image_path');
	catch
		set_env('image_path','');
		p = get_env('image_path');
	end
else
	set_env('image_path',d);
	p = d;
end
