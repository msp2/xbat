function t = is_rgb(X)

% is_rgb - check for rgb image
% ----------------------------
%
% t = is_rgb(X)
%
% Input:
% ------
%  X - input image
%
% Output:
% -------
%  t - rgb indicator

%--
% check dimensions and positivity
%--

if ((ndims(X) == 3) & ~(any(X(:) < 0)))
	t = 1;
else
	t = 0;
end
