function SE = se_tr(SE1)

% se_tr - transpose structuring element
% -------------------------------------
% 
% SE = se_tr(SE1)
%
% Input:
% ------
%  SE1 - structuring element
%
% Output:
% -------
%  SE - transposed structuring element

%--
% compute according to representation
%--

switch (se_rep(SE1))

	case ('mat')
		SE = flipud(fliplr(SE1));
		
	case ('vec')
		SE = -SE1;
		
end
