function SE = se_line(v,n)

% se_line - line structuring element
% ----------------------------------
% 
% SE = se_line(v)
%    = se_line(v,n)
%
% Input:
% ------
%  v - direction vector
%  n - periodic length 
%
% Output:
% -------
%  SE - structuring element

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
% $Revision: 132 $
%--------------------------------

%--
% compute according to type
%--

switch (nargin)

	%--
	% bressenham line
	%--
	
	case (1)
	
		v = v(:)';
		s = round([linspace(0,v(1))' linspace(0,v(2))']);
		SE = unique(s,'rows');
	
	%--
	% periodic line
	%--
	
	case (2)
	
		v = v(:)';
		SE = [0 0; diag(1:(n - 1))*repmat(v,(n - 1),1)];
		
end
