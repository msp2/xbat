function SEQ = se_build_decomp(SE,D)

% se_build_decomp - build structuring element decomposition
% ---------------------------------------------------------
%
% SEQ = se_build_decomp(SE,D);
%
% Input:
% ------
%  SE - structuring element to decompose
%  D - dictionary integers
%
% Output:
% -------
%  SEQ - structuring element decomposition

%--
% set dictionary
%--

if ((nargin < 2) | isempty(D))
	D = [17, 24, 26, 50, 58, 80, 144, 154, 156, 176, 178, 184, 186];
end

%--
% create structuring element library
%--

B = zeros(3,3,length(D));

for k = 1:length(D)
	
	b = fliplr(double(dec2bin(D(k))) - 48);
	b = [b, zeros(1,9 - length(b))];
	
	B(:,:,k) = reshape(b,3,3);
	
end

B

%--
% get size of input structuring element
%--

A = size(SE);

N = se_size(SE);

%--
% search through compositions
%--


SEQ = SE;

for j = 1:length(D)
	
	j
	
	B1 = B(:,:,j);
	
	for k = 1:length(D)
		
		B2 = B(:,:,k);
		
		B12 = se_comp(B1,B2);
		
		if (any(size(B12) > A) | (se_size(B12) > N))
			
			continue;
			
		else
			
			if (isempty(se_diff(SE,B12)))
				
				SEQ = {B1,B2};
				
			else
				
				for l = 1:length(D)
					
					B3 = B(:,:,l);
					
					B123 = se_comp(B12,B3);
					
					
					fig; se_view(B123); pause(0.1); close;
					
					
					if (any(size(B123) > A) | (se_size(B123) > N))
		
						continue;
						
					else
						
						if (isempty(se_diff(SE,B12)))
							
							SEQ = {B1,B2,B3};
							
						else
							
							continue;
							
						end
						
					end
					
				end
				
			end
			
		end
		
	end 
	
end

