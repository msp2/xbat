function Y = rgb_reshape(X,m,n)

% rgb_reshape - reshape rgb columns into rgb image
% ------------------------------------------------
%
% Y = rgb_reshape(X,m,n)
%   = rgb_reshape(X,m)
% 
% Input:
% ------
%  X - rgb columns
%  m - rows, or rows and columns
%  n - columns
% 
% Output:
% -------
%  Y - rgb image

%--
% set rows and columns
%--

if (nargin < 3)
	n = m(2);
	m = m(1);
end

%--
% check rgb columns
%--

if (size(X,2) ~= 3)
	error('Input image is not in pixel row format.');
end

%--
% reshape image
%--

Y = zeros(m,n,3);

for k = 1:3
	Y(:,:,k) = reshape(X(:,k),m,n);
end
