function Y = gray_to_rgb(X,C,opt)

% gray_to_rgb - convert a grayscale image to rgb using a colormap
% ---------------------------------------------------------------
%
% Y = gray_to_rgb(X,C,opt)
%
% Input:
% ------
%  X - input grayscale image
%  C - colormap matrix
%  opt - integer output option
%
% Output:
% -------
%  Y - rgb image

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--------------------------------------------------
% HANDLE INPUT
%--------------------------------------------------

%--
% set defaut output
%--

if ((nargin < 3) || isempty(opt))
	opt = 0;
end

%--
% set and check colormap
%--

if ((nargin < 2) || isempty(C))
	C = gray(256);
end

if ((size(C,2) ~= 3) || any(~isreal(C)))
	disp(' ');
	error('Colormap matrix must be real and have three columns.');
end

b = fast_min_max(C);

if ((b(1) < 0) || (b(2) > 1))
	disp(' ');
	error('RGB values in colormap matrix must be between zero and one.');
end

%--
% check that image has single plane
%--

if (ndims(X) > 2)
	disp(' '); 
	error('Only scalar images are supported.');
end

%--
% convert complex images to abs
%--

if (any(~isreal(X)))
	
	X = abs(X);
	
	disp('  ');
	warning('Converting complex image to norm image.');
	
end

%--------------------------------------------------
% CREATE RGB IMAGE
%--------------------------------------------------

%--
% allocate output image
%--

[m,n] = size(X);

Y = zeros(m,n,3);

%--
% use colormap as lookup table
%--

b = fast_min_max(X);

for k = 1:3
	Y(:,:,k) = lut_apply(X,C(:,k),b);
end

%--
% optionally output uint8 image
%--

if (opt) 
	Y = uint8(lut_range(Y));
end