function Y = rgb_vec(X)

% rgb_vec - reshape RGB image into pixel row format
% -------------------------------------------------
%
% Y = rgb_vec(X)
%
% Input:
% ------
%  X - RGB image
%
% Output:
% -------
%  Y - image in pixel row form

%--
% check size of input image
%--

[m,n,d] = size(X);

if (d ~= 3)
	disp(' ');
	error('Input image does not have three channels.');
end

%--
% put image in pixel row form
%--

for k = 1:3
	tmp = X(:,:,k);
	Y(:,k) = tmp(:);
end