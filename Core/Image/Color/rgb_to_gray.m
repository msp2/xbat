function Y = rgb_to_gray(X)

% rgb_to_gray - rgb to grayscale image
% ------------------------------------
%
% Y = rgb_to_gray(X)
%
% Input:
% ------
%  X - rgb image
%
% Output:
% -------
%  Y - grayscale image

%--
% get size of input
%--

[m,n,d] = size(X);

if (d ~= 3)
	error('Input image is not a multiple channel image.');
end

%--
% make double image
%--

X = double(X);

%--
% rgb to ??? transformation
%--

V = [ ...
	0.393  0.365  0.192; ...
	0.212  0.701  0.087; ...
	0.019  0.112  0.958 ...
];

%--
% apply transformation
%--

Y = reshape(rgb_vec(X)*V(2,:)',m,n);
