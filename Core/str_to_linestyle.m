function  str = str_to_linestyle(lt,opt)

% str_to_linestyle - linestyle name of linetype string
% ----------------------------------------------------
%
% str = str_to_linestyle(lt,opt)
%
% Input:
% ------
%  lt - linestyle string
%  opt - linestyle set option 'strict' or 'loose' (def: 'loose')
%
% Output:
% -------
%  str - linestyle name

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% set linestyle set option
%--

if ((nargin < 2) | isempty(opt))
	opt = 'loose';
else
	ix = find(strcmp(opt,{'strict','loose'}));
	if (isempty(ix))
		disp(' ');
		error('Unrecognized linestyle set option.');
	end
end

%--
% create name color cell array table
%--

% loose interpretation of linestyle, may lead to warning

if (strcmp(opt,'loose'))
	
	T = { ...
		'Solid','-'; ...
		'Dash','--'; ...
		'Dash-Dot','-.'; ...
		'Dot',':'; ...
		'Cross','x'; ...
		'Plus','+'; ...
		'Star','*'; ...
		'Circle','o';...
		'Diamond','d'; ...
		'Square','s'; ...
		'Triangle (Down)','v'; ...
		'Triangle (Up)','^'; ...
		'Triangle (Left)','<' ...
	};

% strict intepreration of linestyle

else
	
	T = { ...
		'Solid','-'; ...
		'Dash','--'; ...
		'Dash-Dot','-.'; ...
		'Dot',':' ...
	};
	
end

if (nargin & ~isempty(lt))
	
	%--
	% look up linestyle
	%--
	
	ix = find(strcmp(lt,T(:,2)));
	
	if (~isempty(ix))
		str = T{ix,1};
	else
		str = '';
	end
	
else
	
	str = '';
	
end

