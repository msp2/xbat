function update_logs_event_id

% update_logs_event_id - add event_id field to logs
% -------------------------------------------------

% TODO: make this into a general log update function and rename

%--
% get unique libraries
%--

% NOTE: this gets all libraries currently linked through a user

libs = get_unique_libraries;

%--
% create temporary log to get new structure
%--

% NOTE: this log should have the 'event_id' field

temp = temp_log;

%--
% loop over libraries
%--

for k = 1:length(libs)
	
	str = libs(k).path;
	
	disp(' '); 
	disp(str_line(length(str)));
	disp(str);
	disp(str_line(length(str)));
	
	%--
	% get library log names
	%--
	
	logs = get_library_logs('file',libs(k));
	
	if (isempty(logs))
		continue;
	end
	
	%--
	% loop over logs
	%--
	
	for j = 1:length(logs)
		
		%--
		% load log
		%--
		
		log = log_load(logs{j});
		
		%--
		% update log
		%--
		
		if (isfield(log,'event_id'))
			disp(logs{j}); 
			continue;
		end
		
		log.event_id = 1;
		
		log = orderfields(log,temp); 
		
		%--
		% save log
		%--
		
		log_save(log);
		
		disp(['* ', logs{j}]);
		
	end
	
end

disp(' ');
