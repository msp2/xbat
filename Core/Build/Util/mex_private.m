function varargout = mex_private(varargin)

% mex_private: call build_mex, and put files where they need to be according
% to the xbat idiom of dir/MEX/foo.c --> dir/private/foo.dll
%
% M. Robbins
%
%

d1 = dir;
build_mex(varargin{:});
d2 = dir;

fn = '';
for ix = 1:length(d2)
	if ~d2(ix).isdir	
		if isempty(strfind([d1.name], d2(ix).name))
			fn = d2(ix).name;
			break
		end
	end
end

% TODO: only clear the function being compiled

clear('functions');

movefile(fn, ['..', filesep, 'private']);

