function c = auto_calibrate(cl)

% auto_calibrate - compute a reasonable calibration value
% -------------------------------------------------------
%
% c = auto_calibrate(cl)
%
% Input:
% ------
%  cl - decibel range for channel (one per row)
%
% Output:
% -------
%  c - calibration values for each channel

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% get maximum decibel for all channels
%--

D = max(cl(:,2));

%--
% set calibration values to match this maximum in other channels
%--

c = D - cl(:,2);

%--
% ignore channels that seem to be empty
%--

ix = find(cl(:,2) < -10);
c(ix) = 0;