function html_close(fid)

% html_close - output body and html close
% ---------------------------------------
%
% html_close(fid)
%
% Input:
% ------
%  fid - output file identifier

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2014 $
% $Date: 2005-10-25 17:43:52 -0400 (Tue, 25 Oct 2005) $
%--------------------------------

%--
% send template lines to output
%--

html_template(fid, 'html_close.html');
