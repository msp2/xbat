function [page, k] = get_page_by_name(pages, name)

% GET_PAGE_BY_NAME get named page from pages 
%
% [page, k] = get_page_by_name(pages, name)

[page, select] = struct_select(pages, 'name', name);

k = find(select, 1);