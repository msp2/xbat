function link = page_link(site, page, target, varargin)

% PAGE_LINK link to site page
%
% link = page_link(site, page, target, varargin)

%--
% get build root relative name for target
%--

name = strrep(page_file(site, target), [build_root(site), filesep], '');

url = [level_prefix(page.level), strrep(name, '\', '/')];

%--
% get attributes and options
%--

[attr, opt] = parse_tokens('a', varargin);

% NOTE: we may get a link text from the tokens, typically we use target name

if isfield(opt, 'label')
	label = opt.label;
else
	label = prepare_label(target.name);
end 

link = ['<a href="', url, '"', attr, '>', label, '</a>'];
