function file = page_file(site, page)

% PAGE_FILE compute name of output page file
% 
% file = page_file(site, page)

file = strrep(page.file, pages_root(site), build_root(site));
