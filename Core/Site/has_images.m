function [value, images] = has_images(varargin)

% HAS_IMAGES check that site has images
%
% [value, images] = has_images(site, update)

[value, images] = has_assets('images', varargin{:});

