function [root, exists] = site_root(site)

% SITE_ROOT root directory for site
%
% [root, exists] = site_root(site)

if ~proper_site_name(site)
	root = ''; exists = 0; return;
end 

root = [sites_root, filesep, site];

exists = exist_dir(root);
