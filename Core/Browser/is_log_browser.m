function [value, info] = is_log_browser(par)

% is_browser - check browser handles
% ----------------------------------
%
% [value, info] = is_browser(par)
%
% Input:
% ------
%  par - proposed browser handles
%
% Output:
% -------
%  value - browser indicator
%  info - browser info

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1600 $
% $Date: 2005-08-18 17:41:06 -0400 (Thu, 18 Aug 2005) $
%--------------------------------

% TODO: make this leaner for the simple most common case, like 'is_palette'

%--------------------------------
% HANDLE INPUT
%--------------------------------

%--
% handle multiple handles recursively
%--

if numel(par) > 1
	
	value = zeros(size(par)); 
	
	if iscell(par)
		
		for k = 1:numel(par);
			value(k) = is_log_browser(par{k});
		end
		
	else
		
		for k = 1:numel(par);
			value(k) = is_log_browser(par(k));
		end
		
	end
	
	return;
	
end

%--
% check for figure handles
%--

if ~is_handles(par, 'figure')
	value = 0; info = []; return;
end

%--------------------------------
% TEST HANDLE
%--------------------------------

%--
% get browser info and check type
%--

info = get_browser_info(par);

value = ~isempty(strfind(upper(info.type), 'LOG_BROWSER'));