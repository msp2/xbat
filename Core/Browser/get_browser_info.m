function info = get_browser_info(par)

% get_browser_info - get basic browser info
% -----------------------------------------
%
% info = get_browser_info(par)
%
% Input:
% ------
%  par - browser handle
%
% Output:
% -------
%  info - browser info

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2093 $
% $Date: 2005-11-08 16:46:14 -0500 (Tue, 08 Nov 2005) $
%--------------------------------

% TODO: create 'set_browser_info' to store browser info?

%------------------------------
% HANDLE INPUT
%------------------------------

%--
% check handle input
%--

if any(~ishandle(par))
	error('Handle input is required.');
end

%--
% check figure handles
%--

if any(~strcmp(get(par, 'type'), 'figure'))
	error('Input handles must be figure handles.');
end

%--
% handle arrays recursively
%--

if (numel(par) > 1)
	
	for k = 1:numel(par)
		info(k) = get_browser_info(par(k));
	end
	
	info = reshape(info, size(par)); return;
	
end

%------------------------------
% GET BROWSER INFO
%------------------------------

info = parse_browser_tag(get(par, 'tag'));
