function page = get_browser_page(par, data)

% get_browser_page - get standard page struct from browser
% --------------------------------------------------------
%  
% page = get_browser_page(par, data)
%
% Input:
% ------
%  par - browser handle
%  data - browser data
%
% Output:
% -------
%  page - the page

%--
% handle input
%--

if ~nargin || isempty(par)
	par = get_active_browser;
end

if nargin < 2 || isempty(data)
	data = get_browser(par);
end

%--
% populate page from browser
%--

slider = get_time_slider(par);

page.start = slider.value;

page.duration = get_browser(par, 'page__duration', data);

