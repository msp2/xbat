function set_browser_info(h,sound)

% set_browser_info - set basic browser info
% -----------------------------------------
%
% info = set_browser_info(h,sound)
%
% Input:
% ------
%  h - browser handle
%
% Output:
% -------
%  info - browser info

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2093 $
% $Date: 2005-11-08 16:46:14 -0500 (Tue, 08 Nov 2005) $
%--------------------------------

%------------------------------
% HANDLE INPUT
%------------------------------


%------------------------------
% SET BROWSER INFO
%------------------------------

% NOTE: the implementation could change to a handle hash

%--
% get info string components
%--

sep = '::';

user = get_active_user;

lib = user.library(user.active);

%--
% build and store info string
%--

% TODO: extend this to handle various browser types

tag = [ ...
	'XBAT_SOUND_BROWSER', sep , user.name, sep, lib.name, sep, sound_name(sound) ...
];

set(h,'tag',tag);

%--
% get browser info if needed
%--

if (nargout)
	info = get_browser_info(h);
end