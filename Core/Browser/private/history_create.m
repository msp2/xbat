function history = history_create(varargin)

% history_create - create browser history struct
% ----------------------------------------------
%
% history = history_create
%
% Output:
% -------
%  history - history settings structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 132 $
% $Date: 2004-12-02 15:58:47 -0500 (Thu, 02 Dec 2004) $
%--------------------------------

%--
% create history struct
%--

% time related fields

history.created = now; 

history.elapsed = [];

% browser state fields

history.time = []; 

history.channels = [];

history.page = []; 

history.grid = []; 

history.colormap = [];

%--
% set fields if provided
%--

if (length(varargin))
	history = parse_inputs(history,varargin{:});
end
