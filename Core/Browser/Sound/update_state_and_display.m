function update_state_and_display(par, data)

%----------------------------
% HANDLE INPUT
%----------------------------

if nargin < 2
	data = get_browser(par);
end

%----------------------------
% UPDATE STATE
%----------------------------
	
%--
% get current channels state
%--

current = get_browser(par, 'channels');

%--
% adapt spectrogram parameters
%--

% NOTE: this updates the spectrogram palette

% NOTE: channels may have been updated in state, but not yet in display!

data = update_specgram_param(par, data, 1);

%--
% check if we need channel update
%--

if ~isequal(current, data.browser.channels)
	browser_view_menu(par, 'Update Channels');
end

%--
% perform scrollbar update
%--

browser_view_menu(par, 'scrollbar');


