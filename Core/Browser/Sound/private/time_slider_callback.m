function time_slider_callback(obj, eventdata, par, state)

% NOTE: we are thinking that 'set_time_slider' will call this fucntion
% programatically and that the time slider will of course have it as its
% callback

%--
% initialize persistent slider state
%--

persistent SLIDER_STATE;

if isempty(SLIDER_STATE)
	SLIDER_STATE = 0;
end

if nargin > 3
	SLIDER_STATE = state;
end

%--
% update slider state and possibly refresh
%--

current = get(obj, 'value'); previous = get_browser_history(par, 'time');

if current == previous
	SLIDER_STATE = 0; return;
end

% NOTE: it seems like this should talk to the daemon and let it do the
% actual refreshing?

if SLIDER_STATE
	browser_refresh(par);
end

SLIDER_STATE = ~SLIDER_STATE;

