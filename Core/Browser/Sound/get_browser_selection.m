function [selection, count] = get_browser_selection(par, data)

% get_browser_selection - get browser selection
% ---------------------------------------------
%
% selection = get_browser_selection(par, data)
%
% Input:
% ------
%  par - browser
%  data - state
%
% Output:
% -------
%  selection - selection

%--
% handle input
%--

if nargin < 2
	data = get_browser(par);
end

if isempty(data)
	selection = []; count = 0; return;
end

%--
% get selection
%--

selection = data.browser.selection; 

count = numel(selection.event);
