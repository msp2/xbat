function str = get_grid_time_string(grid,time,realtime)

% get_grid_time_string - get time string according to grid
% --------------------------------------------------------
%
% str = get_grid_time_string(grid,time,realtime)
%
% Input:
% ------
%  grid - grid options
%  time - time
%  realtime - date offset
%
% Output:
% -------
%  str - time string according to grid

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 4695 $
% $Date: 2006-04-20 11:22:26 -0400 (Thu, 20 Apr 2006) $
%--------------------------------

%------------------------------
% HANDLE INPUT
%------------------------------

%--
% set default empty realtime
%--

if nargin < 3
	realtime = []; 
end

%--
% handle multiple times recursively
%--

if numel(time) > 1
	
	str = cell(size(time));
	
	for k = 1:numel(time)
		str{k} = get_grid_time_string(grid,time(k),realtime);
	end 
	
	return; 

end

%------------------------------
% COMPUTE TIME STRINGS
%------------------------------

%--
% compute time string based on grid settings, time, and realtime
%--

% TODO: consider a 'get_grid_label_types'

switch grid.time.labels
	
	case 'seconds'
		
		% NOTE: the rounding preserves two decimal digits if needed
		
		str = [num2str(round(100 * time) / 100), ' sec'];
		
	case 'clock'
		
		str = sec_to_clock(time);
		
	case 'date and time'
		
		if isempty(realtime)
			str = sec_to_clock(time);
		else
			str = datestr(offset_date(realtime, time));
		end
		
end
