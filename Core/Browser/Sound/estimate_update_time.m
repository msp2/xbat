function t = estimate_update_time(browser)

% estimate_update_time - estimate browser display update time
%------------------------------------------------------------
%
% t = estimate_update_time(browser)
%
% Input:
% ------
% browser - handle to browser
%
% Output:
% -------
% t - time in seconds to update display

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision$
% $Date$
%--------------------------------


%--
% handle input
%--

if ((nargin < 1) || (isempty(browser)))
	browser = gcf;
end
	
%--
% get browser data
%--

data = get_browser(browser);

%----------------------------
% SPECTROGRAM CREATION TIME
%----------------------------

dt = data.browser.page.duration;

fs = get_sound_rate(data.browser.sound);

% NOTE: speed is in units of samples per second

speed = get_env('mean_specgram_speed');  % NOTE: something like this might be a good idea

if isempty(speed)
	% NOTE: Magic Numbers are our friends!
	speed = 200000;
end
	
base = dt*fs/speed;	

%----------------------------
% FILTERING
%----------------------------

filter = 0;

%----------------------------
% ACTIVE DETECTION
%----------------------------

detect = 0;

t = base + filter + detect;
