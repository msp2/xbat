function set_browser_selection(par, event, m, ix) 

% set_browser_selection - set browser selection
% ---------------------------------------------
%
% set_browser_selection(par, event, m, ix)
%
% Input:
% ------
%  par - browser
%  event - selection event
%  m - log index
%  ix - event index

%--
% check browser input
%--

if ~is_browser(par)
	return;
end

%--
% set selection
%--

% NOTE: in this case the function works as an alias

if (nargin < 3) || isempty(m)
	browser_bdfun(par, event); 
end

%--
% set event selection
%--

if (nargin < 4) || isempty(ix)
	
	if isempty(event.id) || isempty(m)
		return;
	end

	data = get_browser(par);
	
	ix = find(event.id == [data.browser.log(m).event.id], 1);
	
	if isempty(ix)
		return;
	end
	
end

event_bdfun(par, m, ix);