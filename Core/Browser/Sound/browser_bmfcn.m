function browser_bmfcn

% h = gco;
% 
% if (strcmp(get(h,'type'),'uicontrol'))
% 	if (strcmp(get(h,'style'),'slider'))
% 		p = get(gcf,'currentpoint')
% 		tmp = text(p(1),p(2),num2str(get(h,'value')));
% 		drawnow;
% % 		delete(tmp)
% 	end
% end

%--
% get slider handle
%--

data = get(gcf,'userdata');

slider = data.browser.slider;

%--
% display slider value if we are changing the time
%--

if (gco == slider)
	
	t = sec_to_clock(get(slider,'value'));
	
	p = get(gcf,'currentpoint');
	
	tmp = text(p(1),p(2),t);
	
	drawnow;
	
end