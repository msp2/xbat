function geometry = get_geometry(sound, type)

%------------------------------
% HANDLE INPUT
%------------------------------

if nargin < 2 || isempty(type)
	type = 'local';
end

types = {'local', 'global'};

if ~ismember(type, types)
	error(['invalid geometry type, "', type, '".']);
end

%------------------------------
% GET GEOMETRY
%------------------------------
	
error_str = [type, ' geometry not available for "', sound_name(sound), '"'];

if ~isfield(sound, 'geometry')
	geometry = []; return;
end

%--
% structured geometry
%--

if isstruct(sound.geometry) 
	
	if isfield(sound.geometry, type)
		geometry = sound.geometry.(type); return;	
	else
		error(error_str);
	end
	
end

%--
% simple geometry (BC)
%--

if strcmp(type, 'local') 
	geometry = sound.geometry; return;
else
	error(error_str);
end

