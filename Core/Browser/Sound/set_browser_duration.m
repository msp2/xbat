function duration = set_browser_time(par, duration, data)

% set_browser_time - set browser page-start time
% ----------------------------------------------
%
% set_browser_time(par, duration)
%
% Input:
% ------
%  par - parent browser handle
%  duration - duration
%
% Output:
% -------
%  duration - resulting duration

%--------------------------------
% Author: Matt Robbins
%--------------------------------
% $Revision$
% $Date$
%--------------------------------

%--------------------------------
% HANDLE INPUT
%--------------------------------

if isempty(par)
	
	par = get_active_browser;
	
	if isempty(par)
		return;
	end
	
end

%-------------------------------
% SET DURATION
%-------------------------------

set_browser_page(par, [], 'duration', duration);
