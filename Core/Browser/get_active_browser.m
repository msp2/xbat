function par = get_active_browser

% get_active_browser - get handle to current browser
% --------------------------------------------------
%
% par = get_active_browser
%
% Output:
% -------
%  par - active browser handle

persistent LAST_BROWSER

%--
% get browser handles
%--

par = get_xbat_figs('type', 'sound'); 

%--
% no browser or a single browser
%--

if isempty(par) || (numel(par) == 1)
	return;
end

%--
% return single visible browser
%--

visible = get(par, 'visible');

ix = find(strcmp(visible, 'on'));

if numel(ix) == 1
	par = par(ix); LAST_BROWSER = par; return;
end

%--
% browser is current figure
%--

current = gcf;

if ismember(current, par)
	par = current; LAST_BROWSER = par; return;
end

%--
% return browser parent of palette
%--

if is_palette(current)
	
	current = get_palette_parent(current);
	
	if ismember(current, par)
		par = current; LAST_BROWSER = par; return;
	end
	
end

if ~isempty(LAST_BROWSER) && is_browser(LAST_BROWSER)
    par = LAST_BROWSER; return;
end

% NOTE: return empty if all of the above failed

par = [];

