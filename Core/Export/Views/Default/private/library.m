function lines = library(data)

%--
% get template data variables
%--

library = data.library;

%--
% generate file
%--

line{1} = '<table>';

for field = fieldnames(library)
	
	value = library.(field);
	
	line{end + 1} = process_line('<tr><td>$field</td><td>$value</td></tr>');
	
end
    
line{end + 1} = '</table>'