function export_user(user,root,view)

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2014 $
% $Date: 2005-10-25 17:43:52 -0400 (Tue, 25 Oct 2005) $
%--------------------------------

%--------------------------------
% HANDLE INPUT
%--------------------------------

%--
% set default view
%--

% NOTE: the name default is a convention

if ((nargin < 3) || isempty(view))
	view = 'default';
end

%--------------------------------
% EXPORT USER
%--------------------------------

%--
% create users root directory if needed
%--

% NOTE: this creates the user parent directory if needed

user_root = create_dir([root, filesep, 'User', filesep, user.name]);

if (isempty(user_root))
	return;
end

%--
% create user page
%--

% data.id = user.id;

data.user = user;

out = 'index.html'

process_template(view,'user',data,user_root,out);

%--
% export user linked libraries
%--

libs = get_libraries(user);

for lib = libs
	export_library(lib,root,view);
end
