function views = get_views(root)

% get_views - get available export views
% --------------------------------------
%
% views = get_views(root)
%
% Input:
% ------
%  root - views root directory (def: views_root)
%
% Output:
% -------
%  views - available views

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2014 $
% $Date: 2005-10-25 17:43:52 -0400 (Tue, 25 Oct 2005) $
%--------------------------------

%--------------------------------
% HANDLE INPUT
%--------------------------------

%--
% get views root if needed
%--

if ((nargin < 1) || isempty(root))
	root = views_root;
end

%-----------------------------------
% GET VIEWS
%-----------------------------------

% TODO: implement persistent cache

%--
% get view directories
%--

dirs = get_field(what_ext(root),'dir');

%--
% evaluate constructors to get views
%--

views = []; 

init = pwd;

for k = 1:length(dirs)

	%--
	% move to view directory
	%--
	
	cd([root, filesep, dirs{k}]);
	
	%--
	% get valid function handle
	%--
	
	% NOTE: view name must nearly be a variable name
	
	name = lower(dirs{k});
	
	if (~isvarname(name))
		continue;
	end
	
	% NOTE: check that function handle points to actual function 
	
	fun = str2func(name);
		
	if (isempty(get_field(functions(fun),'file')))
		continue;
	end

	%--
	% evaluate constructor
	%--
	
	% NOTE: handle view evaluation failure
	
	try
		if (isempty(views))
			views = feval(fun);
		else
			views(end + 1) = feval(fun);
		end
	end

end

cd(init);