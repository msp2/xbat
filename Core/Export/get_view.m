function view = get_view(name)

% get_view - get named view root
% ------------------------------
%
% view = get_view(name)
%
% Input:
% ------
%  name - view name
%
% Output:
% -------
%  view - view structure

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 2014 $
% $Date: 2005-10-25 17:43:52 -0400 (Tue, 25 Oct 2005) $
%--------------------------------

%--------------------------------
% SETUP
%--------------------------------

%--
% get available views
%--

views = get_views;

% NOTE: there are no view to select from

if (isempty(views))
	view = []; return;
end

%--------------------------------
% SELECT VIEW
%--------------------------------

%--
% find name in view names
%--

names = {views.name}';

ix = find(strcmpi(name,names));

%--
% select view
%--

if (isempty(ix))
	view = [];
else
	view = views(ix);
end

