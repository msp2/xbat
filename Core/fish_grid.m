function x = fish_grid(n,p)

% fish_grid - create non-uniform grid in unit interval
% ----------------------------------------------------
%
% x = fish_grid(n,p)
%
% Input:
% ------
%  n - number of points in grid
%  p - grid parameters
%
% Output:
% -------
%  x - non-uniform grid points in unit interval

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Date: 2004-12-12 23:59:12 -0500 (Sun, 12 Dec 2004) $
% $Revision: 261 $
%--------------------------------

%--
% set parameters
%--

if ((nargin < 2) | isempty(p))
	p = 2.5;
end

%--
% set number of points
%--

if ((nargin < 1) | isempty(n))
	n = 32;
end

%--
% compute non-uniform grid on unit circle
%--

if (mod(n,2) == 0)
	
	y = linspace(0,1,n/2);
	x = (1 - y).^(1/p) / 2;
	x = [fliplr(x), 1 - x];
	
else
	
	y = linspace(0,1,floor(n/2));
	x = (1 - y).^(1/p) / 2;
	x = [fliplr(x), 0.5, 1 - x];
	
% 	y = linspace(0,1,floor(n/2));
% 	
% 	% parabolic
% 	
% 	x = sqrt(y);
% 	
	% circular
	
% 	x = sqrt(1 - y.^2) ./ 2;
% 	x = [fliplr(x), 0.5, 1 - x];
	
end
	