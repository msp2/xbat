function attribute = load(store, context)

% SENSOR_GEOMETRY - load

attribute = sensor_geometry(store);

if isempty(attribute)
	return;
end

if size(attribute.local, 2) < 3
	attribute.local(:, 3) = 0;
end

if ~isfield(attribute, 'global')
	return;
end

if size(attribute.global, 2) < 3
	attribute.global(:, 3) = 0;
end