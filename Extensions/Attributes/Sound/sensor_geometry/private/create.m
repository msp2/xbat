function attribute = create(context)

% SENSOR_GEOMETRY - create

attribute = struct;

channels = context.sound.channels;

attribute.local = default_geometry(channels);

attribute.global = [];

attribute.offset = [];

attribute.ellipsoid = [];