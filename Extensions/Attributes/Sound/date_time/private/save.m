function save(attribute, store, context)

% DATE_TIME - save

lines{1} = 'Date and Time';

lines{2} = '(MATLAB date vector)';

vec = datevec(attribute.datetime);

line = '';

for k = 1:length(vec)
	line = [line, num2str(vec(k)), ', '];
end

line(end-1:end) = '';

lines{3} = line;

file_writelines(store, lines);