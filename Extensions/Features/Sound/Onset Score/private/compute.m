function [feature, context] = compute(page, parameter, context)

% ONSET SCORE - compute

feature = struct;

%--
% get samples
%--

if ~isempty(page.filtered)
	page.samples = page.filtered;
end

%--
% compute spectrogram
%--

rate = get_sound_rate(context.sound);

[X, freq, time] = fast_specgram(page.samples, rate, 'norm', context.sound.specgram);

%--
% compute feature time
%--

% NOTE: the common onset feature time grid is between spectrogram frames

time(end) = []; time = time + 0.5 * (time(2) - time(1));

feature.time = page.start + time;

%--
% compute onset score
%--

onset = diff_dist(X, 'foote');

feature.onset.sequence = onset;

% TODO: this should be into a 'get_stats' type function

feature.onset.range = fast_min_max(onset);

feature.onset.mean = mean(onset);

feature.onset.std = std(onset);

% TODO: make the scale a parameter

[feature.onset.peaks, feature.onset.smooth] = scale_peaks(onset(:), 63);
