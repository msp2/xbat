function parameter = parameter__create(context)

% AMPLITUDE - parameter__create

parameter = struct;

% NOTE: this implements a model constraint

rate = get_sound_rate(context.sound);

block = get_block_range(rate);

parameter.block = clip_to_range(0.025, block);

parameter.auto = 1;

parameter.overlap = 0;
