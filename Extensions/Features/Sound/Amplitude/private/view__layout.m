function handles = view__layout(par, parameter, context)

% AMPLITUDE - view__layout

%--
% clear axes from figure
%--

delete_me = findobj(par, 'type', 'axes');

if ~isempty(delete_me)
	delete(delete_me);
end

%--
% layout axes and get handles
%--

% NOTE: this is a generic stacked channels feature display, factor!

layout = layout_create(numel(context.page.channels), 1); layout.margin(1) = 0.5;

harray(par, layout);

handles = harray_select(par, 'level', 1);

%--
% tag axes
%--

% NOTE: this is the tagging convention currently used by 'get_play_axes'

for k = 1:length(handles)
	set(handles(k), 'tag', int2str(context.page.channels(k)));
end