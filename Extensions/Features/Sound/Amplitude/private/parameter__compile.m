function [parameter, context] = parameter__compile(parameter, context)

% AMPLITUDE - parameter__compile

%--
% consider adapt request
%--

% NOTE: we check whether adaptation is requested and possible in context

if ~parameter.auto || ~isfield(context, 'page')
	return;
end

%--
% adapt block
%--

rate = get_sound_rate(context.sound); 

block = get_block_range(rate);

parameter.block = clip_to_range(context.page.duration / 1000, block);

%--
% update block control 
%--

pal = get_extension_palette(context.ext, context.par);

if isempty(pal)
	return; 
end

set_control(pal, 'block', 'value', parameter.block);
