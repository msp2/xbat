function model = component_fit_spline(component, n)

% component_fit_spline - fit spline to component
% ----------------------------------------------
%
% model = component_fit_spline(component, n)
%
% Input:
% ------
%  component - a component event
%  n - the number of knots to model the event with
%
% Output:
% -------
% model - the model: y- and t-coordinates of knots


%--
% fit spline
%--

[y, val, flag] = spline_fit_image(component.image, n);

%--
% scale frequency
%--

f = component.event.freq / 1000;  bw = diff(f);

y = y * (bw/size(component.image, 1)) + f(1); 

%--
% get time grid
%--

% NOTE: remember skips and plops

knot_time = component.event.duration / (n - 1); 

%--
% N knots [0 : N - 1] and two phantom knots
%--

t = [-1:n]*knot_time; 

model.y = y; 

model.t = t + component.event.time(1);
