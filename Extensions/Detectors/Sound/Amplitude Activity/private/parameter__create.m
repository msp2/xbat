function parameter = parameter__create(context)

% AMPLITUDE ACTIVITY - parameter__create

% size of decision block

parameter.block = 0.025;

% size of window

parameter.window = 5;

% percentile to mark as signal amplitude

parameter.percent = 0.7;

% fraction needed to exceed signal amplitude in block

parameter.fraction = 0.4;

% markovian relaxation of fraction

parameter.relax = 0.70;

% flag for event refining using energy time frequency distribution

parameter.refine = 1;
