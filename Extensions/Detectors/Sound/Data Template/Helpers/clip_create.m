function clip = clip_create(event, rate, data, code, mode, id)

%--
% handle input
%--

if nargin < 6
	id = [];
end

% NOTE: the default value is the code for 'Keep (Exclusive)'

if nargin < 5 || isempty(mode)
	mode = 2;
end

if nargin < 4 || isempty(code)
	code = '';
end

if nargin < 3 || isempty(data)
	data = [];
end

if nargin < 2 || isempty(rate)
	rate = [];
end

if ~nargin || isempty(event)
	event = empty(event_create);
end


clip.event = event;

clip.samplerate = rate;

%--
% read enough samples to fully re-generate the event spectrogram (2x event duration)
%--

clip.data = data;

clip.code = code;

% NOTE: the default value is the code for 'Keep (Exclusive)'

clip.mode = mode; 

clip.id = id;