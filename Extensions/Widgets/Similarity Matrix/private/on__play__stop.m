function [handles, context] = on__play__stop(widget, data, parameter, context)

% SIMILARITY MATRIX - on__play__stop

handles = [];

%--
% get parent axes
%--

ax = similarity_matrix_axes(widget);

if isempty(ax)
	return; 
end

%--
% delete cursors
%--

delete([get_cursor(ax, 1), get_cursor(ax, 2)]);