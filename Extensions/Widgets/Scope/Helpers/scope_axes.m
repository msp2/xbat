function handles = scope_axes(widget, varargin)

handles(1) = create_axes(widget, 'scope_axes::1', varargin{:});

% NOTE: in the case of single channel sounds we use a single scope axis

if isempty(findobj(widget, 'tag', 'scope_axes::2'))
	return;
end

handles(2) = create_axes(widget, 'scope_axes::2', varargin{:});