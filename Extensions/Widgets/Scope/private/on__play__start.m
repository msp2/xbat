function [handles, context] = on__play__start(widget, data, parameter, context)

% SCOPE - on__play__start

handles = [];

if isempty(data.buffer)
	return;
end

%--
% create play line in spectrum axes
%--

ax = scope_axes(widget);

% NOTE: get sound player display options

player = sound_player;

scope_line(ax, ...
	'xdata', [0, 1], ...
	'ydata', [0, 0], ...
	'color', player.color ...
);

%--
% scale axes for a better display
%--

ylim = get_scope_ylim(data.buffer.samples);

scope_axes(widget, 'ylim', ylim);
