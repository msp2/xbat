function handles = layout(widget, parameter, context)

% SCOPE - layout

%--
% clear existing widget axes
%--

delete(findobj(widget, 'type', 'axes'));

%--
% layout axes and get handles
%--

% NOTE: for single channel files we only ever need one scope

if context.sound.channels < 2
	n = 1;
else
	n = 2;
end

layout = layout_create(n, 1); layout.margin(1) = 0.5; layout.margin(4) = 0.75;

harray(widget, layout);

handles = harray_select(widget, 'level', 1);

%--
% tag and label axes
%--

for k = 1:n
	set(handles(k), 'tag', ['scope_axes::', int2str(k)], 'xticklabel', []);
end

%-------------
% TEST CODE
%-------------

%--
% set display options
%--

% TODO: this should happen at a higher level

color = context.display.grid.color;

set(handles, 'xcolor', color, 'ycolor', color);

%--
% display zero line
%--

for k = 1:length(handles)
	
	line( ...
		'parent', handles(k), ...
		'xdata', [-10^10, 10^10], ...
		'ydata', [0, 0], ...
		'linestyle', ':', ...
		'color', color ...
	);

end
