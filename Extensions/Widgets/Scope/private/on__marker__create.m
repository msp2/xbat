function [handles, context] = on__marker__create(widget, data, parameter, context)

% SCOPE - on__marker__create

%--
% get marker samples
%--

marker = data.marker;

samples = get_marker_samples(marker, parameter, context);

%--
% update scope display
%--

handles = update_scope_display(widget, marker.time, parameter.duration, samples, context);

set(handles, 'color', marker.color);

%--
% scale axes for a better display
%--

% TODO: add parameters to adjust scaling

ylim = get_scope_ylim(samples, parameter);

scope_axes(widget, 'ylim', ylim);
