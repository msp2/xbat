function [handles, context] = on__selection__create(widget, data, parameter, context)

% SPECTRUM - on__selection__create

handles = [];

im = data.browser.images; im = im(1);

%--
% get spectrogram slices
%--

slices = get_spectrogram_slices(im, data.selection.event.time);

%--
% update display
%--

ax = spectrum_axes(widget); nyq = get_sound_rate(context.sound) / 2;

if ~isempty(ax)
	
	%--
	% display spectrum
	%--
	
	selection_line(ax, ...
		'color', data.selection.color, ...
		'xdata', linspace(0, nyq, size(slices, 1)), ...
		'ydata', mean(slices, 2) ...
	);

	%--
	% display frequency guides
	%--
	
	[low, high] = frequency_guides(ax, ...
		'color', context.display.grid.color ...
	);
	
	set(low, ...
		'xdata', data.selection.event.freq(1) * ones(1, 2), ...
		'ydata', get(ax, 'ylim') ...
	);

	set(high, ...
		'xdata', data.selection.event.freq(2) * ones(1, 2), ...
		'ydata', get(ax, 'ylim') ...
	);

end
