function [result, context] = compute(log, parameter, context)

% STRIP - compute

result = [];

%--
% strip log events
%--

if parameter.all
	
	for k = 1:length(log.event)
		log.event(k).measurement = empty(measurement_create);
	end
	
else
	
	for k = 1:length(log.event)
		
		for j = length(log.event(k).measurement):-1:1
			
			if ~ismember(log.event(k).measurement(j).name, parameter.measures)
				continue;
			end
			
			log.event(k).measurement(j) = [];
			
		end
		
	end
	
end

%--
% save stripped log
%--

% NOTE: if we are modifying the input log file backup 

if parameter.inplace || isempty(parameter.suffix)
	log_backup(log);
else
	log.file = [file_ext(log.file), parameter.suffix, '.mat'];
end

log_save(log);




