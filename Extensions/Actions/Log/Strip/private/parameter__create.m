function parameter = parameter__create(context)

% STRIP - parameter__create

parameter = struct;

parameter.suffix = '_CLEAN';

parameter.inplace = 1;

parameter.measures = {};

parameter.all = 1;
