function result = parameter__control__callback(callback, context)

% DETECT - parameter__control__callback

result = [];

switch callback.control.name
	
	case 'detector'

		%--
		% get detector extension
		%--
		
		detector = get_control(callback.pal.handle, 'detector', 'value');
		
		ext = get_extensions('sound_detector', 'name', detector{1});
		
		%--
		% get preset names
		%--
		
		presets = {};
		
		if ~isempty(ext)
			presets = get_preset_names(ext);
		end
		
		% NOTE: this may happen under various conditions
		
		if isempty(presets)
			presets = {'(No Presets Found)'};
		end

		%--
		% update presets control
		%--
		
		handles = get_control(callback.pal.handle, 'preset', 'handles');
		
		set(handles.obj, 'string', presets, 'value', []);
		
	case 'preset'
		
		% TODO: this will update the preset info controls in the future
		
	case 'output'
		
		% TODO: allow for some tokens
		
end

%--
% update state of OK button
%--

% NOTE: consider having a type validation callback and extending validation

handles = get_control(callback.pal.handle, 'output', 'handles');

set_control(callback.pal.handle, 'OK', ...
	'enable', proper_filename(get(handles.obj, 'string')) ...
);




