function [result, context] = compute(event, parameter, context)

% COPY TO LOG - compute

result = struct;

log = get_library_logs('logs', context.library, context.sound, parameter.log{1});

%--
% append events to log
%--

log = log_append(log, event);

%--
% save the log
%--

log_save(log);

try
    par = context.callback.par.handle;
catch
    par = get_active_browser;
end

%--
% change log in browser if necessary
%--

if log_is_open(log, par)
    
    names = log_name(get_browser(par, 'log'));
    
    ix = find(strcmp(names, log_name(log)));
    
    %--
    % set browser log
    %--
    
    data = get_browser(par); 
    
    data.browser.log(ix) = log; 
    
    set(par, 'userdata', data);
    
    %--
    % redraw event palette
    %--
  
    pal = get_palette(par, 'Event');
    
    if ~isempty(pal)
        control_callback([], pal, 'find_events');     
    end
    
end