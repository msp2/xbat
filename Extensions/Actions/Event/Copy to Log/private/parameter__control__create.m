function control = parameter__control__create(parameter, context)

% COPY TO LOG - parameter__control__create

control = empty(control_create);

%--
% get browser for context
%--

try
    par = context.callback.par.handle;
catch
    par = [];
end
      
if isempty(par)
    par = get_active_browser;
end

%--
% get log_name(s) that we can't copy to
%--

if iscell(context.target)
    
    event_log_name = cell(1, length(context.target));
    
    for k = 1:length(context.target)
        
        [ignore, event_log_name{k}] = get_str_event(par, context.target{k}); 
        
    end
    
else

    [event, event_log_name] = get_str_event(par, context.target); 

end

%--
% get list of logs names that the event(s) can be copied to
%--

log_names = setdiff(log_name(get_browser(par, 'log')), event_log_name);

if isempty(log_names)
    log_names = '(No Logs Available)';
end

%--
% create control
%--

control(end + 1) = control_create( ...
    'style', 'popup', ...
    'name', 'log', ...
    'alias', 'destination', ...
    'string', log_names, ...
    'value',1 ...
);