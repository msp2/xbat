function simple = prepare_csv_event(event, context)

% NOTE: this function prepares an event that we can use 'struct_to_csv' on

% TODO: consider providing a field selection control for this operation

%--
% parent log and id
%--

simple.log = context.log;

simple.id = event.id;

%--
% time and frequency information
%--

% time

% TODO: ensure that the context is right to get the real time

simple.real_start = get_browser_time_string([], event.time(1), context)';

simple.real_stop = get_browser_time_string([], event.time(2), context)';

simple.record_start = event.time(1); simple.record_stop = event.time(2);

simple.duration = diff(event.time);

% frequency

simple.min_freq = event.freq(1); simple.max_freq = event.freq(2);

simple.bandwidth = diff(event.freq);

%--
% metadata
%--

simple.score = event.score; simple.rating = event.rating;

simple.tags = event.tags; simple.notes = event.notes;
