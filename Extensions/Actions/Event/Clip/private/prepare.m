function [result, context] = prepare(parameter, context)

% CLIP - prepare

%--
% create output directory, throw error if we fail
%--

% NOTE: the output root and whether we needed to create it are the result

[result.root, result.created] = create_dir(parameter.output);

if isempty(result.root)
	error('Unable to create output directory.');
end
