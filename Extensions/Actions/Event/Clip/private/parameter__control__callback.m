function result = parameter__control__callback(callback, context)

% CLIP - parameter__control__callback

result = struct;

pal = callback.pal.handle;

%--
% perform control-specific action
%--

switch callback.control.name
    
    case 'add_token'
        
        str = get_control(pal, 'file_names', 'string');
        
        value = get_control(pal, 'add_token', 'value');
        
        value = file_name_tokens(value{1});
        
        if isempty(str)
            str = value;
        else
            str = [str, '_', value];
        end
        
        set_control(pal, 'file_names', 'string', str);
        
    case 'tapering'
        
        enable = get_control(pal, 'tapering', 'value');

        set_control(pal, 'taper_length', 'enable', enable);
        
end