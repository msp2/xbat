function code = gradient_type_code(type)

% gradient_type_code - convert gradient type string to code
% ---------------------------------------------------------
%
% code = gradient_type_code(type)
%
% Input:
% ------
%  type - type string
% 
% Output:
% -------
%  code - code corresponding to type

switch (type) 
	
	case ('Dilation')
		code = -1;
		
	case ('Symmetric')
		code = 0;

	case ('Erosion')
		code = 1;
    
	otherwise
		error(['Unrecognized gradient type ''', type, '''.']);

end