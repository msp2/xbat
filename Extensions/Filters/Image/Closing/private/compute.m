function [X, context] = compute(X, parameter, context)

% CLOSING - compute

SE = create_se(parameter);

X = morph_close(X, SE);
