function [X, context] = compute(X, parameter, context)

% TOPHAT - compute

SE = create_se(parameter); code = tophat_type_code(parameter.type{1});

X = morph_tophat(X, SE, code);