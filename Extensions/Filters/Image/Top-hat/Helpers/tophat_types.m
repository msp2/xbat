function [types,ix] = tophat_types

% tophat_types - tophat type strings
% ----------------------------------
%
% [types,ix] = tophat_types
%
% Output:
% -------
%  types - cell of type strings
%  ix - default value index

types = {'Self-Complement', 'Black', 'White'};

ix = 1;