function code = tophat_type_code(type)

% tophat_type_code - convert tophat type string to code
% -----------------------------------------------------
%
% code = tophat_type_code(type)
%
% Input:
% ------
%  type - type string
% 
% Output:
% -------
%  code - code corresponding to type

switch (type) 
	
	case ('Self-Complement')
		code = -1;
		
	case ('Black')
		code = 0;

	case ('White')
		code = 1;
    
	otherwise
		error(['Unrecognized Top-Hat type ''', type, '''.']);

end