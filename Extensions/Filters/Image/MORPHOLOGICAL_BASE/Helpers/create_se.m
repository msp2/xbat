function SE = create_se(param)

% create_se - structuring element from extension parameters
% ---------------------------------------------------------
%
% SE = create_se(param)
%
% Input:
% ------
%  param - morphological base parameters
%
% Output:
% -------
%  SE - structuring element

%--------------------------------
% Author: Harold Figueroa
%--------------------------------
% $Revision: 1.0 $
% $Date: 2003-06-11 18:22:03-04 $
%--------------------------------

%--
% allow some flexibility in style
%--

% NOTE: we accept a string or a cell container as well as insignificant space

if (iscell(param.style))
	param.style = param.style{1};
end 

style = lower(strtrim(param.style));

%--
% compute based on style
%--

% NOTE: there are remaining options for 'se_ball' not exposed

switch (style)

	case ('rectangle')
		SE = ones(2 * param.height + 1,2 * param.width + 1);

	case ('disc')
		SE = se_ball([param.width, param.height],2);

	case ('diamond')	
		SE = se_ball([param.width, param.height],1);

	case ('star')
		SE = zeros(2 * param.height + 1,2 * param.width + 1); 
        SE(param.height + 1,:) = 1; SE(:,param.width + 1) = 1;
		
end
