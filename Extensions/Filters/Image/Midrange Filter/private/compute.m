function [X, context] = compute(X, parameter, context)

% MIDRANGE FILTER - compute

SE = create_se(parameter);

% NOTE: simultaneous erosion and dilation is slightly more efficient

[E, D] = morph_min_max(X, SE);

X = 0.5 * (E + D);
