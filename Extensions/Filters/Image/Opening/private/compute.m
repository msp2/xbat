function [X, context] = compute(X, parameter, context)

% OPENING - compute

SE = create_se(parameter);

X = morph_open(X, SE);