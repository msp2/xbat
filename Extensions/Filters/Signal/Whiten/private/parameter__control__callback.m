function result = parameter__control__callback(callback, context)

% WHITEN - parameter__control__callback

% result = struct;

switch callback.control.name

	case 'use_log'
		
		value = get_control(callback.pal.handle, callback.control.name, 'value');
			
		set_control(callback.pal.handle, 'noise_log', 'enable', value);

		% NOTE: this is mostly used during 'onload', it has a global effect	
		if ~isempty(callback.par.handle)
            update_extension_palettes(callback.par.handle);
        end
        
	case 'lowpass'
		
		value = get_control(callback.pal.handle, callback.control.name, 'value');
			
		set_control(callback.pal.handle, 'max_freq', 'enable', value);
		
end

% TODO: we need to update filter so that there is something to update in display

%--
% call parent callback
%--

fun = parent_fun; result = fun(callback, context);
