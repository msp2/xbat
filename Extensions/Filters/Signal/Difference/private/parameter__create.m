function parameter = parameter__create(context)

% DIFFERENCE - parameter__create

%--
% get parent parameters
%--

fun = parent_fun; parameter = fun(context);

%--
% add difference order parameter
%--

parameter.order = 1;