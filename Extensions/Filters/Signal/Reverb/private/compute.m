function [X, context] = compute(X, parameter, context)

% REVERB - compute

% NOTE: too simple a reverb aka the cyborg filter

% a = zeros(1, 200); a(1) = 1; a(end - 2:end) = 0.1; b = 1;
% 
% for k = 1:4
% 	X = filter(b, a, X);
% end

param = moorer;

param.fs = get_sound_rate(context.sound);

X = moorer(X, param);
