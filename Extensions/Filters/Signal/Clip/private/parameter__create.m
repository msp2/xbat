function parameter = parameter__create(context)

% CLIP - parameter__create

parameter.percent_level = 20;

parameter.oversample = 2;