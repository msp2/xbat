function [parameter, context] = parameter__compile(parameter, context)

% LOWPASS-BUTTERWORTH - parameter__compile

nyq = get_sound_rate(context.sound) / 2;

cutoff = parameter.max_freq / nyq;

type = parameter.type;

if iscell(type)
	type = type{1};
end

[parameter.filter.b, parameter.filter.a] = butter(parameter.order, cutoff, lower(type));