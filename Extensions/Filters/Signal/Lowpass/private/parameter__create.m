function parameter = parameter__create(context)

% LOWPASS - parameter__create

fun = parent_fun; parameter = fun(context);

rate = get_sound_rate(context.sound); nyq = 0.5 * rate;

parameter.min_freq = 0;

parameter.max_freq = 0.5 * nyq;