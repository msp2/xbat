function [parameter, context] = parameter__compile(parameter, context)

% NOTCH - parameter__compile

nyq = get_sound_rate(context.sound) / 2;

%--
% get zero angle
%--

freq = 0.5 * parameter.center_freq / nyq;

%--
% get filter impulse response
%--

%--
% get number of conjugate zero pairs
%--

np = round((parameter.order - 1) / 2);

%--
% generate sampled frequency response
%--

F = linspace(0, 0.5, np);

k = find(F < freq, 1, 'last');

Fl = linspace(0, freq, k); Fh = linspace(freq, 0.5, np - k); Fh = Fh(2:end);

F = [Fl, Fh];

H = ones(size(F));  H(k) = 0;

%--
% design filter using frequency sampling
%--

h = fsamp(F, H); 

% h = h .* hamming(length(h));

parameter.filter.b = h;


