function [X, context] = compute(X, parameter, context)

% WHITENING - compute

order = round(parameter.order);

for k = 1:size(X, 2);
	
	%--
	% adapt coefficients to this page
	%--
	
	if isempty(parameter.h)
		h = aryule(X(:, k), order);
	else
		h = parameter.h;
	end
	
	%--
	% mitigate filtering if necessary
	%--
	
	h = h .* (parameter.r .^ [0:order]);

	%--
	% display adapted filter frequency response
	%--

	if context.debug
		figure; freqz(h);
	end

	%--
	% filter samples
	%--

	X(:,k) = filter(h, 1, X(:,k));

end

