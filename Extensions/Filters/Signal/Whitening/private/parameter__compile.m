function [parameter, context] = parameter__compile(parameter, context)

% WHITENING - parameter__compile

%--
% try to get the noise log from parameters
%--

log_name = parameter.noise_log;

if isempty(log_name)
	return;
end

if iscell(log_name)
	log_name = log_name{1};
end

[ignore, log_name] = fileparts(log_name);

%--
% get the actual log
%--

log = get_library_logs('logs', context.library, context.sound, log_name);

parameter.h = estimate_log_noise(log, parameter, context);
