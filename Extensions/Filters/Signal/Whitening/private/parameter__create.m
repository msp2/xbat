function parameter = parameter__create(context)

% WHITENING - parameter__create

parameter = struct;

parameter.order = 3;

parameter.r = 1;

parameter.noise_log = [];

parameter.h = [];
