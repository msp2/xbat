


ServiWin v1.30
Copyright (c) 2003 - 2006 Nir Sofer



Description
===========

ServiWin utility displays the list of installed drivers and services on
your system. For some of them, additional useful information is
displayed: file description, version, product name, company that created
the driver file, and more.
In addition, ServiWin allows you to easily stop, start, restart, pause,
and continue service or driver, change the startup type of service or
driver (automatic, manual, disabled, boot or system), save the list of
services and drivers to file, or view HTML report of installed
services/drivers in your default browser.



Versions History
================


* Version 1.30:
  o Offline mode - Allows you to view all services/drivers and change
    the startup type in another instance of Windows operating system.
    (/offline command-line option)
  o A tooltip is displayed when a string in a column is longer than
    the column length.

* Version 1.20:
  o Added find dialog-box.
  o Added accelerator keys.
  o Added toolbar buttons for Start, Restart, and Stop actions.
  o Added /remote command-line option
  o New column: Dependencies
  o New column: Error Control
  o New column: Last Error - Displays an error message when
    starting/stopping service is failed.

* Version 1.11: Added support for Windows XP visual styles.
* Version 1.10:
  o Started/Disabled services are marked with different colors.
  o Ability to translate to other languages.
  o The "copy" option nows copies the services/drivers information as
    tab-delimited text, so you can paste it directly to Excel.

* Version 1.00 - First release.



System Requirement
==================

This utility works under Windows 2000, Windows XP, and Windows NT.
Windows 98 and Windows ME are not supported.



Using ServiWin
==============

This utility is a standalone executable, so it doesn't require any
installation process or additional DLLs. Just run the executable
(serviwin.exe) and start using it.
The main window displays the list of all drivers or services, according
to your selection. You can switch between drivers list and services list
by selecting the desired list from the View menu, or simply use the F7
and F8 keys.
You can select one or more drivers or services from the list, and then
change their status (Stop, Start, Restart, Pause, or Continue) or their
startup type (Automatic, Manual, Disabled, Boot, or System). You can also
save the selected items to text or HTML file (Ctrl + S) or copy this
information to the clipboard.

Warning: Changing the status or the startup type of some drivers or
services may cause to your operating system to work improperly. Do not
disable or stop drivers/services if you are not 100% sure about what you
are doing.



Connecting To Another Computer
==============================

ServiWin allows you to work with drivers/services list of another
computer on your LAN. In order to do that, you must be connected to the
other computer with administrator privileges, as well as admin shares
(\\computer\drive$) should be enabled on that computer.



Offline Mode
============

Starting from version 1.30, ServiWin allows you to connect another
instance of Windows operating system. In this offline mode, you cannot
stop or start a service (because the other operating system is not really
running...), but you can change the "Startup Type" of a service, so the
next time that the other operating system is loaded, the service will be
started or won't be started according to the startup type that you chooe.

In order to use ServiWin in offline mode, run ServiWin with /offline
command-line option, and specify the Windows directory of the operating
system that you want to load. For example:
serviwin.exe /offline e:\windows

Be aware that when using this offline mode, the 'SYSTEM' registry file of
the other operating system is temporarily loaded as a new hive under
HKEY_LOCAL_MACHINE.



Colors In ServiWin
==================

Starting from v1.10, ServiWin marks the services and drivers with
different colors according to the following rules:


Blue
All started services/drivers are painted with this color.
ForeColorStarted
BackColorStarted


Red
All disabled services/drivers are painted with this color.
ForeColorDisabled
BackColorDisabled


Purple
All services/drivers that starts automatically by the operating systems
(with 'Automatic' and 'Boot' Startup types) but they are not currently
running.
ForeAutoNotStarted
BackAutoNotStarted


For advanced users only: If you want to view the services/drivers in
different colors, add the approprite Registry value (according to the
above table) to HKEY_CURRENT_USER\Software\NirSoft\ServiWin. Each color
value is DWORD value that represents the color in RGB format.
For example: If you want to mark all started services with yellow
background color, add BackColorStarted value as DWORD containing 'a0ffff'.



Command-Line Options
====================

General syntax:
serviwin [/save type] [drivers | services] [filename] {\\computer}

The [/save type] may contain one of the following values:


/stext
Saves the list of all drivers/services into a regular text file.

/stab
Saves the list of all drivers/services into a tab-delimited text file.

/stabular
Saves the list of all drivers/services into a tabular text file.

/shtml
Saves the list of all drivers/services into horizontal HTML file.

/sverhtml
Saves the list of all drivers/services into vertical HTML file.

The second parameter specifies whether to save the 'services' list or the
'drivers' list.
The [filename] parameter specifies the filename to save the
services/drivers list.
The last parameter, {\\computer} is optional. If you omit this parameter,
the drivers/services list will be loaded from the local computer.


Examples:

serviwin.exe  /shtml services "c:\temp\serv.html" \\comp1
serviwin.exe  /stext drivers "c:\temp\drv.txt"


Starting from version 1.20, you can use the /remote parameter in order to
start ServiWin with the specified remote computer (without saving the
drivers/services to file). For example:

serviwin.exe  /remote \\comp10


Starting from version 1.30, you can use /offline parameter in order to
connect another instance of Windows operating system. For example:

serviwin.exe  /offline d:\windows




Translating to another language
===============================

ServiWin allows you to easily translate all menus, dialog-boxes, and
other strings to other languages.
In order to do that, follow the instructions below:
1. Run ServiWin with /savelangfile parameter:
   serviwin.exe /savelangfile
   A file named serviwin_lng.ini will be created in the folder of
   ServiWin utility.
2. Open the created language file in Notepad or in any other text
   editor.
3. Translate all menus, dialog-boxes, and string entries to the
   desired language.
4. After you finish the translation, Run ServiWin, and all translated
   strings will be loaded from the language file.
   If you want to run ServiWin without the translation, simply rename the
   language file, or move it to another folder.



License
=======

This utility is released as freeware. You are allowed to freely
distribute this utility via floppy disk, CD-ROM, Internet, or in any
other way, as long as you don't charge anything for this. If you
distribute this utility, you must include all files in the distribution
package, without any modification !



Disclaimer
==========

The software is provided "AS IS" without any warranty, either expressed
or implied, including, but not limited to, the implied warranties of
merchantability and fitness for a particular purpose. The author will not
be liable for any special, incidental, consequential or indirect damages
due to loss of data or any other reason.



Feedback
========

If you have any problem, suggestion, comment, or you found a bug in my
utility, you can send a message to nirsofer@yahoo.com


